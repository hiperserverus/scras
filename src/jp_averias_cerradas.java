
import Control_bd.operaciones_sql;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author usuario
 */
public class jp_averias_cerradas extends javax.swing.JPanel {

                  public DefaultTableModel modelo2 = new DefaultTableModel() {
            // codigo para las que las celdas del jtbale no sean editables
            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return false; // el resto de las celdas no son editables

            }

    };
              
operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
ResultSet res;//creacion de la variable resultset
    public jp_averias_cerradas() {
        initComponents();
        averias_cerradas();
    }

   public void averias_cerradas(){
   
                            //  Para el desplazamiento horizoltal del scrollpane en el jtable       
                            jt_vista_2.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                            jt_vista_2.doLayout(); 

                            //inabilita el movimiento de las columns   del jtable  
                            jt_vista_2.getTableHeader().setReorderingAllowed(false);
                            jt_vista_2.getTableHeader().setResizingAllowed(false);

                            jt_vista_2.setModel(modelo2);

                            modelo2.addColumn("Pedido");
                            modelo2.addColumn("Nombre");
                            modelo2.addColumn("Region");
                            modelo2.addColumn("Estado");
                            modelo2.addColumn("Municipio");
                            modelo2.addColumn("Parroquia");
                            modelo2.addColumn("Direccion");
                            modelo2.addColumn("Nombre del Cliente Principal");
                            modelo2.addColumn("Tipo de Sector");
                            modelo2.addColumn("Sector");
                            modelo2.addColumn("MAC");
                            modelo2.addColumn("IP");
                            modelo2.addColumn("MASCARA");
                            modelo2.addColumn("QoS");
                            modelo2.addColumn("Telepuerto");
                            modelo2.addColumn("Circuito");
                            modelo2.addColumn("Estatus de la Red");
                            modelo2.addColumn("Empresa Instaladora");
                            modelo2.addColumn("Codigo de Cierre");
                            modelo2.addColumn("Fecha de Reporte");
                            modelo2.addColumn("Contacto de Reporte");
                            modelo2.addColumn("Telefono de Contacto");
                            modelo2.addColumn("Descripcion de la Falla");
                            modelo2.addColumn("Causa de la Falla");
                            modelo2.addColumn("Observaciones Operativas");
                            modelo2.addColumn("Fecha de Atencion");
                            modelo2.addColumn("Empresa Encargada");
                            modelo2.addColumn("Nombre del Responsable");
                            modelo2.addColumn("Telefono");
                            modelo2.addColumn("Observaciones para Telepuerto");
                            modelo2.addColumn("Acciones de Correcion de Falla");
    
                            res = operaciones.mostrar_averias_cerradas();

                               try {   
                                   while (res.next()) {

                                        modelo2.addRow(new Object[]{ res.getString("n_pedido"), res.getString("nombre"), res.getString("region"), res.getString("edo"), res.getString("municipio"),res.getString("parroquia"), res.getString("direccion_completa"),res.getString("nombre_del_cliente_principal"), res.getString("tipo_sector"), res.getString("sector"),res.getString("mac"), res.getString("despliegue_ip"),  res.getString("ipoam"),  res.getString("qos_operativo"),  res.getString("telepuerto"),res.getString("circuito"), res.getString("estatus_red"), res.getString("empresa_instaladora"),res.getString("codigo_de_cierre"), res.getString("f_apertura_a"), res.getString("nombre_contacto"), res.getString("telefono_contacto"), res.getString("descripcion_falla"),res.getString("causa_falla"),res.getString("o_operativas"),res.getString("f_asignacion"),res.getString("responsable_reparacion"),res.getString("nombre_reparador"),res.getString("telefono_contacto_r"),res.getString("obser_p_tele"),res.getString("acciones_corretivas")});
                                   }
                               } catch (Exception e) {

                                   JOptionPane.showMessageDialog(null, "ERROR" + e);

                               }


                                //aqui para asignarle un tamñao a cada columna individualmente
                                TableColumnModel columnModel2 = jt_vista_2.getColumnModel();
                                columnModel2.getColumn(0).setPreferredWidth(150);
                                columnModel2.getColumn(1).setPreferredWidth(300);
                                columnModel2.getColumn(2).setPreferredWidth(150);
                                columnModel2.getColumn(3).setPreferredWidth(150);
                                columnModel2.getColumn(4).setPreferredWidth(150);
                                columnModel2.getColumn(5).setPreferredWidth(150);
                                columnModel2.getColumn(6).setPreferredWidth(150);
                                columnModel2.getColumn(7).setPreferredWidth(250);
                                columnModel2.getColumn(8).setPreferredWidth(150);
                                columnModel2.getColumn(9).setPreferredWidth(150);
                                columnModel2.getColumn(10).setPreferredWidth(150);
                                columnModel2.getColumn(11).setPreferredWidth(150);
                                columnModel2.getColumn(12).setPreferredWidth(150);
                                columnModel2.getColumn(13).setPreferredWidth(150);
                                columnModel2.getColumn(14).setPreferredWidth(150);
                                columnModel2.getColumn(15).setPreferredWidth(150);
                                columnModel2.getColumn(16).setPreferredWidth(150);
                                columnModel2.getColumn(17).setPreferredWidth(150);
                                columnModel2.getColumn(18).setPreferredWidth(150);
                                columnModel2.getColumn(19).setPreferredWidth(150);
                                columnModel2.getColumn(20).setPreferredWidth(150);
                                columnModel2.getColumn(21).setPreferredWidth(150);
                                columnModel2.getColumn(22).setPreferredWidth(250);
                                columnModel2.getColumn(23).setPreferredWidth(250);
                                columnModel2.getColumn(24).setPreferredWidth(250);
                                columnModel2.getColumn(25).setPreferredWidth(150);
                                columnModel2.getColumn(26).setPreferredWidth(150);
                                columnModel2.getColumn(27).setPreferredWidth(150);
                                columnModel2.getColumn(28).setPreferredWidth(150);
                                columnModel2.getColumn(29).setPreferredWidth(250);
                                columnModel2.getColumn(30).setPreferredWidth(250);
   
   
   }
     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jp_table = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jt_vista_2 = new javax.swing.JTable();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jp_table.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Vista Previa", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 14), new java.awt.Color(0, 0, 204))); // NOI18N
        jp_table.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jt_vista_2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jt_vista_2);

        jp_table.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 800, 260));

        add(jp_table, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 820, 300));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel jp_table;
    public static javax.swing.JTable jt_vista_2;
    // End of variables declaration//GEN-END:variables
}
