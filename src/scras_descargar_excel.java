
import java.io.FileOutputStream;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



/**
 *
 * @author Dayana Martinez <dmaro006@gmail.com>
 */
public class scras_descargar_excel {
   jp_averias_abiertas jpaa = new jp_averias_abiertas();
   jp_averias_cerradas jac = new jp_averias_cerradas();
   
    DefaultTableModel modelo;
  
    
      public  void exel_xlsx_averias_abiertas(String ruta, String titulo_1, String titulo_2){
        
        
        modelo = (DefaultTableModel)jpaa.jt_vista.getModel() ;
        
        
       //creamos el libro 
        XSSFWorkbook libro = new XSSFWorkbook();
        XSSFSheet hoja = libro.createSheet();
       
        // para el estilo de la fuente
        XSSFFont fuente = libro.createFont();
           //estilo fuente 1
        fuente.setColor(XSSFFont.COLOR_NORMAL);
        fuente.setFontName("Arial");
        fuente.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        
      
       
        //para el estilo de las celdas
        XSSFCellStyle estiloCelda = libro.createCellStyle();
        estiloCelda.setFillForegroundColor(XSSFCellStyle.BORDER_DASH_DOT_DOT);
        estiloCelda.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
          //estilo celda 1
          estiloCelda.setFont(fuente);
        
        
      // tamaño de las columnas
        hoja.setColumnWidth(0, 250 * 40);
        hoja.setColumnWidth(1, 250 * 40);
        hoja.setColumnWidth(2, 250 * 40);
        hoja.setColumnWidth(3, 250 * 40);
        hoja.setColumnWidth(4, 250 * 40);
        hoja.setColumnWidth(5, 250 * 40);
        hoja.setColumnWidth(6, 250 * 40);
        hoja.setColumnWidth(7, 250 * 40);
        hoja.setColumnWidth(8, 250 * 40);
        hoja.setColumnWidth(9, 250 * 40);
        hoja.setColumnWidth(10, 250 * 40);
        hoja.setColumnWidth(11, 250 * 40);
        hoja.setColumnWidth(12, 250 * 40);
        hoja.setColumnWidth(13, 250 * 40);
        hoja.setColumnWidth(14, 250 * 40);
        hoja.setColumnWidth(15, 250 * 40);
        hoja.setColumnWidth(16, 250 * 40);
        hoja.setColumnWidth(17, 250 * 40);
        hoja.setColumnWidth(18, 250 * 40);
        hoja.setColumnWidth(19, 250 * 40);
        hoja.setColumnWidth(20, 250 * 40);
        hoja.setColumnWidth(21, 250 * 40);
        hoja.setColumnWidth(22, 250 * 40);
        hoja.setColumnWidth(23, 250 * 40);
        hoja.setColumnWidth(24, 250 * 40);
           
        XSSFRow fila3 = hoja.createRow(2);
        XSSFCell celda3 = fila3.createCell(0);
        fila3 = hoja.createRow(2);
        celda3= fila3.createCell(0);
//        celda3.setCellStyle(estiloCelda);
        celda3.setCellValue(titulo_1);
        
      //  "Estatus Averias Reportadas SCRAS"
        XSSFRow fila4 = hoja.createRow(3);
        XSSFCell celda4 = fila4.createCell(0);
        fila4 = hoja.createRow(3);
        celda4= fila4.createCell(0);
//        celda4.setCellStyle(estiloCelda);
        celda4.setCellValue(titulo_2);//"REGION ORIENTE ( Bolivar - Delta Amacuro - Monagas ) "
        
 //INICIO CREACION DE FILAS Y CELDAS PARA LOS DATOS 
        XSSFRow fila2 = hoja.createRow(5);
        
        
        
        
        XSSFCell celda2 = fila2.createCell(0);
        fila2 = hoja.createRow(5);
        
        
      
        celda2 = fila2.createCell(0);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("PEDIDO");
        celda2 = fila2.createCell(1);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("NOMBRE  ");
        celda2 = fila2.createCell(2);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("REGION ");
        celda2 = fila2.createCell(3);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("ESTADO  ");
         celda2 = fila2.createCell(4);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MUNICIPIO ");
         celda2 = fila2.createCell(5);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("PARROQUIA ");
         celda2 = fila2.createCell(6);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("DIRECCION");
         celda2 = fila2.createCell(7);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("NOMBRE DEL CLIENTE COMPLETO");
        celda2 = fila2.createCell(8);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TIPO SECTOR  ");
        celda2 = fila2.createCell(9);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("SECTOR ");
        celda2 = fila2.createCell(10);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MAC  ");
         celda2 = fila2.createCell(11);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("IP ");
         celda2 = fila2.createCell(12);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MASCARA ");
         celda2 = fila2.createCell(13);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("QoS");
         celda2 = fila2.createCell(14);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TELEPUERTO");
        celda2 = fila2.createCell(15);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CIRCUITO  ");
        celda2 = fila2.createCell(16);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("ESTATUS DE RED ");
        celda2 = fila2.createCell(17);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("EMPRESA INSTALADORA  ");
         celda2 = fila2.createCell(18);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CODIGO DE CIERRE ");
         celda2 = fila2.createCell(19);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("FECHA REPORTE ");
         celda2 = fila2.createCell(20);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CONTACTO REPORTE");
        celda2 = fila2.createCell(21);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TELEFONO CONTACTO  ");
         celda2 = fila2.createCell(22);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("DESCRIPCION FALLA");
         celda2 = fila2.createCell(23);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CAUSA FALLA ");
         celda2 = fila2.createCell(24);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("OBSERVACIONES OPERATIVAS");
        
       
    
        for(int i = 0; i <= modelo.getRowCount() -1; i++){
            fila2 = hoja.createRow(i + 6); // se crea la fila
            for(int j = 0; j <= 24; j++){
                celda2 = fila2.createCell(j);// se crea la celda
               
                celda2.setCellValue(jpaa.jt_vista.getValueAt(i, j).toString());
            }  
        }
        
       //FIN CREACION DE FILAS Y CELDAS PARA LOS DATOS DE LOS ALMUMNOS
       
        
        try{
            FileOutputStream Fichero = new FileOutputStream(ruta);
            
           
            libro.write(Fichero);//se crea el fichero
            Fichero.close();//se cierra el fichero            
        }catch(Exception e){
            
            System.out.println(e);
        }
    }   
    
    
    
    
    
      public  void exel_xls_averias_abiertas(String ruta,String titulo_1,String titulo_2){
        
        
        modelo = (DefaultTableModel)jpaa.jt_vista.getModel() ;
        
        
       //creamos el libro 
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
       
        // para el estilo de la fuente
        HSSFFont fuente = libro.createFont();
           //estilo fuente 1
        fuente.setColor(HSSFFont.COLOR_NORMAL);
        fuente.setFontName("Arial");
        fuente.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        
      
       
        //para el estilo de las celdas
        HSSFCellStyle estiloCelda = libro.createCellStyle();
        estiloCelda.setFillForegroundColor(HSSFCellStyle.BORDER_DASH_DOT_DOT);
        estiloCelda.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
          //estilo celda 1
          estiloCelda.setFont(fuente);
        
        
      // tamaño de las columnas
        hoja.setColumnWidth(0, 250 * 40);
        hoja.setColumnWidth(1, 250 * 40);
        hoja.setColumnWidth(2, 250 * 40);
        hoja.setColumnWidth(3, 250 * 40);
        hoja.setColumnWidth(4, 250 * 40);
        hoja.setColumnWidth(5, 250 * 40);
        hoja.setColumnWidth(6, 250 * 40);
        hoja.setColumnWidth(7, 250 * 40);
        hoja.setColumnWidth(8, 250 * 40);
        hoja.setColumnWidth(9, 250 * 40);
        hoja.setColumnWidth(10, 250 * 40);
        hoja.setColumnWidth(11, 250 * 40);
        hoja.setColumnWidth(12, 250 * 40);
        hoja.setColumnWidth(13, 250 * 40);
        hoja.setColumnWidth(14, 250 * 40);
        hoja.setColumnWidth(15, 250 * 40);
        hoja.setColumnWidth(16, 250 * 40);
        hoja.setColumnWidth(17, 250 * 40);
        hoja.setColumnWidth(18, 250 * 40);
        hoja.setColumnWidth(19, 250 * 40);
        hoja.setColumnWidth(20, 250 * 40);
        hoja.setColumnWidth(21, 250 * 40);
        hoja.setColumnWidth(22, 250 * 40);
        hoja.setColumnWidth(23, 250 * 40);
        hoja.setColumnWidth(24, 250 * 40);
           
        HSSFRow fila3 = hoja.createRow(2);
        HSSFCell celda3 = fila3.createCell(0);
        fila3 = hoja.createRow(2);
        celda3= fila3.createCell(0);
//        celda3.setCellStyle(estiloCelda);
        celda3.setCellValue(titulo_1);
        
        
         HSSFRow fila4 = hoja.createRow(3);
        HSSFCell celda4 = fila4.createCell(0);
        fila4 = hoja.createRow(3);
        celda4= fila4.createCell(0);
//        celda4.setCellStyle(estiloCelda);
        celda4.setCellValue(titulo_2);
        
 //INICIO CREACION DE FILAS Y CELDAS PARA LOS DATOS 
        HSSFRow fila2 = hoja.createRow(5);
        
        
        
        
        HSSFCell celda2 = fila2.createCell(0);
        fila2 = hoja.createRow(5);
        
        
      
        celda2 = fila2.createCell(0);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("PEDIDO");
        celda2 = fila2.createCell(1);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("NOMBRE  ");
        celda2 = fila2.createCell(2);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("REGION ");
        celda2 = fila2.createCell(3);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("ESTADO  ");
         celda2 = fila2.createCell(4);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MUNICIPIO ");
         celda2 = fila2.createCell(5);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("PARROQUIA ");
         celda2 = fila2.createCell(6);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("DIRECCION");
         celda2 = fila2.createCell(7);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("NOMBRE DEL CLIENTE COMPLETO");
        celda2 = fila2.createCell(8);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TIPO SECTOR  ");
        celda2 = fila2.createCell(9);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("SECTOR ");
        celda2 = fila2.createCell(10);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MAC  ");
         celda2 = fila2.createCell(11);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("IP ");
         celda2 = fila2.createCell(12);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MASCARA ");
         celda2 = fila2.createCell(13);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("QoS");
         celda2 = fila2.createCell(14);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TELEPUERTO");
        celda2 = fila2.createCell(15);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CIRCUITO  ");
        celda2 = fila2.createCell(16);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("ESTATUS DE RED ");
        celda2 = fila2.createCell(17);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("EMPRESA INSTALADORA  ");
         celda2 = fila2.createCell(18);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CODIGO DE CIERRE ");
         celda2 = fila2.createCell(19);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("FECHA REPORTE ");
         celda2 = fila2.createCell(20);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CONTACTO REPORTE");
        celda2 = fila2.createCell(21);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TELEFONO CONTACTO  ");
         celda2 = fila2.createCell(22);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("DESCRIPCION FALLA");
         celda2 = fila2.createCell(23);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CAUSA FALLA ");
         celda2 = fila2.createCell(24);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("OBSERVACIONES OPERATIVAS");
        
       
    
        for(int i = 0; i <= modelo.getRowCount() -1; i++){
            fila2 = hoja.createRow(i + 6); // se crea la fila
            for(int j = 0; j <= 24; j++){
                celda2 = fila2.createCell(j);// se crea la celda
               
                celda2.setCellValue(jpaa.jt_vista.getValueAt(i, j).toString());
            }  
        }
        
       //FIN CREACION DE FILAS Y CELDAS PARA LOS DATOS DE LOS ALMUMNOS
       
        
        try{
            FileOutputStream Fichero = new FileOutputStream(ruta);
            
           
            libro.write(Fichero);//se crea el fichero
            Fichero.close();//se cierra el fichero            
        }catch(Exception e){
            
            System.out.println(e);
        }
    }   
  
      
      
      
      
      
    
      public  void exel_xlsx_averias_cerradas(String ruta){
        
        
        modelo = (DefaultTableModel)jac.jt_vista_2.getModel() ;
        
        
       //creamos el libro 
        XSSFWorkbook libro = new XSSFWorkbook();
        XSSFSheet hoja = libro.createSheet();
       
        // para el estilo de la fuente
        XSSFFont fuente = libro.createFont();
           //estilo fuente 1
        fuente.setColor(XSSFFont.COLOR_NORMAL);
        fuente.setFontName("Arial");
        fuente.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        
      
       
        //para el estilo de las celdas
        XSSFCellStyle estiloCelda = libro.createCellStyle();
        estiloCelda.setFillForegroundColor(XSSFCellStyle.BORDER_DASH_DOT_DOT);
        estiloCelda.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
          //estilo celda 1
          estiloCelda.setFont(fuente);
        
        
      // tamaño de las columnas
        hoja.setColumnWidth(0, 250 * 40);
        hoja.setColumnWidth(1, 250 * 40);
        hoja.setColumnWidth(2, 250 * 40);
        hoja.setColumnWidth(3, 250 * 40);
        hoja.setColumnWidth(4, 250 * 40);
        hoja.setColumnWidth(5, 250 * 40);
        hoja.setColumnWidth(6, 250 * 40);
        hoja.setColumnWidth(7, 250 * 40);
        hoja.setColumnWidth(8, 250 * 40);
        hoja.setColumnWidth(9, 250 * 40);
        hoja.setColumnWidth(10, 250 * 40);
        hoja.setColumnWidth(11, 250 * 40);
        hoja.setColumnWidth(12, 250 * 40);
        hoja.setColumnWidth(13, 250 * 40);
        hoja.setColumnWidth(14, 250 * 40);
        hoja.setColumnWidth(15, 250 * 40);
        hoja.setColumnWidth(16, 250 * 40);
        hoja.setColumnWidth(17, 250 * 40);
        hoja.setColumnWidth(18, 250 * 40);
        hoja.setColumnWidth(19, 250 * 40);
        hoja.setColumnWidth(20, 250 * 40);
        hoja.setColumnWidth(21, 250 * 40);
        hoja.setColumnWidth(22, 250 * 40);
        hoja.setColumnWidth(23, 250 * 40);
        hoja.setColumnWidth(24, 250 * 40);
        hoja.setColumnWidth(25, 250 * 40);
        hoja.setColumnWidth(26, 250 * 40);
        hoja.setColumnWidth(27, 250 * 40);
        hoja.setColumnWidth(28, 250 * 40);
        hoja.setColumnWidth(29, 250 * 40);
        hoja.setColumnWidth(30, 300 * 40);
           
        XSSFRow fila3 = hoja.createRow(2);
        XSSFCell celda3 = fila3.createCell(0);
        fila3 = hoja.createRow(2);
        celda3= fila3.createCell(0);
//        celda3.setCellStyle(estiloCelda);
        celda3.setCellValue("Estatus Averias Reportadas SCRAS");
        
        
        XSSFRow fila4 = hoja.createRow(3);
        XSSFCell celda4 = fila4.createCell(0);
        fila4 = hoja.createRow(3);
        celda4= fila4.createCell(0);
//        celda4.setCellStyle(estiloCelda);
        celda4.setCellValue("REGION ORIENTE ( Bolivar - Delta Amacuro - Monagas ) ");
        
 //INICIO CREACION DE FILAS Y CELDAS PARA LOS DATOS 
        XSSFRow fila2 = hoja.createRow(5);
        
        
        
        
        XSSFCell celda2 = fila2.createCell(0);
        fila2 = hoja.createRow(5);
        
        
      
        celda2 = fila2.createCell(0);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("PEDIDO");
        celda2 = fila2.createCell(1);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("NOMBRE  ");
        celda2 = fila2.createCell(2);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("REGION ");
        celda2 = fila2.createCell(3);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("ESTADO  ");
         celda2 = fila2.createCell(4);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MUNICIPIO ");
         celda2 = fila2.createCell(5);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("PARROQUIA ");
         celda2 = fila2.createCell(6);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("DIRECCION");
         celda2 = fila2.createCell(7);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("NOMBRE DEL CLIENTE COMPLETO");
        celda2 = fila2.createCell(8);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TIPO SECTOR  ");
        celda2 = fila2.createCell(9);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("SECTOR ");
        celda2 = fila2.createCell(10);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MAC  ");
         celda2 = fila2.createCell(11);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("IP ");
         celda2 = fila2.createCell(12);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MASCARA ");
         celda2 = fila2.createCell(13);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("QoS");
         celda2 = fila2.createCell(14);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TELEPUERTO");
        celda2 = fila2.createCell(15);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CIRCUITO  ");
        celda2 = fila2.createCell(16);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("ESTATUS DE RED ");
        celda2 = fila2.createCell(17);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("EMPRESA INSTALADORA  ");
         celda2 = fila2.createCell(18);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CODIGO DE CIERRE ");
         celda2 = fila2.createCell(19);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("FECHA REPORTE ");
         celda2 = fila2.createCell(20);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CONTACTO REPORTE");
        celda2 = fila2.createCell(21);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TELEFONO CONTACTO  ");
         celda2 = fila2.createCell(22);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("DESCRIPCION FALLA");
         celda2 = fila2.createCell(23);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CAUSA FALLA ");
         celda2 = fila2.createCell(24);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("OBSERVACIONES OPERATIVAS");
          celda2 = fila2.createCell(25);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("FECHA ATENCION  ");
        celda2 = fila2.createCell(26);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("EMPRESA ENCARGADA ");
        celda2 = fila2.createCell(27);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("NOMBRE DEL RESPONSABLE  ");
         celda2 = fila2.createCell(28);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TELEFONO ");
         celda2 = fila2.createCell(29);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("OBSERVACIONES TELEPUERTO ");
         celda2 = fila2.createCell(30);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("ACCCIONES DE CORRECCION DE FALLA");
        
       
    
        for(int i = 0; i <= modelo.getRowCount() -1; i++){
            fila2 = hoja.createRow(i + 6); // se crea la fila
            for(int j = 0; j <= 24; j++){
                celda2 = fila2.createCell(j);// se crea la celda
               
                celda2.setCellValue(jac.jt_vista_2.getValueAt(i, j).toString());
            }  
        }
        
       //FIN CREACION DE FILAS Y CELDAS PARA LOS DATOS DE LOS ALMUMNOS
       
        
        try{
            FileOutputStream Fichero = new FileOutputStream(ruta);
            
           
            libro.write(Fichero);//se crea el fichero
            Fichero.close();//se cierra el fichero            
        }catch(Exception e){
            
            System.out.println(e);
        }
    }   
    
    
    
    
    
      public  void exel_xls_averias_cerradas(String ruta){
        
        
        modelo = (DefaultTableModel)jac.jt_vista_2.getModel() ;
        
        
       //creamos el libro 
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
       
        // para el estilo de la fuente
        HSSFFont fuente = libro.createFont();
           //estilo fuente 1
        fuente.setColor(HSSFFont.COLOR_NORMAL);
        fuente.setFontName("Arial");
        fuente.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        
      
       
        //para el estilo de las celdas
        HSSFCellStyle estiloCelda = libro.createCellStyle();
        estiloCelda.setFillForegroundColor(HSSFCellStyle.BORDER_DASH_DOT_DOT);
        estiloCelda.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
          //estilo celda 1
          estiloCelda.setFont(fuente);
        
        
      // tamaño de las columnas
        hoja.setColumnWidth(0, 250 * 40);
        hoja.setColumnWidth(1, 250 * 40);
        hoja.setColumnWidth(2, 250 * 40);
        hoja.setColumnWidth(3, 250 * 40);
        hoja.setColumnWidth(4, 250 * 40);
        hoja.setColumnWidth(5, 250 * 40);
        hoja.setColumnWidth(6, 250 * 40);
        hoja.setColumnWidth(7, 250 * 40);
        hoja.setColumnWidth(8, 250 * 40);
        hoja.setColumnWidth(9, 250 * 40);
        hoja.setColumnWidth(10, 250 * 40);
        hoja.setColumnWidth(11, 250 * 40);
        hoja.setColumnWidth(12, 250 * 40);
        hoja.setColumnWidth(13, 250 * 40);
        hoja.setColumnWidth(14, 250 * 40);
        hoja.setColumnWidth(15, 250 * 40);
        hoja.setColumnWidth(16, 250 * 40);
        hoja.setColumnWidth(17, 250 * 40);
        hoja.setColumnWidth(18, 250 * 40);
        hoja.setColumnWidth(19, 250 * 40);
        hoja.setColumnWidth(20, 250 * 40);
        hoja.setColumnWidth(21, 250 * 40);
        hoja.setColumnWidth(22, 250 * 40);
        hoja.setColumnWidth(23, 250 * 40);
        hoja.setColumnWidth(24, 250 * 40);
           
        HSSFRow fila3 = hoja.createRow(2);
        HSSFCell celda3 = fila3.createCell(0);
        fila3 = hoja.createRow(2);
        celda3= fila3.createCell(0);
//        celda3.setCellStyle(estiloCelda);
        celda3.setCellValue("Estatus Averias Reportadas SCRAS");
        
        
         HSSFRow fila4 = hoja.createRow(3);
        HSSFCell celda4 = fila4.createCell(0);
        fila4 = hoja.createRow(3);
        celda4= fila4.createCell(0);
//        celda4.setCellStyle(estiloCelda);
        celda4.setCellValue("REGION ORIENTE ( Bolivar - Delta Amacuro - Monagas ) ");
        
 //INICIO CREACION DE FILAS Y CELDAS PARA LOS DATOS 
        HSSFRow fila2 = hoja.createRow(5);
        
        
        
        
        HSSFCell celda2 = fila2.createCell(0);
        fila2 = hoja.createRow(5);
        
        
      
        celda2 = fila2.createCell(0);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("PEDIDO");
        celda2 = fila2.createCell(1);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("NOMBRE  ");
        celda2 = fila2.createCell(2);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("REGION ");
        celda2 = fila2.createCell(3);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("ESTADO  ");
         celda2 = fila2.createCell(4);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MUNICIPIO ");
         celda2 = fila2.createCell(5);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("PARROQUIA ");
         celda2 = fila2.createCell(6);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("DIRECCION");
         celda2 = fila2.createCell(7);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("NOMBRE DEL CLIENTE COMPLETO");
        celda2 = fila2.createCell(8);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TIPO SECTOR  ");
        celda2 = fila2.createCell(9);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("SECTOR ");
        celda2 = fila2.createCell(10);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MAC  ");
         celda2 = fila2.createCell(11);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("IP ");
         celda2 = fila2.createCell(12);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("MASCARA ");
         celda2 = fila2.createCell(13);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("QoS");
         celda2 = fila2.createCell(14);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TELEPUERTO");
        celda2 = fila2.createCell(15);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CIRCUITO  ");
        celda2 = fila2.createCell(16);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("ESTATUS DE RED ");
        celda2 = fila2.createCell(17);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("EMPRESA INSTALADORA  ");
         celda2 = fila2.createCell(18);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CODIGO DE CIERRE ");
         celda2 = fila2.createCell(19);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("FECHA REPORTE ");
         celda2 = fila2.createCell(20);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CONTACTO REPORTE");
        celda2 = fila2.createCell(21);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("TELEFONO CONTACTO  ");
         celda2 = fila2.createCell(22);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("DESCRIPCION FALLA");
         celda2 = fila2.createCell(23);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("CAUSA FALLA ");
         celda2 = fila2.createCell(24);
        celda2.setCellStyle(estiloCelda);
        celda2.setCellValue("OBSERVACIONES OPERATIVAS");
        
       
    
        for(int i = 0; i <= modelo.getRowCount() -1; i++){
            fila2 = hoja.createRow(i + 6); // se crea la fila
            for(int j = 0; j <= 24; j++){
                celda2 = fila2.createCell(j);// se crea la celda
               
                celda2.setCellValue(jac.jt_vista_2.getValueAt(i, j).toString());
            }  
        }
        
       //FIN CREACION DE FILAS Y CELDAS PARA LOS DATOS DE LOS ALMUMNOS
       
        
        try{
            FileOutputStream Fichero = new FileOutputStream(ruta);
            
           
            libro.write(Fichero);//se crea el fichero
            Fichero.close();//se cierra el fichero            
        }catch(Exception e){
            
            System.out.println(e);
        }
    }   

}
