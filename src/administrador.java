
import Control_bd.operaciones_sql;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dayana Martinez <dmaro006@gmail.com>
 */
public class administrador extends javax.swing.JFrame {
    

        public DefaultTableModel modelo = new DefaultTableModel() {
            // codigo para las que las celdas del jtbale no sean editables
            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return false; // el resto de las celdas no son editables

            }

    };

operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
ResultSet res;//creacion de la variable resultset

    public administrador() {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/scras_icono.png")).getImage()); 
        setTitle("SCRAS ADMINISTRADOR");
        tamaño_columnas();    

    }


void tamaño_columnas(){
}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jp_fondo = new javax.swing.JPanel();
        lbl_clave = new javax.swing.JLabel();
        lbl_usuario = new javax.swing.JLabel();
        bot_crear = new javax.swing.JButton();
        txt_usuario = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        bot_consultar = new javax.swing.JButton();
        txt_clave = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SCRAS");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jp_fondo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Nuevo Usuario", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 14))); // NOI18N
        jp_fondo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_clave.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_clave.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_clave.setText("Clave:");
        jp_fondo.add(lbl_clave, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 80, 30));

        lbl_usuario.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_usuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_usuario.setText("Usuario:");
        jp_fondo.add(lbl_usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 80, 30));

        bot_crear.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        bot_crear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/user (1).png"))); // NOI18N
        bot_crear.setToolTipText("Crear Usuario");
        bot_crear.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_crear.setEnabled(false);
        bot_crear.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/user (2).png"))); // NOI18N
        bot_crear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_crearActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_crear, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 50, 90, 70));

        txt_usuario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_usuarioMouseClicked(evt);
            }
        });
        txt_usuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_usuarioKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_usuarioKeyTyped(evt);
            }
        });
        jp_fondo.add(txt_usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 50, 290, 30));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Crear Usuario");
        jp_fondo.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 120, 90, 30));

        bot_consultar.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        bot_consultar.setText("Consultar");
        bot_consultar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_consultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_consultarActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_consultar, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 90, 20, 10));

        txt_clave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_claveMouseClicked(evt);
            }
        });
        txt_clave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_claveKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_claveKeyTyped(evt);
            }
        });
        jp_fondo.add(txt_clave, new org.netbeans.lib.awtextra.AbsoluteConstraints(111, 90, 290, 30));

        getContentPane().add(jp_fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 540, 170));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bot_crearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_crearActionPerformed
        
       
        if (operaciones.insertar_usuario(txt_usuario.getText(),txt_clave.getText())) {
              
            JOptionPane.showMessageDialog(null, "Registro de usuario  Exitoso  ");
            Identificacion interfaz = new Identificacion();
            interfaz.setVisible(true);
            dispose();
            
        } else {

            JOptionPane.showMessageDialog(null, "Registro de usuario no exitoso ");
        }
    }//GEN-LAST:event_bot_crearActionPerformed

    private void txt_usuarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_usuarioMouseClicked
          txt_usuario.setText("");  
          txt_clave.setText(""); 
          
        try{
       for (int i=0;
       i<modelo.getRowCount();i--){
        for (int j=0;
                j<modelo.getRowCount();j--){
           modelo.removeRow(0);
       }
    
     }
     }catch (Exception e)  {
        
     }
         
    }//GEN-LAST:event_txt_usuarioMouseClicked

    private void txt_usuarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_usuarioKeyTyped
        
        char car = evt.getKeyChar();
        if (txt_usuario.getText().length() >= 20) { //la longitud del campo de texto la pones deacuerdo a tu necesidad
            evt.consume();}
    }//GEN-LAST:event_txt_usuarioKeyTyped
public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
String cadena= (jTextfieldS.getText()).toUpperCase();
jTextfieldS.setText(cadena);
}
    private void txt_usuarioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_usuarioKeyReleased
        bot_crear.setVisible(true);
        convertiraMayusculasEnJtextfield(txt_usuario);
    }//GEN-LAST:event_txt_usuarioKeyReleased

    private void bot_consultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_consultarActionPerformed
        //            //  Para el desplazamiento horizoltal del scrollpane en el jtable
        //  jt_usuarios.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //  jt_usuarios.doLayout();
        //   //inabilita el movimiento de las columns   del jtable
        //    jt_usuarios.getTableHeader().setReorderingAllowed(false);
        //    jt_usuarios.getTableHeader().setResizingAllowed(false);
        //
        //    jt_usuarios.setModel(modelo);
        //
        //    modelo.addColumn("id");
        //    modelo.addColumn("usuario");
        //    modelo.addColumn("clave");
        //
        //    res=operaciones.mostrar_usuario();
        //
        //           res = operaciones.mostrar_usuario();
        //
        //        try {
            //            while (res.next()) {
                //
                //                 modelo.addRow(new Object[]{ res.getString("id_user"), res.getString("usuario"), res.getString("clave")});
                //            }
            //        } catch (Exception e) {
            //
            //            JOptionPane.showMessageDialog(null, "ERROR" + e);
            //
            //        }
        //  //aqui para asignarle un tamñao a cada columna individualmente
        //TableColumnModel columnModel = jt_usuarios.getColumnModel();
        //columnModel.getColumn(0).setPreferredWidth(150);
        //columnModel.getColumn(1).setPreferredWidth(150);
        //columnModel.getColumn(2).setPreferredW idth(150);
    }//GEN-LAST:event_bot_consultarActionPerformed

    private void txt_claveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_claveMouseClicked
        bot_crear.setVisible(true);
    }//GEN-LAST:event_txt_claveMouseClicked

    private void txt_claveKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_claveKeyTyped
          bot_crear.setEnabled(true);
    }//GEN-LAST:event_txt_claveKeyTyped

    private void txt_claveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_claveKeyPressed
       if(evt.getKeyCode()==10){
       bot_crear.doClick();}
    }//GEN-LAST:event_txt_claveKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new administrador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bot_consultar;
    private javax.swing.JButton bot_crear;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jp_fondo;
    private javax.swing.JLabel lbl_clave;
    private javax.swing.JLabel lbl_usuario;
    private javax.swing.JPasswordField txt_clave;
    private javax.swing.JTextField txt_usuario;
    // End of variables declaration//GEN-END:variables
}
