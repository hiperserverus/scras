
import Control_bd.operaciones_sql;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author usuario
 */
public class jp_averias_abiertas extends javax.swing.JPanel {

    /** Creates new form jp_averias_abiertas */
    public jp_averias_abiertas() {
        initComponents();
        averias_abiertas();
    }
    
           public DefaultTableModel modelo1 = new DefaultTableModel() {
            // codigo para las que las celdas del jtbale no sean editables
            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return false; // el resto de las celdas no son editables

            }

    };
           
           
operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
ResultSet res;//creacion de la variable resultset
              
    public void averias_abiertas(){
      //  Para el desplazamiento horizoltal del scrollpane en el jtable       
                            jt_vista.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                            jt_vista.doLayout(); 
                            //inabilita el movimiento de las columns   del jtable  
                            jt_vista.getTableHeader().setReorderingAllowed(false);
                            jt_vista.getTableHeader().setResizingAllowed(false);

                            jt_vista.setModel(modelo1);
       
                            modelo1.addColumn("Pedido");
                            modelo1.addColumn("Nombre");
                            modelo1.addColumn("Region");
                            modelo1.addColumn("Estado");
                            modelo1.addColumn("Municipio");
                            modelo1.addColumn("Parroquia");
                            modelo1.addColumn("Direccion");
                            modelo1.addColumn("Nombre del Cliente Principal");
                            modelo1.addColumn("Tipo de Sector");
                            modelo1.addColumn("Sector");
                            modelo1.addColumn("MAC");
                            modelo1.addColumn("IP");
                            modelo1.addColumn("MASCARA");
                            modelo1.addColumn("QoS");
                            modelo1.addColumn("Telepuerto");
                            modelo1.addColumn("Circuito");
                            modelo1.addColumn("Estatus de la Red");
                            modelo1.addColumn("Empresa Instaladora");
                            modelo1.addColumn("Codigo de Cierre");
                            modelo1.addColumn("Fecha de Reporte");
                            modelo1.addColumn("Contacto de Reporte");
                            modelo1.addColumn("Telefono de Contacto");
                            modelo1.addColumn("Descripcion de la Falla");
                            modelo1.addColumn("Causa de la Falla");
                            modelo1.addColumn("Observaciones Operativas");

                             res = operaciones.mostrar_averias();

                                try {   
                                    while (res.next()) {

                                         modelo1.addRow(new Object[]{ res.getString("n_pedido"), res.getString("nombre"), res.getString("region"), res.getString("edo"), res.getString("municipio"),res.getString("parroquia"), res.getString("direccion_completa"),res.getString("nombre_del_cliente_principal"), res.getString("tipo_sector"), res.getString("sector"),res.getString("mac"), res.getString("despliegue_ip"),  res.getString("ipoam"),  res.getString("qos_operativo"),  res.getString("telepuerto"),res.getString("circuito"), res.getString("estatus_red"), res.getString("empresa_instaladora"),res.getString("codigo_de_cierre"), res.getString("f_apertura_a"), res.getString("nombre_contacto"), res.getString("telefono_contacto"), res.getString("descripcion_falla"),res.getString("causa_falla"),res.getString("o_operativas")});
                                    }
                                } catch (Exception e) {

                                    JOptionPane.showMessageDialog(null, "ERROR" + e);

                                    }

                            //aqui para asignarle un tamñao a cada columna individualmente
                            TableColumnModel columnModel = jt_vista.getColumnModel();
                            columnModel.getColumn(0).setPreferredWidth(150);
                            columnModel.getColumn(1).setPreferredWidth(300);
                            columnModel.getColumn(2).setPreferredWidth(150);
                            columnModel.getColumn(3).setPreferredWidth(150);
                            columnModel.getColumn(4).setPreferredWidth(150);
                            columnModel.getColumn(5).setPreferredWidth(150);
                            columnModel.getColumn(6).setPreferredWidth(150);
                            columnModel.getColumn(7).setPreferredWidth(250);
                            columnModel.getColumn(8).setPreferredWidth(150);
                            columnModel.getColumn(9).setPreferredWidth(150);
                            columnModel.getColumn(10).setPreferredWidth(150);
                            columnModel.getColumn(11).setPreferredWidth(150);
                            columnModel.getColumn(12).setPreferredWidth(150);
                            columnModel.getColumn(13).setPreferredWidth(150);
                            columnModel.getColumn(14).setPreferredWidth(150);
                            columnModel.getColumn(15).setPreferredWidth(150);
                            columnModel.getColumn(16).setPreferredWidth(150);
                            columnModel.getColumn(17).setPreferredWidth(150);
                            columnModel.getColumn(18).setPreferredWidth(150);
                            columnModel.getColumn(19).setPreferredWidth(150);
                            columnModel.getColumn(20).setPreferredWidth(150);
                            columnModel.getColumn(21).setPreferredWidth(150);
                            columnModel.getColumn(22).setPreferredWidth(250);
                            columnModel.getColumn(23).setPreferredWidth(250);
                            columnModel.getColumn(24).setPreferredWidth(250);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jp_table = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jt_vista = new javax.swing.JTable();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jp_table.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Vista Previa", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 14), new java.awt.Color(0, 0, 204))); // NOI18N
        jp_table.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jt_vista.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jt_vista);

        jp_table.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 800, 260));

        add(jp_table, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 820, 300));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel jp_table;
    public static javax.swing.JTable jt_vista;
    // End of variables declaration//GEN-END:variables

}
