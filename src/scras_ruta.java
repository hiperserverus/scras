import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Control_bd.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author usuario
 */
public class scras_ruta extends javax.swing.JFrame {

    public DefaultTableModel modelo_tabla = new DefaultTableModel() {
            // codigo para las que las celdas del jtbale no sean editables
            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return false; // el resto de las celdas no son editables

            }

    };
    
       public DefaultTableModel modelo_tabla2 = new DefaultTableModel() {
            // codigo para las que las celdas del jtbale no sean editables
            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return false; // el resto de las celdas no son editables

            }

    };
    
operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
ResultSet res;//creacion de la variable resultset
ResultSet res2;


    public scras_ruta() {
        initComponents();
                this.setResizable(false);
        this.setLocationRelativeTo(null);
       mostrar_averias();
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/scras_icono.png")).getImage());     



    }

  // PROCESO   MOSTRAR LA LISTA DE la bd
   public void mostrar_averias() { 

    //  Para el desplazamiento horizoltal del scrollpane en el jtable       
  jt_rutas2.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
  jt_rutas2.doLayout(); 
   //inabilita el movimiento de las columns   del jtable  
    jt_rutas2.getTableHeader().setReorderingAllowed(false);
    jt_rutas2.getTableHeader().setResizingAllowed(false);
        
    jt_rutas2.setModel( modelo_tabla);
       
    modelo_tabla.addColumn("Pedido");
    modelo_tabla.addColumn("Nombre");
     modelo_tabla.addColumn("Region");
   modelo_tabla.addColumn("Estado");
     modelo_tabla.addColumn("Municipio");
   modelo_tabla.addColumn("Parroquia");
   modelo_tabla.addColumn("Direccion");
   modelo_tabla.addColumn("Nombre del Cliente Principal");
   modelo_tabla.addColumn("Tipo de Sector");
     modelo_tabla.addColumn("Sector");
     modelo_tabla.addColumn("MAC");
    modelo_tabla.addColumn("IP");
     modelo_tabla.addColumn("MASCARA");
     modelo_tabla.addColumn("QoS");
     modelo_tabla.addColumn("Telepuerto");
     modelo_tabla.addColumn("Circuito");
     modelo_tabla.addColumn("Estatus de la Red");
     modelo_tabla.addColumn("Empresa Instaladora");
    modelo_tabla.addColumn("Codigo de Cierre");
     modelo_tabla.addColumn("Fecha de Reporte");
     modelo_tabla.addColumn("Contacto de Reporte");
     modelo_tabla.addColumn("Telefono de Contacto");
     modelo_tabla.addColumn("Descripcion de la Falla");
     modelo_tabla.addColumn("Causa de la Falla");
     modelo_tabla.addColumn("Observaciones Operativas");
    
//     res = operaciones.mostrar_averias();
//       
//        try {   
//            while (res.next()) {
//                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
//                 model.addRow(new Object[]{ res.getString("pedido"), res.getString("nombre"), res.getString("region"), res.getString("edo"), res.getString("municipio"),res.getString("parroquia"), res.getString("direccion_completa"),res.getString("nombre_del_cliente_principal"), res.getString("tipo_sector"), res.getString("sector"),res.getString("mac"), res.getString("despliegue_ip"),  res.getString("ipoam"),  res.getString("qos_operativo"),  res.getString("telepuerto"),res.getString("circuito"), res.getString("estatus_red"), res.getString("empresa_instaladora"),res.getString("codigo_de_cierre"), res.getString("f_apertura_a"), res.getString("nombre_contacto"), res.getString("telefono_contacto"), res.getString("descripcion_falla"),res.getString("causa_falla"),res.getString("o_operativas")});
//            }
//        } catch (Exception e) {
//
//            JOptionPane.showMessageDialog(null, "ERROR" + e);
//
//        }
     
         TableColumnModel columnModel = jt_rutas2.getColumnModel();
columnModel.getColumn(0).setPreferredWidth(150);
columnModel.getColumn(1).setPreferredWidth(300);
columnModel.getColumn(2).setPreferredWidth(150);
columnModel.getColumn(3).setPreferredWidth(150);
columnModel.getColumn(4).setPreferredWidth(150);
columnModel.getColumn(5).setPreferredWidth(150);
columnModel.getColumn(6).setPreferredWidth(150);
columnModel.getColumn(7).setPreferredWidth(250);
columnModel.getColumn(8).setPreferredWidth(150);
columnModel.getColumn(9).setPreferredWidth(150);
columnModel.getColumn(10).setPreferredWidth(150);
columnModel.getColumn(11).setPreferredWidth(150);
columnModel.getColumn(12).setPreferredWidth(150);
columnModel.getColumn(13).setPreferredWidth(150);
columnModel.getColumn(14).setPreferredWidth(150);
columnModel.getColumn(15).setPreferredWidth(150);
columnModel.getColumn(16).setPreferredWidth(150);
columnModel.getColumn(17).setPreferredWidth(150);
columnModel.getColumn(18).setPreferredWidth(150);
columnModel.getColumn(19).setPreferredWidth(150);
columnModel.getColumn(20).setPreferredWidth(150);
columnModel.getColumn(21).setPreferredWidth(150);
columnModel.getColumn(22).setPreferredWidth(250);
columnModel.getColumn(23).setPreferredWidth(250);
columnModel.getColumn(24).setPreferredWidth(250);
   }
   
       //METODO PARA CAMBIAR EL TAMAÑO DE LAS COLUMUNAS
void tamaño_columnas(){

}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jp_fondo = new javax.swing.JPanel();
        lbl_titulo = new javax.swing.JLabel();
        jp_tabla = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jt_rutas2 = new javax.swing.JTable();
        lbl_volver = new javax.swing.JLabel();
        txt_muni = new javax.swing.JTextField();
        txt_edo = new javax.swing.JTextField();
        bot_buscar = new javax.swing.JButton();
        lbl_municipio = new javax.swing.JLabel();
        lbl_edo = new javax.swing.JLabel();
        bot_registro = new javax.swing.JButton();
        lbl_regis = new javax.swing.JLabel();
        lbl_volver3 = new javax.swing.JLabel();
        bot_volver = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SCRAS");
        setMinimumSize(new java.awt.Dimension(920, 480));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jp_fondo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_titulo.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        lbl_titulo.setText("Generar Rutas de Control");
        jp_fondo.add(lbl_titulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 250, 20));

        jp_tabla.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ruta de averias Reportadas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 14))); // NOI18N
        jp_tabla.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jt_rutas2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(jt_rutas2);

        jp_tabla.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 760, 280));

        jp_fondo.add(jp_tabla, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 780, 310));

        lbl_volver.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_volver.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_volver.setText("Descargar");
        jp_fondo.add(lbl_volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 190, 90, 30));

        txt_muni.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txt_muni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_muniActionPerformed(evt);
            }
        });
        txt_muni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_muniKeyReleased(evt);
            }
        });
        jp_fondo.add(txt_muni, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 90, 200, 30));

        txt_edo.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txt_edo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_edoMouseClicked(evt);
            }
        });
        txt_edo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_edoKeyReleased(evt);
            }
        });
        jp_fondo.add(txt_edo, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 50, 200, 30));

        bot_buscar.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        bot_buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar (1).png"))); // NOI18N
        bot_buscar.setContentAreaFilled(false);
        bot_buscar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_buscar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar (2).png"))); // NOI18N
        bot_buscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bot_buscarMouseClicked(evt);
            }
        });
        bot_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_buscarActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_buscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 80, 60, 60));

        lbl_municipio.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_municipio.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_municipio.setText("Solo el Municipio:");
        jp_fondo.add(lbl_municipio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 120, 30));

        lbl_edo.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_edo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_edo.setText("Estado:");
        jp_fondo.add(lbl_edo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 110, 30));

        bot_registro.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        bot_registro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar.png"))); // NOI18N
        bot_registro.setContentAreaFilled(false);
        bot_registro.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_registro.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar2.png"))); // NOI18N
        bot_registro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_registroActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_registro, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 230, 80, 80));

        lbl_regis.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_regis.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_regis.setText("Registrar");
        jp_fondo.add(lbl_regis, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 290, 90, 30));

        lbl_volver3.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_volver3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_volver3.setText("Volver ");
        jp_fondo.add(lbl_volver3, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 390, 90, 30));

        bot_volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/volver (1).png"))); // NOI18N
        bot_volver.setContentAreaFilled(false);
        bot_volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_volver.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/volver (2).png"))); // NOI18N
        bot_volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_volverActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 330, 90, 80));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/excel (1).png"))); // NOI18N
        jButton1.setToolTipText("Dercarga Excel");
        jButton1.setContentAreaFilled(false);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/excel (2).png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jp_fondo.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 130, -1, 70));

        getContentPane().add(jp_fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 920, 480));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_muniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_muniActionPerformed
        
      
      
    }//GEN-LAST:event_txt_muniActionPerformed

    private void bot_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_buscarActionPerformed

         if(txt_edo.getText().equals("") && txt_muni.getText().equals("")){
     
         }else{
            try {
  
            res=operaciones.generar_ruta_estado(txt_edo.getText(),txt_muni.getText());
  
           while(res.next()){
              
            modelo_tabla.addRow(new Object[]{ res.getString("n_pedido"), res.getString("nombre"), res.getString("region"), res.getString("edo"), res.getString("municipio"),res.getString("parroquia"), res.getString("direccion_completa"),res.getString("nombre_del_cliente_principal"), res.getString("tipo_sector"), res.getString("sector"),res.getString("mac"), res.getString("despliegue_ip"),  res.getString("ipoam"),  res.getString("qos_operativo"),  res.getString("telepuerto"),res.getString("circuito"), res.getString("estatus_red"), res.getString("empresa_instaladora"),res.getString("codigo_de_cierre"), res.getString("f_apertura_a"), res.getString("nombre_contacto"), res.getString("telefono_contacto"), res.getString("descripcion_falla"),res.getString("causa_falla"),res.getString("o_operativas")});
              txt_edo.setEditable(false);
              txt_muni.setEditable(false);
            }
        } catch (SQLException ex) {
//           Logger.getLogger(scras_ruta.class.getName()).log(Level.SEVERE, null, ex);
//                System.out.println(ex);
        }
       }
        
    
    }//GEN-LAST:event_bot_buscarActionPerformed

    private void txt_edoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_edoMouseClicked
         try{
             txt_edo.setEditable(true);
              txt_muni.setEditable(true);
              txt_edo.setText("");
              txt_muni.setText("");
              
             
       for (int i=0;
       i<modelo_tabla.getRowCount();i--){
        for (int j=0;
                j<modelo_tabla.getRowCount();j--){
           modelo_tabla.removeRow(0);
       }
    
     }
     }catch (Exception e)  {
         
     }
         
       
    }//GEN-LAST:event_txt_edoMouseClicked

    private void bot_buscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bot_buscarMouseClicked
//       if(txt_edo.getText().equals("") && txt_muni.getText().equals("")){
//       bot_buscar.setEnabled(false);
//       }else{
//        bot_buscar.setEnabled(true);
//       }
    }//GEN-LAST:event_bot_buscarMouseClicked

    private void bot_registroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_registroActionPerformed
        scras_registro frame = new scras_registro();
        frame.setVisible(true);
        this.dispose ();
    }//GEN-LAST:event_bot_registroActionPerformed
public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
String cadena= (jTextfieldS.getText()).toUpperCase();
jTextfieldS.setText(cadena);
}
    private void txt_edoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_edoKeyReleased
        convertiraMayusculasEnJtextfield(txt_edo);
    }//GEN-LAST:event_txt_edoKeyReleased

    private void txt_muniKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_muniKeyReleased
        convertiraMayusculasEnJtextfield(txt_muni );
    }//GEN-LAST:event_txt_muniKeyReleased

    private void bot_volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_volverActionPerformed
       this.dispose();
    }//GEN-LAST:event_bot_volverActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
         scras_descargar_excel excel = new scras_descargar_excel();
       boolean bandera=true;
        JFileChooser ventana = new JFileChooser();
        String ruta = "";
        String ruta_2="";
      
      
      
         
String [] operaciones = {"Archivo Excel 97-2003","Archivo Excel 2007..."};
String valor =(String) JOptionPane.showInputDialog(this,"Guardar como ","Acrhivos Excel",JOptionPane.INFORMATION_MESSAGE,null,operaciones,operaciones[0]);
          
          
        try {
            if (ventana.showSaveDialog(null) == ventana.APPROVE_OPTION) {
//                ruta = ventana.getSelectedFile().getAbsolutePath() + ".xls";
//                 ruta_2=ventana.getSelectedFile().getAbsolutePath() + ".xlsx";
                ImageIcon BIENVENIDO ;
              
//    BIENVENIDO = new ImageIcon("src/imagenes/home.png");
//     
//        lbl_icono.setIcon(BIENVENIDO);
          
                if(valor.equals("Archivo Excel 97-2003")){
               ruta = ventana.getSelectedFile().getAbsolutePath() + ".xls";
              excel.exel_xls_averias_abiertas(ruta,"RUTAS DE MANTENIMIENTO","REGION ORIENTE ( Bolivar - Delta Amacuro - Monagas )");
               }else if(valor.equals("Archivo Excel 2007...")){
               ruta_2=ventana.getSelectedFile().getAbsolutePath() + ".xlsx";
                excel.exel_xlsx_averias_abiertas(ruta_2,"RUTAS DE MANTENIMIENTO","REGION ORIENTE ( Bolivar - Delta Amacuro - Monagas )");
                }else{
                  JOptionPane.showMessageDialog(null,"Disculpe no Escogio ninhuna opcion");
               }
            
            
                
              
                JOptionPane.showMessageDialog(null, "EXCEL CREADO CON EXITO");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
            
      
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(scras_ruta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(scras_ruta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(scras_ruta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(scras_ruta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new scras_ruta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bot_buscar;
    private javax.swing.JButton bot_registro;
    private javax.swing.JButton bot_volver;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel jp_fondo;
    private javax.swing.JPanel jp_tabla;
    private javax.swing.JTable jt_rutas2;
    private javax.swing.JLabel lbl_edo;
    private javax.swing.JLabel lbl_municipio;
    private javax.swing.JLabel lbl_regis;
    private javax.swing.JLabel lbl_titulo;
    private javax.swing.JLabel lbl_volver;
    private javax.swing.JLabel lbl_volver3;
    private javax.swing.JTextField txt_edo;
    private javax.swing.JTextField txt_muni;
    // End of variables declaration//GEN-END:variables
}
