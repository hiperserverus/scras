/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dayana Martinez <dmaro006@gmail.com>
 */
import Control_bd.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class scras_registro extends javax.swing.JFrame {

    operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
 
     
     
    public scras_registro() {
        initComponents();
        this.setResizable(false);
        this.setLocationRelativeTo(null);
         setIconImage(new ImageIcon(getClass().getResource("/imagenes/scras_icono.png")).getImage()); 
         
    
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        busqueda = new javax.swing.ButtonGroup();
        jScrollPane2 = new javax.swing.JScrollPane();
        jp_fondo = new javax.swing.JPanel();
        lbl_salir = new javax.swing.JLabel();
        lbl_titulo = new javax.swing.JLabel();
        lbl_busqueda = new javax.swing.JLabel();
        lbl_numero_busqueda = new javax.swing.JLabel();
        jrb_pedido = new javax.swing.JRadioButton();
        jrb_mac = new javax.swing.JRadioButton();
        txt_busqueda = new javax.swing.JTextField();
        bot_buscar = new javax.swing.JButton();
        jp_d_geo = new javax.swing.JPanel();
        txt_sector = new javax.swing.JTextField();
        lbl_sector = new javax.swing.JLabel();
        lbl_t_sector = new javax.swing.JLabel();
        txt_t_sector = new javax.swing.JTextField();
        txt_parroquia = new javax.swing.JTextField();
        lbl_parroquia = new javax.swing.JLabel();
        lbl_region = new javax.swing.JLabel();
        txt_region = new javax.swing.JTextField();
        lbl_direccion = new javax.swing.JLabel();
        txt_estado = new javax.swing.JTextField();
        lbl_estado = new javax.swing.JLabel();
        lbl_municipio = new javax.swing.JLabel();
        txt_municipio = new javax.swing.JTextField();
        txt_nombre_cliente = new javax.swing.JTextField();
        lbl_nombre_cliente = new javax.swing.JLabel();
        txt_num_pedido = new javax.swing.JTextField();
        lbl_num_pedido = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txA_direccion = new javax.swing.JTextArea();
        jp_d_tec = new javax.swing.JPanel();
        txt_cod_cierre = new javax.swing.JTextField();
        lbl_cod_cierre = new javax.swing.JLabel();
        lbl_e_instal = new javax.swing.JLabel();
        txt_e_instal = new javax.swing.JTextField();
        lbl_cir = new javax.swing.JLabel();
        txt_cir = new javax.swing.JTextField();
        lbl_S_red = new javax.swing.JLabel();
        txt_s_red = new javax.swing.JTextField();
        txt_qos = new javax.swing.JTextField();
        txt_tele = new javax.swing.JTextField();
        lbl_tele = new javax.swing.JLabel();
        lbl_qos = new javax.swing.JLabel();
        txt_mas = new javax.swing.JTextField();
        lbl_mas = new javax.swing.JLabel();
        lbl_mac = new javax.swing.JLabel();
        txt_mac = new javax.swing.JTextField();
        lbl_ip = new javax.swing.JLabel();
        txt_ip = new javax.swing.JTextField();
        lbl_nombre = new javax.swing.JLabel();
        txt_nombre = new javax.swing.JTextField();
        bot_C_averia = new javax.swing.JButton();
        bot_A_averia = new javax.swing.JButton();
        bot_contrl = new javax.swing.JButton();
        lbl_salir1 = new javax.swing.JLabel();
        lbl_salir2 = new javax.swing.JLabel();
        lbl_salir3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        bot_nuevo = new javax.swing.JButton();
        lbl_salir4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SCRAS");
        setMinimumSize(new java.awt.Dimension(900, 500));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jp_fondo.setMinimumSize(new java.awt.Dimension(900, 800));
        jp_fondo.setPreferredSize(new java.awt.Dimension(900, 800));
        jp_fondo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_salir.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir.setText("Registrar Averia");
        jp_fondo.add(lbl_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 200, 100, 30));

        lbl_titulo.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lbl_titulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_titulo.setText("Consulta de Centro");
        jp_fondo.add(lbl_titulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 180, 30));

        lbl_busqueda.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_busqueda.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_busqueda.setText("Tipo de búsqueda:");
        jp_fondo.add(lbl_busqueda, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 130, 30));

        lbl_numero_busqueda.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_numero_busqueda.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_numero_busqueda.setText("N°:");
        jp_fondo.add(lbl_numero_busqueda, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, 40, 30));

        busqueda.add(jrb_pedido);
        jrb_pedido.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jrb_pedido.setText("PEDIDO");
        jrb_pedido.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrb_pedidoMouseClicked(evt);
            }
        });
        jp_fondo.add(jrb_pedido, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 40, 80, 30));

        busqueda.add(jrb_mac);
        jrb_mac.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jrb_mac.setText("MAC");
        jrb_mac.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrb_macMouseClicked(evt);
            }
        });
        jp_fondo.add(jrb_mac, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 40, 60, 30));

        txt_busqueda.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txt_busqueda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_busquedaMouseClicked(evt);
            }
        });
        txt_busqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_busquedaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_busquedaKeyReleased(evt);
            }
        });
        jp_fondo.add(txt_busqueda, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 160, 30));

        bot_buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar (1).png"))); // NOI18N
        bot_buscar.setToolTipText("Buscar");
        bot_buscar.setContentAreaFilled(false);
        bot_buscar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_buscar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar (2).png"))); // NOI18N
        bot_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_buscarActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_buscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 70, 50, 50));

        jp_d_geo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos Geograficos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 18))); // NOI18N
        jp_d_geo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_sector.setEditable(false);
        txt_sector.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_geo.add(txt_sector, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 150, 160, 30));

        lbl_sector.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_sector.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_sector.setText("Sector:");
        jp_d_geo.add(lbl_sector, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 150, 90, 30));

        lbl_t_sector.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_t_sector.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_t_sector.setText("Tipo de Sector:");
        jp_d_geo.add(lbl_t_sector, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 110, 100, 30));

        txt_t_sector.setEditable(false);
        txt_t_sector.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_geo.add(txt_t_sector, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 110, 160, 30));

        txt_parroquia.setEditable(false);
        txt_parroquia.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_geo.add(txt_parroquia, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 110, 160, 30));

        lbl_parroquia.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_parroquia.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_parroquia.setText("Parroquia:");
        jp_d_geo.add(lbl_parroquia, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 90, 30));

        lbl_region.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_region.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_region.setText("Región");
        jp_d_geo.add(lbl_region, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 90, 30));

        txt_region.setEditable(false);
        txt_region.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_geo.add(txt_region, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 160, 30));

        lbl_direccion.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_direccion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_direccion.setText("Dirección");
        jp_d_geo.add(lbl_direccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, 80, 30));

        txt_estado.setEditable(false);
        txt_estado.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_geo.add(txt_estado, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 150, 220, 30));

        lbl_estado.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_estado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_estado.setText("Estado:");
        jp_d_geo.add(lbl_estado, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 150, 60, 30));

        lbl_municipio.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_municipio.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_municipio.setText("Municipio:");
        jp_d_geo.add(lbl_municipio, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 80, 30));

        txt_municipio.setEditable(false);
        txt_municipio.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_geo.add(txt_municipio, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 190, 220, 30));

        txt_nombre_cliente.setEditable(false);
        txt_nombre_cliente.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_geo.add(txt_nombre_cliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 70, 430, 30));

        lbl_nombre_cliente.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_nombre_cliente.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_nombre_cliente.setText("Nombre del Cliente:");
        jp_d_geo.add(lbl_nombre_cliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 30, 140, 30));

        txt_num_pedido.setEditable(false);
        txt_num_pedido.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_geo.add(txt_num_pedido, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 30, 160, 30));

        lbl_num_pedido.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_num_pedido.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_num_pedido.setText("Pedido N°:");
        jp_d_geo.add(lbl_num_pedido, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 80, 30));

        txA_direccion.setEditable(false);
        txA_direccion.setColumns(20);
        txA_direccion.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txA_direccion.setLineWrap(true);
        txA_direccion.setRows(5);
        txA_direccion.setWrapStyleWord(true);
        jScrollPane1.setViewportView(txA_direccion);

        jp_d_geo.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 230, 600, 110));

        jp_fondo.add(jp_d_geo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 730, 360));

        jp_d_tec.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos Técnicos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 18))); // NOI18N
        jp_d_tec.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_cod_cierre.setEditable(false);
        txt_cod_cierre.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_tec.add(txt_cod_cierre, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 200, 190, 30));

        lbl_cod_cierre.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_cod_cierre.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_cod_cierre.setText("Código de Cierre");
        jp_d_tec.add(lbl_cod_cierre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, 110, 30));

        lbl_e_instal.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_e_instal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_e_instal.setText("Empresa Instaladora:");
        jp_d_tec.add(lbl_e_instal, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 140, 30));

        txt_e_instal.setEditable(false);
        txt_e_instal.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_tec.add(txt_e_instal, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 240, 540, 30));

        lbl_cir.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_cir.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_cir.setText("Circuito:");
        jp_d_tec.add(lbl_cir, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 80, 30));

        txt_cir.setEditable(false);
        txt_cir.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_tec.add(txt_cir, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 160, 160, 30));

        lbl_S_red.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_S_red.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_S_red.setText("Estatus de la RED:");
        jp_d_tec.add(lbl_S_red, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 200, 140, 30));

        txt_s_red.setEditable(false);
        txt_s_red.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_tec.add(txt_s_red, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 200, 220, 30));

        txt_qos.setEditable(false);
        txt_qos.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_tec.add(txt_qos, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 120, 160, 30));

        txt_tele.setEditable(false);
        txt_tele.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_tec.add(txt_tele, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 160, 160, 30));

        lbl_tele.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_tele.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_tele.setText("Telepuerto:");
        jp_d_tec.add(lbl_tele, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 160, 90, 30));

        lbl_qos.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_qos.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_qos.setText("Qos:");
        jp_d_tec.add(lbl_qos, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 120, 40, 30));

        txt_mas.setEditable(false);
        txt_mas.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_tec.add(txt_mas, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, 160, 30));

        lbl_mas.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_mas.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_mas.setText("MASCARA:");
        jp_d_tec.add(lbl_mas, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 80, 30));

        lbl_mac.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_mac.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_mac.setText("MAC:");
        jp_d_tec.add(lbl_mac, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 80, 30));

        txt_mac.setEditable(false);
        txt_mac.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_tec.add(txt_mac, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 80, 160, 30));

        lbl_ip.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_ip.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_ip.setText("IP:");
        jp_d_tec.add(lbl_ip, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 80, 40, 30));

        txt_ip.setEditable(false);
        txt_ip.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_tec.add(txt_ip, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 80, 160, 30));

        lbl_nombre.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_nombre.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_nombre.setText("Nombre:");
        jp_d_tec.add(lbl_nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 90, 30));

        txt_nombre.setEditable(false);
        txt_nombre.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_d_tec.add(txt_nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(109, 40, 580, 30));

        jp_fondo.add(jp_d_tec, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 480, 730, 290));

        bot_C_averia.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bot_C_averia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cerrar (1).png"))); // NOI18N
        bot_C_averia.setToolTipText("Cerrar Averia");
        bot_C_averia.setContentAreaFilled(false);
        bot_C_averia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_C_averia.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cerrar (2).png"))); // NOI18N
        bot_C_averia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bot_C_averiaMouseClicked(evt);
            }
        });
        bot_C_averia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_C_averiaActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_C_averia, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 240, 80, 80));

        bot_A_averia.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bot_A_averia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/registrar (1).png"))); // NOI18N
        bot_A_averia.setToolTipText("Registrar Averia");
        bot_A_averia.setContentAreaFilled(false);
        bot_A_averia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_A_averia.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/registrar (2).png"))); // NOI18N
        bot_A_averia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_A_averiaActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_A_averia, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 130, 80, 80));

        bot_contrl.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bot_contrl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/control (1).png"))); // NOI18N
        bot_contrl.setToolTipText("Ir a Control de Averias");
        bot_contrl.setContentAreaFilled(false);
        bot_contrl.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_contrl.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/control (2).png"))); // NOI18N
        bot_contrl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_contrlActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_contrl, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 360, 90, 80));

        lbl_salir1.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir1.setText("Cerrar Averia");
        jp_fondo.add(lbl_salir1, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 310, 100, 30));

        lbl_salir2.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir2.setText("Control de Averias");
        jp_fondo.add(lbl_salir2, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 430, 140, 30));

        lbl_salir3.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir3.setText("Nuevo Archivo");
        jp_fondo.add(lbl_salir3, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 550, 100, 30));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menu (1).png"))); // NOI18N
        jButton1.setToolTipText("Menu principal");
        jButton1.setContentAreaFilled(false);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menu (2).png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jp_fondo.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 600, 80, 80));

        bot_nuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo (1).png"))); // NOI18N
        bot_nuevo.setToolTipText("Cargar nuevo archivo de despliegue");
        bot_nuevo.setContentAreaFilled(false);
        bot_nuevo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_nuevo.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo (2).png"))); // NOI18N
        bot_nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_nuevoActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_nuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 480, 80, 80));

        lbl_salir4.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir4.setText("Volver ");
        jp_fondo.add(lbl_salir4, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 670, 100, 30));

        jScrollPane2.setViewportView(jp_fondo);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 890, 610));

        pack();
    }// </editor-fold>//GEN-END:initComponents
  public static String numero_referencia;
   public static String numero_referencia2;
    private void bot_A_averiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_A_averiaActionPerformed
    
   ResultSet  res;
   
    res=operaciones.id_tick(txt_busqueda.getText());
     
    
   if( txt_num_pedido.getText().equals("") &&
                    txt_cir.getText().equals("") &&
                    txt_tele.getText().equals("") &&
                    txt_t_sector.getText().equals("") &&
                    txt_sector.getText().equals("") &&
                    txt_region.getText().equals("") &&
                    txt_estado.getText().equals("") &&
                    txt_municipio.getText().equals("") &&
                    txt_parroquia.getText().equals("") &&
                    txt_nombre_cliente.getText().equals("") &&
                    txt_nombre.getText().equals("") &&
                    txA_direccion.getText().equals("") &&
                    txt_s_red.getText().equals("") &&
                    txt_e_instal.getText().equals("") &&
                    txt_qos.getText().equals("") &&
                    txt_mac.getText().equals("") &&
                    txt_ip.getText().equals("") &&
                    txt_mas.getText().equals("") &&
                    txt_cod_cierre.getText().equals("")){
     JOptionPane.showMessageDialog(null,"Error Capos Vacios");
   
   }else{
       
       if(jrb_mac.isSelected()){
       numero_referencia2=txt_num_pedido.getText();
       }else{
           
       numero_referencia2=txt_busqueda.getText();
       }

     
       
       
       try {
           if (operaciones.insertar_datos_tecnicos(txt_num_pedido.getText(), txt_nombre_cliente.getText(), txt_region.getText(), txt_estado.getText(), txt_municipio.getText(),txt_parroquia.getText(),  txA_direccion.getText(),  txt_t_sector.getText(),  txt_sector.getText(),txt_mac.getText(), txt_nombre.getText(), txt_ip.getText(),txt_mas.getText(), txt_qos.getText(), txt_tele.getText(),txt_cir.getText(),  txt_cod_cierre.getText(), txt_s_red.getText(), txt_e_instal.getText())){
              
               txt_num_pedido.setText("");
                    txt_cir.setText("");
                    txt_tele.setText("");
                    txt_t_sector.setText("");
                    txt_sector.setText("");
                    txt_region.setText("");
                    txt_estado.setText("");
                    txt_municipio.setText("");
                    txt_parroquia.setText("");
                    txt_nombre_cliente.setText("");
                    txt_nombre.setText("");
                    txA_direccion.setText("");
                    txt_s_red.setText("");
                    txt_e_instal.setText("");
                    txt_qos.setText("");
                    txt_mac.setText("");
                    txt_ip.setText("");
                    txt_mas.setText("");
                    txt_cod_cierre.setText("");
              
              
           }
           
           if(res.next()){
           JOptionPane.showMessageDialog(null,"Averia Registrada");
               
           }else{
            scras_registra_averia reg_averia = new scras_registra_averia();
              reg_averia.setVisible(true);
              this.dispose();
           }
           
          
       } catch (SQLException ex) {
//           Logger.getLogger(scras_registro.class.getName()).log(Level.SEVERE, null, ex);
       }
     }

         
    }//GEN-LAST:event_bot_A_averiaActionPerformed

    private void bot_C_averiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_C_averiaActionPerformed
 
//   res=operaciones.id_pedido(txt_busqueda.getText());
 
 ResultSet res3;
 
 if(jrb_mac.isSelected()==true){
  ResultSet res;
  res=operaciones.id_mac(txt_busqueda.getText());
      
            
            if( txt_num_pedido.getText().equals("") &&
                    txt_cir.getText().equals("") &&
                    txt_tele.getText().equals("") &&
                    txt_t_sector.getText().equals("") &&
                    txt_sector.getText().equals("") &&
                    txt_region.getText().equals("") &&
                    txt_estado.getText().equals("") &&
                    txt_municipio.getText().equals("") &&
                    txt_parroquia.getText().equals("") &&
                    txt_nombre_cliente.getText().equals("") &&
                    txt_nombre.getText().equals("") &&
                    txA_direccion.getText().equals("") &&
                    txt_s_red.getText().equals("") &&
                    txt_e_instal.getText().equals("") &&
                    txt_qos.getText().equals("") &&
                    txt_mac.getText().equals("") &&
                    txt_ip.getText().equals("") &&
                    txt_mas.getText().equals("") &&
                    txt_cod_cierre.getText().equals("")){
                
                
                JOptionPane.showMessageDialog(null,"Error Campos Vacios");
            }  else{
                
                res3=operaciones.tbl_cierre_averias(txt_busqueda.getText());
            
             try{
                if(res3.next()){
                  JOptionPane.showMessageDialog(null,"Averia ya cerrada");
                }else{
                   }// 
             }catch(SQLException dayana){
             
             }
      try {
          if(res.next()){
              
               if(jrb_mac.isSelected()){
       numero_referencia=txt_num_pedido.getText();
       }else{
           
       numero_referencia=txt_busqueda.getText();
       }
            
               
               
              scras_cierre_averias frame = new scras_cierre_averias ();
              frame.setVisible(true);
              this.dispose();
              
              
          }else{
              
              JOptionPane.showMessageDialog(null,"No existe Registro Relacionado ");
              
          }
      } catch (SQLException ex) {
         
      }
              
                
                   
             
                  
            }
               
  
  
 }else if(jrb_pedido.isSelected()){
    ResultSet res1; 
    res1=operaciones.id_pedido(txt_busqueda.getText()); 
    
            
            if( txt_num_pedido.getText().equals("") &&
                    txt_cir.getText().equals("") &&
                    txt_tele.getText().equals("") &&
                    txt_t_sector.getText().equals("") &&
                    txt_sector.getText().equals("") &&
                    txt_region.getText().equals("") &&
                    txt_estado.getText().equals("") &&
                    txt_municipio.getText().equals("") &&
                    txt_parroquia.getText().equals("") &&
                    txt_nombre_cliente.getText().equals("") &&
                    txt_nombre.getText().equals("") &&
                    txA_direccion.getText().equals("") &&
                    txt_s_red.getText().equals("") &&
                    txt_e_instal.getText().equals("") &&
                    txt_qos.getText().equals("") &&
                    txt_mac.getText().equals("") &&
                    txt_ip.getText().equals("") &&
                    txt_mas.getText().equals("") &&
                    txt_cod_cierre.getText().equals("")){
                
                
                JOptionPane.showMessageDialog(null,"Error Campos Vacios");
            }  else{
                 res3=operaciones.tbl_cierre_averias(txt_busqueda.getText());
            
              try{
                if(res3.next()){
                  JOptionPane.showMessageDialog(null,"Averia ya cerrada");
                }else{
               
                
      try {
          if(res1.next()){
              
               if(jrb_mac.isSelected()){
       numero_referencia=txt_num_pedido.getText();
       }else{
           
       numero_referencia=txt_busqueda.getText();
       }
             
              scras_cierre_averias frame = new scras_cierre_averias ();
              frame.setVisible(true);
              this.dispose();
              
              
          }else{
              
              JOptionPane.showMessageDialog(null,"No existe Registro Relacionado ");
              
          }
      } catch (SQLException ex) {
         
      }
              }// 
            }catch(SQLException dayana){
             
             } 
                
            }        
                 
                  
            }
               

 
 

  
     
       

    }//GEN-LAST:event_bot_C_averiaActionPerformed

    private void bot_contrlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_contrlActionPerformed
        scras_averias_abiertas frame = new scras_averias_abiertas();
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_bot_contrlActionPerformed

    private void jrb_pedidoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrb_pedidoMouseClicked

    }//GEN-LAST:event_jrb_pedidoMouseClicked

    private void jrb_macMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrb_macMouseClicked

    }//GEN-LAST:event_jrb_macMouseClicked

    private void bot_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_buscarActionPerformed
        String opcion;
        ResultSet res;
        try {
            if (txt_busqueda.getText().equals("")) {

                JOptionPane.showMessageDialog(null, "Campo Vacio");
            } else {
                if (jrb_pedido.isSelected() == true) {
                    opcion = "pedido";
//                    JOptionPane.showMessageDialog(null, "Opcion" + opcion);

                    res = operaciones.consultar_despliegue(txt_busqueda.getText(), opcion);
                    try {
                        if (res.next() == true) {
                            txt_num_pedido.setText(res.getString("pedido"));
                            txt_cir.setText(res.getString("circuito"));
                            txt_tele.setText(res.getString("telepuerto"));
                            txt_t_sector.setText(res.getString("tipo_sector"));
                            txt_sector.setText(res.getString("sector"));
                            txt_region.setText(res.getString("region"));
                            txt_estado.setText(res.getString("edo"));
                            txt_municipio.setText(res.getString("municipio"));
                            txt_parroquia.setText(res.getString("parroquia"));
                            txt_nombre_cliente.setText(res.getString("nombre_del_cliente_principal"));
                            txt_nombre.setText(res.getString("nombre"));
                            txA_direccion.setText(res.getString("direccion_completa"));
                            txt_s_red.setText(res.getString("estatus_red"));
                            txt_e_instal.setText(res.getString("empresa_instaladora"));
                            txt_qos.setText(res.getString("qos_operativo"));
                            txt_mac.setText(res.getString("mac"));
                            txt_ip.setText(res.getString("despliegue_ip"));
                            txt_mas.setText(res.getString("ipoam"));
                            txt_cod_cierre.setText(res.getString("codigo_de_cierre"));
                        }
                    } catch (Exception e) {

//     Logger.getLogger(scras_registro.class.getName()).log(Level.SEVERE,null,ex);
                    }
                } else {
                    if (jrb_mac.isSelected() == true) {
                        opcion = "mac";
//                        JOptionPane.showMessageDialog(null, "Opcion" + opcion);

                        res = operaciones.consultar_despliegue(txt_busqueda.getText(), opcion);
                        try {
                            if (res.next() == true) {
                                txt_num_pedido.setText(res.getString("pedido"));
                                txt_cir.setText(res.getString("circuito"));
                                txt_tele.setText(res.getString("telepuerto"));
                                txt_t_sector.setText(res.getString("tipo_sector"));
                                txt_sector.setText(res.getString("sector"));
                                txt_region.setText(res.getString("region"));
                                txt_estado.setText(res.getString("edo"));
                                txt_municipio.setText(res.getString("municipio"));
                                txt_parroquia.setText(res.getString("parroquia"));
                                txt_nombre_cliente.setText(res.getString("nombre_del_cliente_principal"));
                                txt_nombre.setText(res.getString("nombre"));
                                txA_direccion.setText(res.getString("direccion_completa"));
                                txt_s_red.setText(res.getString("estatus_red"));
                                txt_e_instal.setText(res.getString("empresa_instaladora"));
                                txt_qos.setText(res.getString("qos_operativo"));
                                txt_mac.setText(res.getString("mac"));
                                txt_ip.setText(res.getString("despliegue_ip"));
                                txt_mas.setText(res.getString("ipoam"));
                                txt_cod_cierre.setText(res.getString("codigo_de_cierre"));
                            }
                        } catch (Exception e) {

//     Logger.getLogger(scras_registro.class.getName()).log(Level.SEVERE,null,ex);
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
    }//GEN-LAST:event_bot_buscarActionPerformed
public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
String cadena= (jTextfieldS.getText()).toUpperCase();
jTextfieldS.setText(cadena);
}
    private void txt_busquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_busquedaKeyReleased
        convertiraMayusculasEnJtextfield(txt_busqueda);

    }//GEN-LAST:event_txt_busquedaKeyReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void bot_nuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_nuevoActionPerformed
       scras_cargar_archivo frame = new scras_cargar_archivo();
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_bot_nuevoActionPerformed

    private void txt_busquedaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_busquedaKeyPressed
     if(evt.getKeyCode()==10){
     bot_buscar.doClick();
     }
    }//GEN-LAST:event_txt_busquedaKeyPressed

    private void txt_busquedaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_busquedaMouseClicked
      txt_busqueda.setText("");
    }//GEN-LAST:event_txt_busquedaMouseClicked

    private void bot_C_averiaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bot_C_averiaMouseClicked
       txt_num_pedido.setText("");
                    txt_cir.setText("");
                    txt_tele.setText("");
                    txt_t_sector.setText("");
                    txt_sector.setText("");
                    txt_region.setText("");
                    txt_estado.setText("");
                    txt_municipio.setText("");
                    txt_parroquia.setText("");
                    txt_nombre_cliente.setText("");
                    txt_nombre.setText("");
                    txA_direccion.setText("");
                    txt_s_red.setText("");
                    txt_e_instal.setText("");
                    txt_qos.setText("");
                    txt_mac.setText("");
                    txt_ip.setText("");
                    txt_mas.setText("");
                    txt_cod_cierre.setText("");
    }//GEN-LAST:event_bot_C_averiaMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(scras_registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(scras_registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(scras_registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(scras_registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new scras_registro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bot_A_averia;
    private javax.swing.JButton bot_C_averia;
    private javax.swing.JButton bot_buscar;
    private javax.swing.JButton bot_contrl;
    private javax.swing.JButton bot_nuevo;
    public static javax.swing.ButtonGroup busqueda;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel jp_d_geo;
    private javax.swing.JPanel jp_d_tec;
    private javax.swing.JPanel jp_fondo;
    public static javax.swing.JRadioButton jrb_mac;
    public static javax.swing.JRadioButton jrb_pedido;
    private javax.swing.JLabel lbl_S_red;
    private javax.swing.JLabel lbl_busqueda;
    private javax.swing.JLabel lbl_cir;
    private javax.swing.JLabel lbl_cod_cierre;
    private javax.swing.JLabel lbl_direccion;
    private javax.swing.JLabel lbl_e_instal;
    private javax.swing.JLabel lbl_estado;
    private javax.swing.JLabel lbl_ip;
    private javax.swing.JLabel lbl_mac;
    private javax.swing.JLabel lbl_mas;
    private javax.swing.JLabel lbl_municipio;
    private javax.swing.JLabel lbl_nombre;
    private javax.swing.JLabel lbl_nombre_cliente;
    private javax.swing.JLabel lbl_num_pedido;
    private javax.swing.JLabel lbl_numero_busqueda;
    private javax.swing.JLabel lbl_parroquia;
    private javax.swing.JLabel lbl_qos;
    private javax.swing.JLabel lbl_region;
    private javax.swing.JLabel lbl_salir;
    private javax.swing.JLabel lbl_salir1;
    private javax.swing.JLabel lbl_salir2;
    private javax.swing.JLabel lbl_salir3;
    private javax.swing.JLabel lbl_salir4;
    private javax.swing.JLabel lbl_sector;
    private javax.swing.JLabel lbl_t_sector;
    private javax.swing.JLabel lbl_tele;
    private javax.swing.JLabel lbl_titulo;
    private javax.swing.JTextArea txA_direccion;
    public static javax.swing.JTextField txt_busqueda;
    public static javax.swing.JTextField txt_cir;
    public static javax.swing.JTextField txt_cod_cierre;
    public static javax.swing.JTextField txt_e_instal;
    public static javax.swing.JTextField txt_estado;
    public static javax.swing.JTextField txt_ip;
    public static javax.swing.JTextField txt_mac;
    public static javax.swing.JTextField txt_mas;
    public static javax.swing.JTextField txt_municipio;
    public static javax.swing.JTextField txt_nombre;
    public static javax.swing.JTextField txt_nombre_cliente;
    public static javax.swing.JTextField txt_num_pedido;
    public static javax.swing.JTextField txt_parroquia;
    public static javax.swing.JTextField txt_qos;
    public static javax.swing.JTextField txt_region;
    public static javax.swing.JTextField txt_s_red;
    public static javax.swing.JTextField txt_sector;
    public static javax.swing.JTextField txt_t_sector;
    public static javax.swing.JTextField txt_tele;
    // End of variables declaration//GEN-END:variables

   



}
