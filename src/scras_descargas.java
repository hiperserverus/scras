

import javax.swing.ImageIcon;
import Control_bd.operaciones_sql;
import java.awt.BorderLayout;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;


/**
 *
 * @author Dayana Martinez <dmaro006@gmail.com>
 */
public class scras_descargas extends javax.swing.JFrame {

       public DefaultTableModel modelo1 = new DefaultTableModel() {
            // codigo para las que las celdas del jtbale no sean editables
            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return false; // el resto de las celdas no son editables

            }

    };
              public DefaultTableModel modelo2 = new DefaultTableModel() {
            // codigo para las que las celdas del jtbale no sean editables
            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return false; // el resto de las celdas no son editables

            }

    };
              
operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
ResultSet res;//creacion de la variable resultset
              
    public scras_descargas() {
        initComponents();
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/scras_icono.png")).getImage()); 
        
        
    }

void tamaño_columnas(){

}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_salir = new javax.swing.JLabel();
        jp_fondo = new javax.swing.JPanel();
        lbl_titulo = new javax.swing.JLabel();
        jcb_opcion = new javax.swing.JComboBox();
        bot_descarga = new javax.swing.JButton();
        jp_contenedor = new javax.swing.JPanel();
        lbl_salir1 = new javax.swing.JLabel();
        bot_salir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SCRAS");
        setMinimumSize(new java.awt.Dimension(890, 600));
        setPreferredSize(new java.awt.Dimension(890, 600));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_salir.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir.setText("Descargar EXCEL");
        lbl_salir.setToolTipText("Menu Principal");
        lbl_salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_salir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_salirMouseClicked(evt);
            }
        });
        getContentPane().add(lbl_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 520, 120, 20));

        jp_fondo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Descargar EXCEL", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 14), new java.awt.Color(0, 0, 204))); // NOI18N
        jp_fondo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_titulo.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        lbl_titulo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_titulo.setText("Seleccionar Descarga:");
        jp_fondo.add(lbl_titulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 140, 30));

        jcb_opcion.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jcb_opcion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Registro de Averías Abiertas", "Registro de Averías Cerradas" }));
        jcb_opcion.setToolTipText("Opciones de descarga");
        jcb_opcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcb_opcionActionPerformed(evt);
            }
        });
        jp_fondo.add(jcb_opcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 30, 300, 30));

        getContentPane().add(jp_fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 840, 80));

        bot_descarga.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        bot_descarga.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/excel (1).png"))); // NOI18N
        bot_descarga.setToolTipText("Descargar EXCEL");
        bot_descarga.setContentAreaFilled(false);
        bot_descarga.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_descarga.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/excel (2).png"))); // NOI18N
        bot_descarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_descargaActionPerformed(evt);
            }
        });
        getContentPane().add(bot_descarga, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 460, 80, 60));

        jp_contenedor.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jp_contenedor.setLayout(new java.awt.CardLayout());
        getContentPane().add(jp_contenedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 840, 340));

        lbl_salir1.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir1.setText("Volver \n");
        lbl_salir1.setToolTipText("Menu Principal");
        lbl_salir1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_salir1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_salir1MouseClicked(evt);
            }
        });
        getContentPane().add(lbl_salir1, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 520, 80, 20));

        bot_salir.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        bot_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/volver (1).png"))); // NOI18N
        bot_salir.setToolTipText("Descargar EXCEL");
        bot_salir.setContentAreaFilled(false);
        bot_salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_salir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/volver (2).png"))); // NOI18N
        bot_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_salirActionPerformed(evt);
            }
        });
        getContentPane().add(bot_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 460, 80, 60));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lbl_salirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_salirMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbl_salirMouseClicked

jp_averias_abiertas jpaa = new jp_averias_abiertas();
jp_averias_cerradas jpac = new jp_averias_cerradas();
    private void bot_descargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_descargaActionPerformed
       
     scras_descargar_excel excel = new scras_descargar_excel();
       boolean bandera=true;
        JFileChooser ventana = new JFileChooser();
        String ruta = "";
        String ruta_2="";
         switch(jcb_opcion.getSelectedIndex()){
            case 0: jcb_opcion.getSelectedItem();
            
      
      
         
String [] operaciones = {"Archivo Excel 97-2003","Archivo Excel 2007..."};
String valor =(String) JOptionPane.showInputDialog(this,"Guardar como ","Acrhivos Excel",JOptionPane.INFORMATION_MESSAGE,null,operaciones,operaciones[0]);
          
          
        try {
            if (ventana.showSaveDialog(null) == ventana.APPROVE_OPTION) {
//                ruta = ventana.getSelectedFile().getAbsolutePath() + ".xls";
//                 ruta_2=ventana.getSelectedFile().getAbsolutePath() + ".xlsx";
                ImageIcon BIENVENIDO ;
              
//    BIENVENIDO = new ImageIcon("src/imagenes/home.png");
//     
//        lbl_icono.setIcon(BIENVENIDO);
          
                if(valor.equals("Archivo Excel 97-2003")){
               ruta = ventana.getSelectedFile().getAbsolutePath() + ".xls";
              excel.exel_xls_averias_abiertas(ruta,"Estatus Averias Reportadas SCRAS","REGION ORIENTE ( Bolivar - Delta Amacuro - Monagas ) ");
               }else if(valor.equals("Archivo Excel 2007...")){
               ruta_2=ventana.getSelectedFile().getAbsolutePath() + ".xlsx";
                excel.exel_xlsx_averias_abiertas(ruta_2,"Estatus Averias Reportadas SCRAS","REGION ORIENTE ( Bolivar - Delta Amacuro - Monagas ) ");
                }else{
                  JOptionPane.showMessageDialog(null,"Disculpe no Escogio ninhuna opcion");
               }
            
            
                
              
                JOptionPane.showMessageDialog(null, "EXCEL CREADO CON EXITO");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
            
      

         
            break;  
            case 1: jcb_opcion.getSelectedItem();   
          bandera=true;

      
         
String [] operaciones2 = {"Archivo Excel 97-2003","Archivo Excel 2007..."};
String valor2 =(String) JOptionPane.showInputDialog(this,"Guardar como ","Acrhivos Excel",JOptionPane.INFORMATION_MESSAGE,null,operaciones2,operaciones2[0]);
          
          
        try {
            if (ventana.showSaveDialog(null) == ventana.APPROVE_OPTION) {
//                ruta = ventana.getSelectedFile().getAbsolutePath() + ".xls";
//                 ruta_2=ventana.getSelectedFile().getAbsolutePath() + ".xlsx";
                ImageIcon BIENVENIDO ;
              
//    BIENVENIDO = new ImageIcon("src/imagenes/home.png");
//     
//        lbl_icono.setIcon(BIENVENIDO);
          
                if(valor2.equals("Archivo Excel 97-2003")){
               ruta = ventana.getSelectedFile().getAbsolutePath() + ".xls";
              excel.exel_xls_averias_cerradas(ruta);
               }else if(valor2.equals("Archivo Excel 2007...")){
               ruta_2=ventana.getSelectedFile().getAbsolutePath() + ".xlsx";
                excel.exel_xlsx_averias_cerradas(ruta_2);
                }else{
                  JOptionPane.showMessageDialog(null,"Disculpe no Escogio ninhuna opcion");
               }
            
            
                
              
                JOptionPane.showMessageDialog(null, "EXCEL CREADO CON EXITO");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
            
             
                 break;
                default:
                System.out.println("No Selecciono Ninguno");
                break;
                

}   
         
    }//GEN-LAST:event_bot_descargaActionPerformed

    private void jcb_opcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcb_opcionActionPerformed
       switch(jcb_opcion.getSelectedIndex()){
            case 0: jcb_opcion.getSelectedItem();
            
         jp_averias_abiertas aa = new jp_averias_abiertas();
         jp_contenedor.removeAll();
         jp_contenedor.add(aa,BorderLayout.CENTER);
         jp_contenedor.revalidate();
         jp_contenedor.repaint();  
         
            break;  
            case 1: jcb_opcion.getSelectedItem();   
           
          jp_averias_cerradas ac = new jp_averias_cerradas();
         jp_contenedor.removeAll();
         jp_contenedor.add(ac,BorderLayout.CENTER);
         jp_contenedor.revalidate();
         jp_contenedor.repaint();   
         
         
             
                 break;
                default:
                System.out.println("No Selecciono Ninguno");
                break;
                

}   
            
      
    }//GEN-LAST:event_jcb_opcionActionPerformed

    private void lbl_salir1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_salir1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_lbl_salir1MouseClicked

    private void bot_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_salirActionPerformed
        this.dispose();
    }//GEN-LAST:event_bot_salirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(scras_descargas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(scras_descargas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(scras_descargas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(scras_descargas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new scras_descargas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bot_descarga;
    private javax.swing.JButton bot_salir;
    private javax.swing.JComboBox jcb_opcion;
    private javax.swing.JPanel jp_contenedor;
    private javax.swing.JPanel jp_fondo;
    private javax.swing.JLabel lbl_salir;
    private javax.swing.JLabel lbl_salir1;
    private javax.swing.JLabel lbl_titulo;
    // End of variables declaration//GEN-END:variables
}
