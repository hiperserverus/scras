
import Control_bd.operaciones_sql;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import java.util.Calendar;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dayana Martinez <dmaro006@gmail.com>
 */
public class scras_registra_averia extends javax.swing.JFrame {

operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql

scras_registro registro = new scras_registro();
   
 String numero_ref;

    public scras_registra_averia() {
        initComponents();
        this.setResizable(false);
        this.setLocationRelativeTo(null);
         setIconImage(new ImageIcon(getClass().getResource("/imagenes/scras_icono.png")).getImage()); 
      
       txt_n_ticket.setText(registro.numero_referencia2);
     
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JSP_registrar_averia = new javax.swing.JScrollPane();
        jp_fondo = new javax.swing.JPanel();
        jp_r_ave = new javax.swing.JPanel();
        txt_n_ticket = new javax.swing.JTextField();
        lbl_fecha = new javax.swing.JLabel();
        lbl_n_ticket = new javax.swing.JLabel();
        jcalendar_1 = new com.toedter.calendar.JDateChooser();
        jp_contac = new javax.swing.JPanel();
        lbl_n_contac = new javax.swing.JLabel();
        lbl_t_contac = new javax.swing.JLabel();
        txt_t_contac = new javax.swing.JTextField();
        txt_n_contac = new javax.swing.JTextField();
        jp_obser = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txA_o_ope = new javax.swing.JTextArea();
        lbl_o_ope = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txA_c_falla = new javax.swing.JTextArea();
        lbl_c_falla = new javax.swing.JLabel();
        lbl_des_tecni = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txA_des_tecni = new javax.swing.JTextArea();
        bot_regis_averia = new javax.swing.JButton();
        bot_salir = new javax.swing.JButton();
        lbl_regis = new javax.swing.JLabel();
        lbl_salir = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SCRAS");
        setMinimumSize(new java.awt.Dimension(635, 500));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jp_fondo.setMinimumSize(new java.awt.Dimension(650, 721));
        jp_fondo.setPreferredSize(new java.awt.Dimension(650, 721));
        jp_fondo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jp_r_ave.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Registrar Avería", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 18))); // NOI18N
        jp_r_ave.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_n_ticket.setEditable(false);
        txt_n_ticket.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jp_r_ave.add(txt_n_ticket, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, 160, 30));

        lbl_fecha.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        lbl_fecha.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_fecha.setText("Fecha de Reporte:");
        jp_r_ave.add(lbl_fecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 50, 100, 30));

        lbl_n_ticket.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        lbl_n_ticket.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_n_ticket.setText("N° de Referencia:");
        jp_r_ave.add(lbl_n_ticket, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 100, 30));
        jp_r_ave.add(jcalendar_1, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 50, 120, 30));

        jp_fondo.add(jp_r_ave, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 590, 110));

        jp_contac.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Contacto", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 18))); // NOI18N
        jp_contac.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_n_contac.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        lbl_n_contac.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_n_contac.setText("Contacto del Reporte:");
        jp_contac.add(lbl_n_contac, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 120, 30));

        lbl_t_contac.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        lbl_t_contac.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_t_contac.setText("Teléfono del Contacto:");
        jp_contac.add(lbl_t_contac, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, -1, 30));

        txt_t_contac.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txt_t_contac.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_t_contacKeyTyped(evt);
            }
        });
        jp_contac.add(txt_t_contac, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, 170, 30));

        txt_n_contac.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txt_n_contac.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_n_contacKeyTyped(evt);
            }
        });
        jp_contac.add(txt_n_contac, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 30, 440, 30));

        jp_fondo.add(jp_contac, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 590, 120));

        jp_obser.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Observaciones", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 18))); // NOI18N
        jp_obser.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txA_o_ope.setColumns(20);
        txA_o_ope.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txA_o_ope.setRows(5);
        txA_o_ope.setWrapStyleWord(true);
        txA_o_ope.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txA_o_opeKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txA_o_opeKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(txA_o_ope);

        jp_obser.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 250, 400, -1));

        lbl_o_ope.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        lbl_o_ope.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_o_ope.setText("Observaciones Operativas:");
        jp_obser.add(lbl_o_ope, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, 150, 30));

        txA_c_falla.setColumns(20);
        txA_c_falla.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txA_c_falla.setRows(5);
        txA_c_falla.setWrapStyleWord(true);
        txA_c_falla.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txA_c_fallaKeyTyped(evt);
            }
        });
        jScrollPane3.setViewportView(txA_c_falla);

        jp_obser.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 140, 400, -1));

        lbl_c_falla.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        lbl_c_falla.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_c_falla.setText("Causa de la Falla:");
        jp_obser.add(lbl_c_falla, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 140, 100, 30));

        lbl_des_tecni.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        lbl_des_tecni.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_des_tecni.setText("Descripción de la Falla:");
        jp_obser.add(lbl_des_tecni, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 140, 30));

        txA_des_tecni.setColumns(20);
        txA_des_tecni.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txA_des_tecni.setRows(5);
        txA_des_tecni.setWrapStyleWord(true);
        txA_des_tecni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txA_des_tecniKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txA_des_tecniKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(txA_des_tecni);

        jp_obser.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 30, 400, -1));

        jp_fondo.add(jp_obser, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 590, 360));

        bot_regis_averia.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bot_regis_averia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar.png"))); // NOI18N
        bot_regis_averia.setToolTipText("Registrar Averia");
        bot_regis_averia.setContentAreaFilled(false);
        bot_regis_averia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_regis_averia.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar2.png"))); // NOI18N
        bot_regis_averia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_regis_averiaActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_regis_averia, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 600, 80, 70));

        bot_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/volver (1).png"))); // NOI18N
        bot_salir.setToolTipText("Menu Principal");
        bot_salir.setContentAreaFilled(false);
        bot_salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_salir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/volver (2).png"))); // NOI18N
        bot_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_salirActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 600, 80, 70));

        lbl_regis.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_regis.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_regis.setText("Registrar");
        jp_fondo.add(lbl_regis, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 660, 80, 30));

        lbl_salir.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir.setText("Volver");
        jp_fondo.add(lbl_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 660, 80, 30));

        JSP_registrar_averia.setViewportView(jp_fondo);

        getContentPane().add(JSP_registrar_averia, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 620, 510));

        pack();
    }// </editor-fold>//GEN-END:initComponents
 
    private void bot_regis_averiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_regis_averiaActionPerformed
    int dia = jcalendar_1.getCalendar().get(Calendar.DAY_OF_MONTH);
    int mes = jcalendar_1.getCalendar().get(Calendar.MONTH)+1;
    int año = jcalendar_1.getCalendar().get(Calendar.YEAR);
    String fecha= dia+ "/"+mes+"/"+año ;
    ResultSet res1;
    Integer id_tick=0;
    res1=operaciones.id_pedido(txt_n_ticket.getText()); //CAPTURAMOS LA CLAVE PRIMARIA DE LA TABLA DATOS TECNICOS
    try{
       while(res1.next()){
        id_tick=Integer.parseInt(res1.getString("n_dato"));
         
          }
    }catch(SQLException e){
    
    }//FIN CAPTURA DE CLAVE PRIMARIA DE LA TABLA DATOS TECNICOS
    
    try {
 System.out.println(id_tick);
            if (operaciones.insertar_averia(txt_n_ticket.getText(), fecha, txt_n_contac.getText(), txt_t_contac.getText(), txA_des_tecni.getText(), txA_c_falla.getText(), txA_o_ope.getText(),  id_tick)) {
                operaciones.insertar_averia_2(txt_n_ticket.getText(), fecha, txt_n_contac.getText(), txt_t_contac.getText(), txA_des_tecni.getText(), txA_c_falla.getText(), txA_o_ope.getText());
                JOptionPane.showMessageDialog(null, "Registro de averia  Exitoso  ");
               
               
            } else {
                
                JOptionPane.showMessageDialog(null, "Registro de averia no exitoso ");
            }
            

    } catch(Exception ex) {
//        Logger.getLogger(scras_registra_averia.class.getName()).log(Level.SEVERE, null, ex);
    }
   dispose();
   registro.dispose();
    }//GEN-LAST:event_bot_regis_averiaActionPerformed

    private void txA_o_opeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txA_o_opeKeyPressed
    
        /*codigo que se pone en el ultimo campo de texto para que cuando
        le des enter se active el boton por ejemplo de guardar*/
        
     if(evt.getKeyCode()==10){
     bot_regis_averia.doClick();

     }
    }//GEN-LAST:event_txA_o_opeKeyPressed

    private void txt_t_contacKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_t_contacKeyTyped
          //solo numerros

        char car = evt.getKeyChar();
        if (txt_t_contac.getText().length() >= 11) { //la longitud del campo de texto la pones deacuerdo a tu necesidad
            evt.consume();
        }
        if ((car < '0' || car > '9')
                
                   //aqui pondras cualquier simbolo letra o numero que quieras que te 
                // acepte el sistema de cuerdo al codigo asci
                
                && car != 46) {

            evt.consume();

        }
    }//GEN-LAST:event_txt_t_contacKeyTyped

    private void txt_n_contacKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_n_contacKeyTyped
       char car = evt.getKeyChar();
        if (txt_n_contac.getText().length() >= 200) {//la longitud del campo de texto la pones deacuerdo a tu necesidad
            evt.consume();
        }
        
        
        
       
        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') // solo letras
                
                 //aqui pondras cualquier simbolo letra o numero que quieras que te 
                // acepte el sistema de cuerdo al codigo asci
                
                && car != 'á' //Minúsculas

                && car != 'é'
                && car != 'í'
                && car != 'ó'
                && car != 'ú'
                && car != 'Á' //Mayúsculas

                && car != 'É'
                && car != 'Í'
                && car != 'Ó'
                && car != 'Ú'
                && (car != (char) KeyEvent.VK_SPACE)) //barra espaciadora
        {

            evt.consume();

        }
    }//GEN-LAST:event_txt_n_contacKeyTyped

    private void txA_des_tecniKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txA_des_tecniKeyPressed
     
    }//GEN-LAST:event_txA_des_tecniKeyPressed

    private void txA_c_fallaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txA_c_fallaKeyTyped
        char car = evt.getKeyChar();
        if (txA_c_falla.getText().length() >= 200) {//la longitud del campo de texto la pones deacuerdo a tu necesidad
            evt.consume();
        }
        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') // solo letras

                
                
                 //aqui pondras cualquier simbolo letra o numero que quieras que te 
                // acepte el sistema de cuerdo al codigo asci
                
                && car != 'á' //Minúsculas

                && car != 'é'
                && car != 'í'
                && car != 'ó'
                && car != 'ú'
                && car != 'Á' //Mayúsculas

                && car != 'É'
                && car != 'Í'
                && car != 'Ó'
                && car != 'Ú'
                && (car != (char) KeyEvent.VK_SPACE)) //barra espaciadora
        {

            evt.consume();

        }
    }//GEN-LAST:event_txA_c_fallaKeyTyped

    private void txA_o_opeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txA_o_opeKeyTyped
     char car = evt.getKeyChar();
        if (txA_o_ope.getText().length() >= 100) {//la longitud del campo de texto la pones deacuerdo a tu necesidad
            evt.consume();
        }
        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') // solo letras

                
                
                 
                 //aqui pondras cualquier simbolo letra o numero que quieras que te 
                // acepte el sistema de cuerdo al codigo asci
                && car != 'á' //Minúsculas

                && car != 'é'
                && car != 'í'
                && car != 'ó'
                && car != 'ú'
                && car != 'Á' //Mayúsculas

                && car != 'É'
                && car != 'Í'
                && car != 'Ó'
                && car != 'Ú'
                && (car != (char) KeyEvent.VK_SPACE)) //barra espaciadora
        {

            evt.consume();

        }
    }//GEN-LAST:event_txA_o_opeKeyTyped

    private void txA_des_tecniKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txA_des_tecniKeyTyped
       char car = evt.getKeyChar();
        if (txA_des_tecni.getText().length() >= 200) {//la longitud del campo de texto la pones deacuerdo a tu necesidad
            evt.consume();
        }
        
        
        
       
        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') // solo letras
                
                 //aqui pondras cualquier simbolo letra o numero que quieras que te 
                // acepte el sistema de cuerdo al codigo asci
                
                && car != 'á' //Minúsculas

                && car != 'é'
                && car != 'í'
                && car != 'ó'
                && car != 'ú'
                && car != 'Á' //Mayúsculas

                && car != 'É'
                && car != 'Í'
                && car != 'Ó'
                && car != 'Ú'
                && (car != (char) KeyEvent.VK_SPACE)) //barra espaciadora
        {

            evt.consume();

        }
    }//GEN-LAST:event_txA_des_tecniKeyTyped

    private void bot_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_salirActionPerformed
       this.dispose();
    }//GEN-LAST:event_bot_salirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(scras_registra_averia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(scras_registra_averia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(scras_registra_averia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(scras_registra_averia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new scras_registra_averia().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane JSP_registrar_averia;
    private javax.swing.JButton bot_regis_averia;
    private javax.swing.JButton bot_salir;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private com.toedter.calendar.JDateChooser jcalendar_1;
    private javax.swing.JPanel jp_contac;
    private javax.swing.JPanel jp_fondo;
    private javax.swing.JPanel jp_obser;
    private javax.swing.JPanel jp_r_ave;
    private javax.swing.JLabel lbl_c_falla;
    private javax.swing.JLabel lbl_des_tecni;
    private javax.swing.JLabel lbl_fecha;
    private javax.swing.JLabel lbl_n_contac;
    private javax.swing.JLabel lbl_n_ticket;
    private javax.swing.JLabel lbl_o_ope;
    private javax.swing.JLabel lbl_regis;
    private javax.swing.JLabel lbl_salir;
    private javax.swing.JLabel lbl_t_contac;
    private javax.swing.JTextArea txA_c_falla;
    private javax.swing.JTextArea txA_des_tecni;
    private javax.swing.JTextArea txA_o_ope;
    private javax.swing.JTextField txt_n_contac;
    public static javax.swing.JTextField txt_n_ticket;
    private javax.swing.JTextField txt_t_contac;
    // End of variables declaration//GEN-END:variables
}
