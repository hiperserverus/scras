 
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Control_bd.*;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Dayana Martinez <dmaro006@gmail.com>
 */


public class scras_averias_abiertas extends javax.swing.JFrame {

    public DefaultTableModel modelo_tabla = new DefaultTableModel() {
            // codigo para las que las celdas del jtbale no sean editables
            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return false; // el resto de las celdas no son editables

            }

    };
    
operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
ResultSet res;//creacion de la variable resultset

    public scras_averias_abiertas() {
        initComponents();
      
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        mostrar_averias(modelo_tabla);
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/scras_icono.png")).getImage());     

   
//aqui para asignarle un tamñao a cada columna individualmente
TableColumnModel columnModel = jt_matriz_ave.getColumnModel();
columnModel.getColumn(0).setPreferredWidth(150);
columnModel.getColumn(1).setPreferredWidth(300);
columnModel.getColumn(2).setPreferredWidth(150);
columnModel.getColumn(3).setPreferredWidth(150);
columnModel.getColumn(4).setPreferredWidth(150);
columnModel.getColumn(5).setPreferredWidth(150);
columnModel.getColumn(6).setPreferredWidth(150);
columnModel.getColumn(7).setPreferredWidth(250);
columnModel.getColumn(8).setPreferredWidth(150);
columnModel.getColumn(9).setPreferredWidth(150);
columnModel.getColumn(10).setPreferredWidth(150);
columnModel.getColumn(11).setPreferredWidth(150);
columnModel.getColumn(12).setPreferredWidth(150);
columnModel.getColumn(13).setPreferredWidth(150);
columnModel.getColumn(14).setPreferredWidth(150);
columnModel.getColumn(15).setPreferredWidth(150);
columnModel.getColumn(16).setPreferredWidth(150);
columnModel.getColumn(17).setPreferredWidth(150);
columnModel.getColumn(18).setPreferredWidth(150);
columnModel.getColumn(19).setPreferredWidth(150);
columnModel.getColumn(20).setPreferredWidth(150);
columnModel.getColumn(21).setPreferredWidth(150);
columnModel.getColumn(22).setPreferredWidth(250);
columnModel.getColumn(23).setPreferredWidth(250);
columnModel.getColumn(24).setPreferredWidth(250);

    }
    
  // PROCESO   MOSTRAR LA LISTA DE la bd
   public void mostrar_averias(DefaultTableModel model) { 
    
    //  Para el desplazamiento horizoltal del scrollpane en el jtable       
   jt_matriz_ave.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
   jt_matriz_ave.doLayout(); 
   //inabilita el movimiento de las columns   del jtable  
    jt_matriz_ave.getTableHeader().setReorderingAllowed(false);
    jt_matriz_ave.getTableHeader().setResizingAllowed(false);
        
    jt_matriz_ave.setModel(model);
       
    model.addColumn("Pedido");
    model.addColumn("Nombre");
    model.addColumn("Region");
    model.addColumn("Estado");
    model.addColumn("Municipio");
    model.addColumn("Parroquia");
    model.addColumn("Direccion");
    model.addColumn("Nombre del Cliente Principal");
    model.addColumn("Tipo de Sector");
    model.addColumn("Sector");
    model.addColumn("MAC");
    model.addColumn("IP");
    model.addColumn("MASCARA");
    model.addColumn("QoS");
    model.addColumn("Telepuerto");
    model.addColumn("Circuito");
    model.addColumn("Estatus de la Red");
    model.addColumn("Empresa Instaladora");
    model.addColumn("Codigo de Cierre");
    model.addColumn("Fecha de Reporte");
    model.addColumn("Contacto de Reporte");
    model.addColumn("Telefono de Contacto");
    model.addColumn("Descripcion de la Falla");
    model.addColumn("Causa de la Falla");
    model.addColumn("Observaciones Operativas");
    
     res = operaciones.mostrar_averias();
       
        try {   
            while (res.next()) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                 model.addRow(new Object[]{ res.getString("n_pedido"), res.getString("nombre"), res.getString("region"), res.getString("edo"), res.getString("municipio"),res.getString("parroquia"), res.getString("direccion_completa"),res.getString("nombre_del_cliente_principal"), res.getString("tipo_sector"), res.getString("sector"),res.getString("mac"), res.getString("despliegue_ip"),  res.getString("ipoam"),  res.getString("qos_operativo"),  res.getString("telepuerto"),res.getString("circuito"), res.getString("estatus_red"), res.getString("empresa_instaladora"),res.getString("codigo_de_cierre"), res.getString("f_apertura_a"), res.getString("nombre_contacto"), res.getString("telefono_contacto"), res.getString("descripcion_falla"),res.getString("causa_falla"),res.getString("o_operativas")});
            }
        } catch (Exception e) {

          

        }
   }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jp_fondo = new javax.swing.JPanel();
        lbl_salir = new javax.swing.JLabel();
        lbl_titulo = new javax.swing.JLabel();
        jp_averias = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jt_matriz_ave = new javax.swing.JTable();
        bot_c_averia = new javax.swing.JButton();
        bot_ruta = new javax.swing.JButton();
        bot_a_averia = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        lbl_salir1 = new javax.swing.JLabel();
        lbl_salir2 = new javax.swing.JLabel();
        lbl_salir3 = new javax.swing.JLabel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SCRAS");
        setMinimumSize(new java.awt.Dimension(920, 500));
        setPreferredSize(new java.awt.Dimension(920, 500));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jp_fondo.setMinimumSize(new java.awt.Dimension(975, 500));
        jp_fondo.setPreferredSize(new java.awt.Dimension(975, 722));
        jp_fondo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_salir.setBackground(new java.awt.Color(0, 0, 204));
        lbl_salir.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir.setText("Generar Ruta");
        lbl_salir.setToolTipText("Menu Principal");
        lbl_salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jp_fondo.add(lbl_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 410, 100, 30));

        lbl_titulo.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        lbl_titulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_titulo.setText("Averías Abiertas");
        jp_fondo.add(lbl_titulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 180, 30));

        jp_averias.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Averías Actuales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 14))); // NOI18N
        jp_averias.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jt_matriz_ave.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jt_matriz_ave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jt_matriz_aveMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jt_matriz_ave);

        jp_averias.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 840, 240));

        jp_fondo.add(jp_averias, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 860, 280));

        bot_c_averia.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bot_c_averia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cerrar (1).png"))); // NOI18N
        bot_c_averia.setToolTipText("Cerrar Averia Existente");
        bot_c_averia.setContentAreaFilled(false);
        bot_c_averia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_c_averia.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cerrar (2).png"))); // NOI18N
        bot_c_averia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_c_averiaActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_c_averia, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 340, 90, 80));

        bot_ruta.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bot_ruta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Ruta (1).png"))); // NOI18N
        bot_ruta.setToolTipText("Ruta de Control");
        bot_ruta.setContentAreaFilled(false);
        bot_ruta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_ruta.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Ruta (2).png"))); // NOI18N
        bot_ruta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_rutaActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_ruta, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 340, 90, 80));

        bot_a_averia.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bot_a_averia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/registrar (1).png"))); // NOI18N
        bot_a_averia.setToolTipText("Registrar Nueva Averia Satelital");
        bot_a_averia.setContentAreaFilled(false);
        bot_a_averia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_a_averia.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/registrar (2).png"))); // NOI18N
        bot_a_averia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_a_averiaActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_a_averia, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 340, 90, 80));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menu (1).png"))); // NOI18N
        jButton1.setToolTipText("Menu Principal");
        jButton1.setContentAreaFilled(false);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menu (2).png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jp_fondo.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 340, 90, 80));

        lbl_salir1.setBackground(new java.awt.Color(0, 0, 204));
        lbl_salir1.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir1.setText("Registrar Averia");
        lbl_salir1.setToolTipText("Menu Principal");
        lbl_salir1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jp_fondo.add(lbl_salir1, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 410, 120, 30));

        lbl_salir2.setBackground(new java.awt.Color(0, 0, 204));
        lbl_salir2.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir2.setText("Cerrar averia");
        lbl_salir2.setToolTipText("Menu Principal");
        lbl_salir2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jp_fondo.add(lbl_salir2, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 410, 90, 30));

        lbl_salir3.setBackground(new java.awt.Color(0, 0, 204));
        lbl_salir3.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir3.setText("Volver ");
        lbl_salir3.setToolTipText("Menu Principal");
        lbl_salir3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jp_fondo.add(lbl_salir3, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 410, 90, 30));

        getContentPane().add(jp_fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 900, 490));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bot_a_averiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_a_averiaActionPerformed
        scras_registro frame = new scras_registro();
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_bot_a_averiaActionPerformed

    private void bot_rutaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_rutaActionPerformed
        scras_ruta frame = new scras_ruta();
        frame.setVisible(true);
        this.dispose ();
    }//GEN-LAST:event_bot_rutaActionPerformed

    private void bot_c_averiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_c_averiaActionPerformed
        scras_registro frame = new scras_registro();
        frame.setVisible(true);
        this.dispose ();
    }//GEN-LAST:event_bot_c_averiaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed
 String id;
   
    private void jt_matriz_aveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jt_matriz_aveMouseClicked
      
 
   id = jt_matriz_ave.getValueAt(jt_matriz_ave.getSelectedRow(), 19).toString();
        System.out.println(id);
       
    }//GEN-LAST:event_jt_matriz_aveMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(scras_averias_abiertas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(scras_averias_abiertas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(scras_averias_abiertas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(scras_averias_abiertas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new scras_averias_abiertas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bot_a_averia;
    private javax.swing.JButton bot_c_averia;
    private javax.swing.JButton bot_ruta;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JPanel jp_averias;
    private javax.swing.JPanel jp_fondo;
    public static javax.swing.JTable jt_matriz_ave;
    private javax.swing.JLabel lbl_salir;
    private javax.swing.JLabel lbl_salir1;
    private javax.swing.JLabel lbl_salir2;
    private javax.swing.JLabel lbl_salir3;
    private javax.swing.JLabel lbl_titulo;
    // End of variables declaration//GEN-END:variables
}
