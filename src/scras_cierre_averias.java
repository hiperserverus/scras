
import Control_bd.conexion_bd;
import Control_bd.operaciones_sql;
import com.mysql.jdbc.PreparedStatement;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

        import java.util.Calendar;





/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dayana Martinez <dmaro006@gmail.com>
 */
public class scras_cierre_averias extends javax.swing.JFrame {

 operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
 ResultSet res;//creacion de la variable resultset
 scras_registra_averia registro = new scras_registra_averia();
 scras_registro registro2 = new scras_registro();
 scras_averias_abiertas sa = new scras_averias_abiertas();
 

 String id_ticket;
    public scras_cierre_averias() {
        initComponents();
        this.setResizable(false);
        this.setLocationRelativeTo(null);
         setIconImage(new ImageIcon(getClass().getResource("/imagenes/scras_icono.png")).getImage()); 
        
     txt_num_refencia.setText(registro2.numero_referencia);
       
          
    }
    
  conexion_bd conexion =  new conexion_bd();
 
 @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SDP_Cierre_Averias = new javax.swing.JScrollPane();
        jp_fondo = new javax.swing.JPanel();
        bot_c_averia = new javax.swing.JButton();
        lbl_salir = new javax.swing.JLabel();
        jp_cerrar_Ave = new javax.swing.JPanel();
        lbl_t_res = new javax.swing.JLabel();
        lbl_n_res = new javax.swing.JLabel();
        lbl_E_reparacion = new javax.swing.JLabel();
        txt_t_res = new javax.swing.JTextField();
        txt_n_res = new javax.swing.JTextField();
        lbl_f_Aten = new javax.swing.JLabel();
        jcalendar_2 = new com.toedter.calendar.JDateChooser();
        txt_e_reparacion = new javax.swing.JTextField();
        lbl_fecha_r_titulo = new javax.swing.JLabel();
        txt_num_refencia = new javax.swing.JTextField();
        jp_obser = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txA_o_tele = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        txA_a_correccion = new javax.swing.JTextArea();
        lbl_a_correccion = new javax.swing.JLabel();
        lbl_o_tele = new javax.swing.JLabel();
        lbl_regis = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SCRAS");
        setMinimumSize(new java.awt.Dimension(695, 470));
        setPreferredSize(new java.awt.Dimension(695, 470));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jp_fondo.setMinimumSize(new java.awt.Dimension(700, 700));
        jp_fondo.setPreferredSize(new java.awt.Dimension(700, 700));
        jp_fondo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bot_c_averia.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bot_c_averia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar.png"))); // NOI18N
        bot_c_averia.setToolTipText("Cerrar Averia");
        bot_c_averia.setContentAreaFilled(false);
        bot_c_averia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_c_averia.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar2.png"))); // NOI18N
        bot_c_averia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_c_averiaActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_c_averia, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 540, 80, 80));

        lbl_salir.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir.setText("Volver");
        lbl_salir.setToolTipText("Menu Principal");
        lbl_salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jp_fondo.add(lbl_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 610, 80, 30));

        jp_cerrar_Ave.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cerrar Avería", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 18))); // NOI18N
        jp_cerrar_Ave.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_t_res.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_t_res.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_t_res.setText("Telefono:");
        jp_cerrar_Ave.add(lbl_t_res, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 200, -1, 30));

        lbl_n_res.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_n_res.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_n_res.setText("Nombre del Responsable:");
        jp_cerrar_Ave.add(lbl_n_res, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, 160, 30));

        lbl_E_reparacion.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_E_reparacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_E_reparacion.setText("Empresa Encargada:");
        jp_cerrar_Ave.add(lbl_E_reparacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 160, 30));

        txt_t_res.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txt_t_res.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_t_resKeyTyped(evt);
            }
        });
        jp_cerrar_Ave.add(txt_t_res, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 200, 200, 30));

        txt_n_res.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txt_n_res.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_n_resKeyTyped(evt);
            }
        });
        jp_cerrar_Ave.add(txt_n_res, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 160, 430, 30));

        lbl_f_Aten.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_f_Aten.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_f_Aten.setText("Fecha de Atención:");
        jp_cerrar_Ave.add(lbl_f_Aten, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 140, 30));
        jp_cerrar_Ave.add(jcalendar_2, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 80, 150, 30));

        txt_e_reparacion.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txt_e_reparacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_e_reparacionKeyTyped(evt);
            }
        });
        jp_cerrar_Ave.add(txt_e_reparacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 120, 430, 30));

        lbl_fecha_r_titulo.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_fecha_r_titulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_fecha_r_titulo.setText("Numero de Referencia");
        jp_cerrar_Ave.add(lbl_fecha_r_titulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 180, 30));

        txt_num_refencia.setEditable(false);
        txt_num_refencia.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jp_cerrar_Ave.add(txt_num_refencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 40, 200, 30));

        jp_fondo.add(jp_cerrar_Ave, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 630, 240));

        jp_obser.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Observaciones", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 18))); // NOI18N
        jp_obser.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txA_o_tele.setColumns(20);
        txA_o_tele.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txA_o_tele.setRows(5);
        txA_o_tele.setWrapStyleWord(true);
        txA_o_tele.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txA_o_teleKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(txA_o_tele);

        jp_obser.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 50, 370, -1));

        txA_a_correccion.setColumns(20);
        txA_a_correccion.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txA_a_correccion.setRows(5);
        txA_a_correccion.setWrapStyleWord(true);
        txA_a_correccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txA_a_correccionKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txA_a_correccionKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(txA_a_correccion);

        jp_obser.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 160, 370, -1));

        lbl_a_correccion.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_a_correccion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_a_correccion.setText("Acciones de Corrección de Falla: ");
        jp_obser.add(lbl_a_correccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, 220, 30));

        lbl_o_tele.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbl_o_tele.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_o_tele.setText("Observaciones para Telepuerto:");
        jp_obser.add(lbl_o_tele, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 200, 30));

        jp_fondo.add(jp_obser, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, 630, 270));

        lbl_regis.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_regis.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_regis.setText("Registrar");
        jp_fondo.add(lbl_regis, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 610, 80, 30));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/volver (1).png"))); // NOI18N
        jButton1.setToolTipText("Menu Principal");
        jButton1.setContentAreaFilled(false);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/volver (2).png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jp_fondo.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 540, 80, 80));

        SDP_Cierre_Averias.setViewportView(jp_fondo);

        getContentPane().add(SDP_Cierre_Averias, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 680, 450));

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void bot_c_averiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_c_averiaActionPerformed
    ResultSet res1;
    Integer id_tick=0;
    int dia = jcalendar_2.getCalendar().get(Calendar.DAY_OF_MONTH);
    int mes = jcalendar_2.getCalendar().get(Calendar.MONTH)+1;
    int año = jcalendar_2.getCalendar().get(Calendar.YEAR);
    String fecha= dia+ "/"+mes+"/"+año ;
    
            
         
    res1=operaciones.id_tick(txt_num_refencia.getText()); //CAPTURAMOS LA CLAVE PRIMARIA DE LA TABLA AVERIAS REPORTADAS
    try{
       while(res1.next()){
        id_tick=Integer.parseInt(res1.getString("id_tick"));
           System.out.print(id_tick);
          }
    }catch(SQLException e){
    
    }//FIN CAPTURA DE CLAVE PRIMARIA DE LA TABLA AVERIAS REPORTADAS
 
    ResultSet res2;
    Integer id_pedido=0;
 
    res2=operaciones.id_pedido(txt_num_refencia.getText()); //CAPTURAMOS LA CLAVE PRIMARIA DE LA TABLA DATOS TECNICOS
    try{
       while(res2.next()){
        id_pedido=Integer.parseInt(res2.getString("n_dato"));
          System.out.print(id_pedido);
         
          }
    }catch(SQLException e){
    
    }//FIN CAPTURA DE CLAVE PRIMARIA DE LA TABLA DATOS TECNICOS
    
    
   
                     
                
                boolean bandera=false;   
                 if (operaciones.insertar_cierre_averia(fecha,txt_e_reparacion.getText(),txA_o_tele.getText(),txA_a_correccion.getText(), txt_n_res.getText(), txt_t_res.getText(),id_tick, id_pedido,txt_num_refencia.getText())) {
                   JOptionPane.showMessageDialog(null, "Cierre de Averia exitosa");
                 bandera=true;
                   
                 
                 }else{
                     JOptionPane.showMessageDialog(null, "Cierre de Averia no  exitosa");
                
                   }
                 
                 
               
//                   
//                
                   if(operaciones.borrar_averia_abierta(txt_num_refencia.getText())){
                         JOptionPane.showMessageDialog(null, "Eliminacion exitosa");
                            dispose();
                            registro2.dispose();
                 } else {
                     
                     JOptionPane.showMessageDialog(null, "Registro no exitoso de Cierre de Averia");
                 }
                 
                   
                   
                  


    }//GEN-LAST:event_bot_c_averiaActionPerformed

    private void txt_e_reparacionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_e_reparacionKeyTyped
               char car = evt.getKeyChar();
        if (txt_e_reparacion.getText().length() >= 200) {//la longitud del campo de texto la pones deacuerdo a tu necesidad
            evt.consume();
        }

        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') // solo letras
                
                 //aqui pondras cualquier simbolo letra o numero que quieras que te 
                // acepte el sistema de cuerdo al codigo asci
                
                && car != 'á' //Minúsculas

                && car != 'é'
                && car != 'í'
                && car != 'ó'
                && car != 'ú'
                && car != 'Á' //Mayúsculas

                && car != 'É'
                && car != 'Í'
                && car != 'Ó'
                && car != 'Ú'
                && (car != (char) KeyEvent.VK_SPACE)) //barra espaciadora
        {

            evt.consume();

        }
    }//GEN-LAST:event_txt_e_reparacionKeyTyped

    private void txt_n_resKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_n_resKeyTyped
               char car = evt.getKeyChar();
        if (txt_n_res.getText().length() >= 200) {//la longitud del campo de texto la pones deacuerdo a tu necesidad
            evt.consume();
        }
        
        
        
       
        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') // solo letras
                
                 //aqui pondras cualquier simbolo letra o numero que quieras que te 
                // acepte el sistema de cuerdo al codigo asci
                
                && car != 'á' //Minúsculas

                && car != 'é'
                && car != 'í'
                && car != 'ó'
                && car != 'ú'
                && car != 'Á' //Mayúsculas

                && car != 'É'
                && car != 'Í'
                && car != 'Ó'
                && car != 'Ú'
                && (car != (char) KeyEvent.VK_SPACE)) //barra espaciadora
        {

            evt.consume();

        }
    }//GEN-LAST:event_txt_n_resKeyTyped

    private void txt_t_resKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_t_resKeyTyped
                  //solo numerros

        char car = evt.getKeyChar();
        if (txt_t_res.getText().length() >= 11) { //la longitud del campo de texto la pones deacuerdo a tu necesidad
            evt.consume();
        }
        if ((car < '0' || car > '9')
                
                   //aqui pondras cualquier simbolo letra o numero que quieras que te 
                // acepte el sistema de cuerdo al codigo asci
                
                && car != 46) {

            evt.consume();

        }
    }//GEN-LAST:event_txt_t_resKeyTyped

    private void txA_o_teleKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txA_o_teleKeyTyped
                       char car = evt.getKeyChar();
        if (txA_o_tele.getText().length() >= 500) {//la longitud del campo de texto la pones deacuerdo a tu necesidad
            evt.consume();
        }

        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') // solo letras
                
                 //aqui pondras cualquier simbolo letra o numero que quieras que te 
                // acepte el sistema de cuerdo al codigo asci
                
                && car != 'á' //Minúsculas

                && car != 'é'
                && car != 'í'
                && car != 'ó'
                && car != 'ú'
                && car != 'Á' //Mayúsculas

                && car != 'É'
                && car != 'Í'
                && car != 'Ó'
                && car != 'Ú'
                && (car != (char) KeyEvent.VK_SPACE)) //barra espaciadora
        {

            evt.consume();

        }
    }//GEN-LAST:event_txA_o_teleKeyTyped

    private void txA_a_correccionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txA_a_correccionKeyTyped
                       char car = evt.getKeyChar();
        if (txA_a_correccion.getText().length() >= 500) {//la longitud del campo de texto la pones deacuerdo a tu necesidad
            evt.consume();
        }

        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') // solo letras
                
                 //aqui pondras cualquier simbolo letra o numero que quieras que te 
                // acepte el sistema de cuerdo al codigo asci
                
                && car != 'á' //Minúsculas

                && car != 'é'
                && car != 'í'
                && car != 'ó'
                && car != 'ú'
                && car != 'Á' //Mayúsculas

                && car != 'É'
                && car != 'Í'
                && car != 'Ó'
                && car != 'Ú'
                && (car != (char) KeyEvent.VK_SPACE)) //barra espaciadora
        {

            evt.consume();

        }
    }//GEN-LAST:event_txA_a_correccionKeyTyped

    private void txA_a_correccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txA_a_correccionKeyPressed
           
        /*codigo que se pone en el ultimo campo de texto para que cuando
        le des enter se active el boton por ejemplo de guardar*/
        
        if(evt.getKeyCode()==10){
        bot_c_averia.doClick();

     }
    }//GEN-LAST:event_txA_a_correccionKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(scras_cierre_averias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(scras_cierre_averias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(scras_cierre_averias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(scras_cierre_averias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new scras_cierre_averias().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane SDP_Cierre_Averias;
    private javax.swing.JButton bot_c_averia;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private com.toedter.calendar.JDateChooser jcalendar_2;
    private javax.swing.JPanel jp_cerrar_Ave;
    private javax.swing.JPanel jp_fondo;
    private javax.swing.JPanel jp_obser;
    private javax.swing.JLabel lbl_E_reparacion;
    private javax.swing.JLabel lbl_a_correccion;
    private javax.swing.JLabel lbl_f_Aten;
    private javax.swing.JLabel lbl_fecha_r_titulo;
    private javax.swing.JLabel lbl_n_res;
    private javax.swing.JLabel lbl_o_tele;
    private javax.swing.JLabel lbl_regis;
    private javax.swing.JLabel lbl_salir;
    private javax.swing.JLabel lbl_t_res;
    private javax.swing.JTextArea txA_a_correccion;
    private javax.swing.JTextArea txA_o_tele;
    public static javax.swing.JTextField txt_e_reparacion;
    private javax.swing.JTextField txt_n_res;
    private javax.swing.JTextField txt_num_refencia;
    private javax.swing.JTextField txt_t_res;
    // End of variables declaration//GEN-END:variables
}
