/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dayana Martinez <dmaro006@gmail.com>
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import Control_bd.*;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.table.TableColumnModel;

public class scras_cargar_archivo extends javax.swing.JFrame {
 
boolean bandera=true;
public DefaultTableModel model = new DefaultTableModel();

  
operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
ResultSet res;//creacion de la variable resultset

    public DefaultTableModel modelo_tabla = new DefaultTableModel() {
            // codigo para las que las celdas del jtbale no sean editables
            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return false; // el resto de las celdas no son editables

            }

    };


    public scras_cargar_archivo() {
        initComponents();
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        fila_columnas(jt_despliegue);
         setIconImage(new ImageIcon(getClass().getResource("/imagenes/scras_icono.png")).getImage()); 
   
//aqui para asignarle un tamñao a cada columna individualmente
TableColumnModel columnModel = jt_despliegue.getColumnModel();
columnModel.getColumn(0).setPreferredWidth(100);
columnModel.getColumn(1).setPreferredWidth(150);
columnModel.getColumn(2).setPreferredWidth(150);
columnModel.getColumn(3).setPreferredWidth(150);
columnModel.getColumn(4).setPreferredWidth(150);
columnModel.getColumn(5).setPreferredWidth(150);
columnModel.getColumn(6).setPreferredWidth(210);
columnModel.getColumn(7).setPreferredWidth(150);
columnModel.getColumn(8).setPreferredWidth(150);
columnModel.getColumn(9).setPreferredWidth(150);
columnModel.getColumn(10).setPreferredWidth(150);
columnModel.getColumn(11).setPreferredWidth(150);
columnModel.getColumn(12).setPreferredWidth(150);
columnModel.getColumn(13).setPreferredWidth(150);
columnModel.getColumn(14).setPreferredWidth(150);
columnModel.getColumn(15).setPreferredWidth(150);
columnModel.getColumn(16).setPreferredWidth(150);
columnModel.getColumn(17).setPreferredWidth(150);
columnModel.getColumn(18).setPreferredWidth(150);
columnModel.getColumn(19).setPreferredWidth(150);
columnModel.getColumn(20).setPreferredWidth(150);
columnModel.getColumn(21).setPreferredWidth(150);
columnModel.getColumn(22).setPreferredWidth(150);
columnModel.getColumn(23).setPreferredWidth(150);
columnModel.getColumn(24).setPreferredWidth(150);
columnModel.getColumn(25).setPreferredWidth(150);
columnModel.getColumn(26).setPreferredWidth(150);
columnModel.getColumn(27).setPreferredWidth(150);
columnModel.getColumn(28).setPreferredWidth(150);
columnModel.getColumn(29).setPreferredWidth(150);
columnModel.getColumn(30).setPreferredWidth(180);
columnModel.getColumn(31).setPreferredWidth(150);
columnModel.getColumn(32).setPreferredWidth(150);
columnModel.getColumn(33).setPreferredWidth(150);
columnModel.getColumn(34).setPreferredWidth(150);
columnModel.getColumn(35).setPreferredWidth(150);
columnModel.getColumn(36).setPreferredWidth(150);
columnModel.getColumn(37).setPreferredWidth(150);
columnModel.getColumn(38).setPreferredWidth(150);
columnModel.getColumn(39).setPreferredWidth(150);
columnModel.getColumn(40).setPreferredWidth(150);
columnModel.getColumn(41).setPreferredWidth(150);
columnModel.getColumn(42).setPreferredWidth(150);
columnModel.getColumn(43).setPreferredWidth(150);
columnModel.getColumn(44).setPreferredWidth(150);
columnModel.getColumn(45).setPreferredWidth(150);
columnModel.getColumn(46).setPreferredWidth(150);
columnModel.getColumn(47).setPreferredWidth(150);
columnModel.getColumn(48).setPreferredWidth(150);
columnModel.getColumn(49).setPreferredWidth(150);
columnModel.getColumn(50).setPreferredWidth(150);
columnModel.getColumn(51).setPreferredWidth(150);
columnModel.getColumn(52).setPreferredWidth(150);
columnModel.getColumn(53).setPreferredWidth(150);
columnModel.getColumn(54).setPreferredWidth(150);
columnModel.getColumn(55).setPreferredWidth(150);
columnModel.getColumn(56).setPreferredWidth(150);
columnModel.getColumn(57).setPreferredWidth(150);
columnModel.getColumn(58).setPreferredWidth(150);
columnModel.getColumn(59).setPreferredWidth(150);
columnModel.getColumn(60).setPreferredWidth(150);
columnModel.getColumn(61).setPreferredWidth(150);
columnModel.getColumn(62).setPreferredWidth(150);
columnModel.getColumn(63).setPreferredWidth(150);
columnModel.getColumn(64).setPreferredWidth(150);
columnModel.getColumn(65).setPreferredWidth(150);
columnModel.getColumn(66).setPreferredWidth(150);
columnModel.getColumn(67).setPreferredWidth(150);
columnModel.getColumn(68).setPreferredWidth(150);


    }
    
      public  JTable fila_columnas(JTable tabla){
          
    //  Para el desplazamiento horizoltal del scrollpane en el jtable       
    jt_despliegue.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    jt_despliegue.doLayout(); 
   //inabilita el movimiento de las columns   del jtable  
    jt_despliegue.getTableHeader().setReorderingAllowed(false);
    jt_despliegue.getTableHeader().setResizingAllowed(false);
        
      
    model.addColumn("Pedido");
    model.addColumn("Repeticiones Pedido");
    model.addColumn("Mes de Creacion");
    model.addColumn("Orden");
    model.addColumn("Orden 2");
    model.addColumn("Codigo de Proyecto");
    model.addColumn("Repeticiones cod. Pedido");
    model.addColumn("MTTO Adecuaciones");
    model.addColumn("Circuito");
    model.addColumn("Telepuerto");
    model.addColumn("Estatus");
    model.addColumn("Buzon Tareas");
    model.addColumn("Buzon de Devolucion");
    model.addColumn("Fecha Incidencia");
    model.addColumn("Fecha de Asignacion");
    model.addColumn("Fase");
    model.addColumn("Tipo Plan");
    model.addColumn("Tipo Proyecto");
    model.addColumn("tipo Sector");
    model.addColumn("Fase Proyecto");
    model.addColumn("Sector");
    model.addColumn("Tipo Plan General");
    model.addColumn("Region");
    model.addColumn("Estado");
    model.addColumn("Ciudad");
    model.addColumn("Municipio");
    model.addColumn("Parroquia");
    model.addColumn("Unidad de Negocios");
    model.addColumn("Nombre del Cliente Principal");
    model.addColumn("Nombre");
    model.addColumn("Direccion Completa");
    model.addColumn("Nombre 2");
    model.addColumn("Direccion 2");
    model.addColumn("Contacto");
    model.addColumn("Telefono"); 
    model.addColumn("Contacto 2");
    model.addColumn("Telefono 2");
    model.addColumn("Estatus Red");
    model.addColumn("Año");
    model.addColumn("Mes");
    model.addColumn("Fecha Plan");
    model.addColumn("Empresa Plan");
    model.addColumn("Prioridad");
    model.addColumn("Empresa Instaladora");
    model.addColumn("Fecha Instalacion");
    model.addColumn("Nombre Instalador");
    model.addColumn("Empresa Retiro Plan");
    model.addColumn("Empresa Retiro");
    model.addColumn("Fecha Retiro");
    model.addColumn("Telefono Instalador");
    model.addColumn("QoS Operativo");
    model.addColumn("MAC");
    model.addColumn("Despliegue IP");
    model.addColumn("IPoam");
    model.addColumn("Codigo de Cierre");
    model.addColumn("Telepuerto Operativo");
    model.addColumn("Observacion");
    model.addColumn("Observacion General");
    model.addColumn("Cadena");
    model.addColumn("QoS M6");
    model.addColumn("Modem Marca");
    model.addColumn("Modem Modelo");
    model.addColumn("Modem Serial");
    model.addColumn("LNB Marca");
    model.addColumn("LNB Modelo");
    model.addColumn("LNB serial");
    model.addColumn("BUC Marca");
    model.addColumn("BUC Modelo");
    model.addColumn("BUC Serial");
  
    
    tabla.setModel(model);
     
return tabla;
  }

Object [] arreglo = new Object[68]; //Se guadar los datos en la tabla



  //metodo para tener inabilitas las columnas del jtable
  public void inabilitar_columnas_jtable(JTable tabla){ 
  
   //inabilita el movimiento de las columns   del jtable  
    tabla.getTableHeader().setReorderingAllowed(false);
    tabla.getTableHeader().setResizingAllowed(false);
  }
  
  
    //metodo para tener inabilitas las celdas del modelo
  public void inabilitar_celdas_jtable(DefaultTableModel model){
   model = new DefaultTableModel() {
            // codigo para las que las celdas del jtbale no sean editables
            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return false; // el resto de las celdas no son editables

            }

    };  
 }
 
  
   

   //CODIGO PARA FORMATO EXCEL XLS
  
   public JTable CreerJTableAvecExcel_xls(File file) throws FileNotFoundException, IOException{
       
        InputStream inp = new FileInputStream(file);
        HSSFWorkbook wb = new HSSFWorkbook(new POIFSFileSystem(inp));//Lecture du fichier excel

        HSSFSheet sheet = wb.getSheetAt(0);//Obtén la página 1 del fichero xls
        
//        int Nrow = sheet.getLastRowNum(10);
        int Nrow = sheet.getLastRowNum()-13830;
        System.out.println("nr "+Nrow);//cuenta el número de filas
        
        int st = NombreMaxColonne_xls(sheet);//cuenta número de columnas
            
		
        
        Object[][] o = new Object[Nrow][st];//crear una nueva tabla de objetos
	  Object a[] = new Object[st];	
////la parte superior de la tabla
       
       //hoja de ruta y las líneas de uno a uno nos recup
    
        for(int i = 0; i<Nrow;i++){
            HSSFRow row = sheet.getRow(i+1);//ruta de la línea para recuperar columnas
           
            if(row!=null){
                for(int j =0;j<st;j++){//Obtiene la célda y su valor
					
                    
                    HSSFCell cell = row.getCell(j);
                    Object value = ContenuCellule_xls(cell);
              
                      
                      a[j] = value;
                       if(a[j]==null){
                        a[j]="  ";
                        }
                                  
                               
                    
                }
                  model.addRow(a);
                 
            }
            
        }
        
     jt_despliegue.setModel(model);
        inp.close();
        
        return jt_despliegue;
    }   
   
//La celda puede contener diferentes tipos de valor que debe ser recuperado en concreto
   
    private static Object ContenuCellule_xls(HSSFCell cell){
        Object value = null ;
        
        if(cell == null){
            value = "";
        }
        
        else if(cell.getCellType() == HSSFCell.CELL_TYPE_BOOLEAN){
            value = cell.getBooleanCellValue();
        }
        else if(cell.getCellType() == HSSFCell.CELL_TYPE_ERROR){
            value = cell.getErrorCellValue();
        }
        else if(cell.getCellType() == HSSFCell.CELL_TYPE_FORMULA){
            value = cell.getCellFormula();
        }
        else if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
            value = cell.getNumericCellValue();
        }
        else if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
            value = cell.getStringCellValue();
        }
        return value;
                
    }	
		
    private static int NombreMaxColonne_xls(HSSFSheet sheet){//Número máximo de columnas
        
       int r = sheet.getLastRowNum();
        int max = 0;
        int s = 0;
       while(s<r){
           if(sheet.getRow(s) != null){
               int c = sheet.getRow(s).getLastCellNum();
               if(c>max){
                   max = c;
               }
           }
           s++;
           
       }
        return max;
    }
    
     //CODIGO PARA FORMATO EXCEL XLSX
    
    public JTable CreerJTableAvecExcel_xlsx(File file) throws FileNotFoundException, IOException{
       
        InputStream inp = new FileInputStream(file);
        XSSFWorkbook wb = new XSSFWorkbook(inp);//Lecture de fichiero excel
 
        XSSFSheet sheet = wb.getSheetAt(0);//Obtén la página 1 del fichero xls
        
        int Nrow = sheet.getLastRowNum();
        System.out.println("nr "+Nrow); //cuenta el número de filas
        
        int st = NombreMaxColonne_xlsx(sheet); //cuenta número de columnas

          Object[][] o = new Object[Nrow][st];
	  Object a[] = new Object[st];	 //crear una nueva tabla de objetos
        
//la parte superior de la tabla
     
     for(int i = 0; i<Nrow;i++){
         
            XSSFRow row = sheet.getRow(i+1); //hoja de ruta y las líneas de uno a uno nos recupera
             
            //ruta de la línea para recuperar columnas
            
            if(row!=null){
                for(int j =0;j<st;j++){//Obtiene la célda y su valor
					
                    
                    XSSFCell cell = row.getCell(j);
                    Object value = ContenuCellule_xlsx(cell);
                   
                   
                  
                                 
                                    a[j] = value;
                                    if(a[j]==null){
                                    a[j]="  ";
                                    }
                                    
                                  
                               }
                  model.addRow(a);
                 
            }
            
        }
        
     jt_despliegue.setModel(model);
       
        
        inp.close();
        
        return jt_despliegue;
    }
    
    private static Object ContenuCellule_xlsx(XSSFCell cell){
        Object value = null ;
        
        if(cell == null){
            value = "";
        }
        
        else if(cell.getCellType() == XSSFCell.CELL_TYPE_BOOLEAN){
            value = cell.getBooleanCellValue();
        }
        else if(cell.getCellType() == XSSFCell.CELL_TYPE_ERROR){
            value = cell.getErrorCellValue();
        }
        else if(cell.getCellType() == XSSFCell.CELL_TYPE_FORMULA){
            value = cell.getCellFormula();
        }
        else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC){
            value = cell.getNumericCellValue();
        }
        else if(cell.getCellType() == XSSFCell.CELL_TYPE_STRING){
            value = cell.getStringCellValue();
        }
        return value;
                
    }	
		
    private static int NombreMaxColonne_xlsx(XSSFSheet sheet){//Número máximo de columnas
        
       int r = sheet.getLastRowNum();
        int max = 0;
        int s = 0;
       while(s<r){
           if(sheet.getRow(s) != null){
               int c = sheet.getRow(s).getLastCellNum();
               if(c>max){
                   max = c;
               }
           }
           s++;
           
       }
        return max;
    } 
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jp_fondo = new javax.swing.JPanel();
        bot_cargar = new javax.swing.JButton();
        lbl_titulo = new javax.swing.JLabel();
        txt_cargar = new javax.swing.JTextField();
        jp_despliegue = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jt_despliegue = new javax.swing.JTable();
        bot_guardar = new javax.swing.JButton();
        bot_mostrar = new javax.swing.JButton();
        bot_nuevo = new javax.swing.JButton();
        bot_salir = new javax.swing.JButton();
        lbl_volver = new javax.swing.JLabel();
        lbl_guardar = new javax.swing.JLabel();
        lbl_nuevo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SCRAS");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jp_fondo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bot_cargar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar (1).png"))); // NOI18N
        bot_cargar.setToolTipText("Seleccionar un Nuevo Archivo");
        bot_cargar.setContentAreaFilled(false);
        bot_cargar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_cargar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar (2).png"))); // NOI18N
        bot_cargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_cargarActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_cargar, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 50, 60, 50));

        lbl_titulo.setFont(new java.awt.Font("Times New Roman", 3, 24)); // NOI18N
        lbl_titulo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbl_titulo.setText("Nuevo Archivo");
        lbl_titulo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jp_fondo.add(lbl_titulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 200, 30));

        txt_cargar.setEditable(false);
        txt_cargar.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        jp_fondo.add(txt_cargar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 360, 30));

        jp_despliegue.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Archivo de Despliegue Satelital", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 18))); // NOI18N
        jp_despliegue.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jt_despliegue.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jt_despliegue);

        jp_despliegue.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 880, 220));

        jp_fondo.add(jp_despliegue, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 900, 270));

        bot_guardar.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        bot_guardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar.png"))); // NOI18N
        bot_guardar.setToolTipText("Guardar el nuevo archivo satelital");
        bot_guardar.setContentAreaFilled(false);
        bot_guardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_guardar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar2.png"))); // NOI18N
        bot_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_guardarActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_guardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 380, 70, 70));

        bot_mostrar.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        bot_mostrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/actual (1).png"))); // NOI18N
        bot_mostrar.setToolTipText("Mostrar el archivo mas reciente ");
        bot_mostrar.setContentAreaFilled(false);
        bot_mostrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_mostrar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/actual (2).png"))); // NOI18N
        bot_mostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_mostrarActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_mostrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 40, 80, 60));

        bot_nuevo.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        bot_nuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/subir (1).png"))); // NOI18N
        bot_nuevo.setToolTipText("Realizar una nueva seleccion");
        bot_nuevo.setContentAreaFilled(false);
        bot_nuevo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_nuevo.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/subir (2).png"))); // NOI18N
        bot_nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_nuevoActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_nuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 380, 70, 70));

        bot_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/volver (1).png"))); // NOI18N
        bot_salir.setToolTipText("Menu Principal");
        bot_salir.setContentAreaFilled(false);
        bot_salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_salir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/volver (2).png"))); // NOI18N
        bot_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_salirActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 380, 70, 70));

        lbl_volver.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_volver.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_volver.setText("Volver");
        jp_fondo.add(lbl_volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 440, 70, 20));

        lbl_guardar.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_guardar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_guardar.setText("Guardar");
        jp_fondo.add(lbl_guardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 440, 70, 20));

        lbl_nuevo.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_nuevo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_nuevo.setText("Nuevo");
        jp_fondo.add(lbl_nuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 440, 70, 20));

        getContentPane().add(jp_fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, -10, 920, 490));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bot_nuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_nuevoActionPerformed
        try{
            for (int i=0;
                i<model.getRowCount();i--){
                for (int j=0;
                    j<model.getRowCount();j--){
                    model.removeRow(0);
                }

            }
        }catch (Exception e)  {

        }
        txt_cargar.setText("");
    }//GEN-LAST:event_bot_nuevoActionPerformed

    private void bot_mostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_mostrarActionPerformed

        SCRAS_Mostrar_datos_despliegue mostrar_todo = new SCRAS_Mostrar_datos_despliegue();
        mostrar_todo.setVisible(true);
        //        mostrar_todo.setBounds(900, 400, WIDTH, WIDTH);

    }//GEN-LAST:event_bot_mostrarActionPerformed
  
    public String tbl(JTable  jt_despliegue,Integer i, Integer j){
        String cadena="";
    
      String caracter=jt_despliegue.getValueAt(i, j).toString();
      int  c=caracter.length();
//        System.out.println ("Nombre Original:  "+jt_despliegue.getValueAt(i, j).toString());
          for (int  contador =0; contador<c; contador++){
            if ( jt_despliegue.getValueAt(i, j).toString().charAt(contador)=='\''){
              
             ;
               
            }else{
              jt_despliegue.getValueAt(i, j).toString().replace('\'',' ');
                cadena=cadena+ jt_despliegue.getValueAt(i, j).toString().charAt(contador);
            }
   
          }
     
     return  cadena;
    }  
    private void bot_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_guardarActionPerformed
     try{
      try{ 
          
          if(txt_cargar.getText().equals("")){
          
          JOptionPane.showMessageDialog(null,"Disculpe Importe un Archivo para Poder Registrar");
          }else{
        try {
        res=operaciones.despliegue_lleno();
        if (res.next()) //verifica si esta vacio, pero desplaza el puntero al siguiente elemento
          {
           res.beforeFirst();//esta regresa el puntero al inicio para no perder el 1er dato
           
           int seleccion = JOptionPane.showOptionDialog(
             this, 
                "¿Deseea Reemplazar el  Archivo Existente?", //Mensaje
                 "Seleccione una opción", // Título
                   JOptionPane.YES_NO_CANCEL_OPTION,
                   JOptionPane.QUESTION_MESSAGE,
                   null,    // null para icono por defecto.
                   new Object[] { "Si", "No"},    // null para YES, NO y CANCEL
                   "Si");
    
                if (seleccion != -1) {
                      if((seleccion + 1)==1) {
                         // PRESIONO SI
                          operaciones.borrar_despliegue();
                           if( operaciones.borrar_despliegue()){
                                      // la tabla despliegue esta vacia
         for(int i=0; i<jt_despliegue.getRowCount();i++){ 

   
   operaciones.insertar_despliegue(tbl(jt_despliegue,i, 0),
          tbl(jt_despliegue,i, 1),tbl(jt_despliegue,i, 2),
          tbl(jt_despliegue, i,3), tbl(jt_despliegue, i,4),
          tbl(jt_despliegue,i, 5),tbl(jt_despliegue,i, 6),
          tbl(jt_despliegue,i, 7),tbl(jt_despliegue,i, 8),
          tbl(jt_despliegue,i, 9),tbl(jt_despliegue, i,10),
          tbl(jt_despliegue,i, 11),tbl(jt_despliegue,i, 12),
          tbl(jt_despliegue,i,13),tbl(jt_despliegue, i,14),
          tbl(jt_despliegue, i,15), tbl(jt_despliegue,i, 16),
          tbl(jt_despliegue,i, 17), tbl(jt_despliegue,i,18),
          tbl(jt_despliegue,i,19), tbl(jt_despliegue,i,20),
          tbl(jt_despliegue,i, 21),tbl(jt_despliegue,i, 22),
          tbl(jt_despliegue,i,23), tbl(jt_despliegue,i, 24),
          tbl(jt_despliegue, i,25),tbl(jt_despliegue, i,26),
          tbl(jt_despliegue,i, 27),tbl(jt_despliegue,i, 28),
          tbl(jt_despliegue, i,29),tbl(jt_despliegue, i,30),
          tbl(jt_despliegue,i, 31),tbl(jt_despliegue,i, 32),
          tbl(jt_despliegue,i,33),tbl(jt_despliegue,i, 34),
          tbl(jt_despliegue,i, 35), tbl(jt_despliegue, i,36),
          tbl(jt_despliegue, i,37), tbl(jt_despliegue,i,38),
          tbl(jt_despliegue,i,39), tbl(jt_despliegue,i,40),
          tbl(jt_despliegue, i,41),tbl(jt_despliegue,i,42),
          tbl(jt_despliegue,i,43), tbl(jt_despliegue,i, 44),
          tbl(jt_despliegue, i,45),tbl(jt_despliegue,i, 46),
          tbl(jt_despliegue,i, 47),tbl(jt_despliegue, i,48),
          tbl(jt_despliegue, i,49),tbl(jt_despliegue,i, 50),
          tbl(jt_despliegue,i, 51),tbl(jt_despliegue, i,52),
          tbl(jt_despliegue,i,53),tbl(jt_despliegue,i, 54),
          tbl(jt_despliegue, i,55), tbl(jt_despliegue,i, 56),
          tbl(jt_despliegue,i, 57), tbl(jt_despliegue,i,58),
          tbl(jt_despliegue,i,59), tbl(jt_despliegue,i,60),
           tbl(jt_despliegue, i,61),tbl(jt_despliegue,i,62),
          tbl(jt_despliegue, i,63), tbl(jt_despliegue, i,64),
          tbl(jt_despliegue,i, 65),tbl(jt_despliegue,i, 66),
          tbl(jt_despliegue,i, 67),tbl(jt_despliegue,i, 68));
      
       
          }
          JOptionPane.showMessageDialog(null,"Registro Exitoso");
                               
    }
                     }else {
                         // PRESIONO NO
                    }
                 }
           res.close();
       }

          else{
        
          // la tabla despliegue esta vacia
         for(int i=0; i<jt_despliegue.getRowCount();i++){ 

   
   operaciones.insertar_despliegue(tbl(jt_despliegue,i, 0),
          tbl(jt_despliegue,i, 1),tbl(jt_despliegue,i, 2),
          tbl(jt_despliegue, i,3), tbl(jt_despliegue, i,4),
          tbl(jt_despliegue,i, 5),tbl(jt_despliegue,i, 6),
          tbl(jt_despliegue,i, 7),tbl(jt_despliegue,i, 8),
          tbl(jt_despliegue,i, 9),tbl(jt_despliegue, i,10),
          tbl(jt_despliegue,i, 11),tbl(jt_despliegue,i, 12),
          tbl(jt_despliegue,i,13),tbl(jt_despliegue, i,14),
          tbl(jt_despliegue, i,15), tbl(jt_despliegue,i, 16),
          tbl(jt_despliegue,i, 17), tbl(jt_despliegue,i,18),
          tbl(jt_despliegue,i,19), tbl(jt_despliegue,i,20),
          tbl(jt_despliegue,i, 21),tbl(jt_despliegue,i, 22),
          tbl(jt_despliegue,i,23), tbl(jt_despliegue,i, 24),
          tbl(jt_despliegue, i,25),tbl(jt_despliegue, i,26),
          tbl(jt_despliegue,i, 27),tbl(jt_despliegue,i, 28),
          tbl(jt_despliegue, i,29),tbl(jt_despliegue, i,30),
          tbl(jt_despliegue,i, 31),tbl(jt_despliegue,i, 32),
          tbl(jt_despliegue,i,33),tbl(jt_despliegue,i, 34),
          tbl(jt_despliegue,i, 35), tbl(jt_despliegue, i,36),
          tbl(jt_despliegue, i,37), tbl(jt_despliegue,i,38),
          tbl(jt_despliegue,i,39), tbl(jt_despliegue,i,40),
          tbl(jt_despliegue, i,41),tbl(jt_despliegue,i,42),
          tbl(jt_despliegue,i,43), tbl(jt_despliegue,i, 44),
          tbl(jt_despliegue, i,45),tbl(jt_despliegue,i, 46),
          tbl(jt_despliegue,i, 47),tbl(jt_despliegue, i,48),
          tbl(jt_despliegue, i,49),tbl(jt_despliegue,i, 50),
          tbl(jt_despliegue,i, 51),tbl(jt_despliegue, i,52),
          tbl(jt_despliegue,i,53),tbl(jt_despliegue,i, 54),
          tbl(jt_despliegue, i,55), tbl(jt_despliegue,i, 56),
          tbl(jt_despliegue,i, 57), tbl(jt_despliegue,i,58),
          tbl(jt_despliegue,i,59), tbl(jt_despliegue,i,60),
           tbl(jt_despliegue, i,61),tbl(jt_despliegue,i,62),
          tbl(jt_despliegue, i,63), tbl(jt_despliegue, i,64),
          tbl(jt_despliegue,i, 65),tbl(jt_despliegue,i, 66),
          tbl(jt_despliegue,i, 67),tbl(jt_despliegue,i, 68));
       
         }
        JOptionPane.showMessageDialog(null,"Registro Exitoso");
        }
        
        }catch(SQLException l){
            JOptionPane.showMessageDialog(null,"ERROR EN EL SISTEMA");
       }
      }
    }catch(Exception e){
     JOptionPane.showMessageDialog(null,"ERROR EN EL SISTEMA");
    }
   }catch(NullPointerException ex){
    
     JOptionPane.showMessageDialog(null,"ERROR EN EL SISTEMA");
 }   
       
        
        
              
        
    }//GEN-LAST:event_bot_guardarActionPerformed

    private void bot_cargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_cargarActionPerformed

//     ce.imp(jt_despliegue);   

        String [] operaciones = {"Archivo Excel 97-2003","Archivo Excel 2007..."};
        String valor =(String) JOptionPane.showInputDialog(this,"Abrir archivo como ","Acrhivos Excel",JOptionPane.INFORMATION_MESSAGE,null,operaciones,operaciones[0]);

        // clase_importar_excel importar_excel = new clase_importar_excel();
        try {
            JFileChooser examinar = new JFileChooser();

            if(valor.equals("Archivo Excel 97-2003")){

                //pasamos un fltro que recivira extensiones solo xls y xlsx
                examinar.setFileFilter(new FileNameExtensionFilter("Archivos excel","xls"));

                int opcion=examinar.showOpenDialog(this); //mostramos l ventana

                File archivo_excel = null;

                if(opcion == JFileChooser.APPROVE_OPTION){

                    archivo_excel = examinar.getSelectedFile().getAbsoluteFile();
                    
                    txt_cargar.setText(archivo_excel.getAbsolutePath());
                   
                    CreerJTableAvecExcel_xls(archivo_excel);
                }
            }else{
                if(valor.equals("Archivo Excel 2007...")){

                    //pasamos un fltro que recivira extensiones solo xls y xlsx
                    examinar.setFileFilter(new FileNameExtensionFilter("Archivos excel","xlsx"));

                    int opcion=examinar.showOpenDialog(this); //mostramos l ventana

                    File archivo_excel = null;

                    if(opcion == JFileChooser.APPROVE_OPTION){

                        archivo_excel = examinar.getSelectedFile().getAbsoluteFile();
                        
                        txt_cargar.setText(archivo_excel.getAbsolutePath());
                        
                        CreerJTableAvecExcel_xlsx(archivo_excel);
                    }

                }

            }

        } catch (IOException ex) {
            Logger.getLogger(scras_cargar_archivo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_bot_cargarActionPerformed

    private void bot_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_salirActionPerformed
       this.dispose();
    }//GEN-LAST:event_bot_salirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(scras_cargar_archivo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(scras_cargar_archivo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(scras_cargar_archivo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(scras_cargar_archivo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new scras_cargar_archivo().setVisible(true);
            }
        });
    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bot_cargar;
    private javax.swing.JButton bot_guardar;
    private javax.swing.JButton bot_mostrar;
    private javax.swing.JButton bot_nuevo;
    private javax.swing.JButton bot_salir;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel jp_despliegue;
    private javax.swing.JPanel jp_fondo;
    private javax.swing.JTable jt_despliegue;
    private javax.swing.JLabel lbl_guardar;
    private javax.swing.JLabel lbl_nuevo;
    private javax.swing.JLabel lbl_titulo;
    private javax.swing.JLabel lbl_volver;
    private javax.swing.JTextField txt_cargar;
    // End of variables declaration//GEN-END:variables
}
