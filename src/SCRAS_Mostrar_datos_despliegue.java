 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Control_bd.*;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Dayana Martinez <dmaro006@gmail.com>
 */

public class SCRAS_Mostrar_datos_despliegue extends javax.swing.JFrame {
public DefaultTableModel modelo_tabla = new DefaultTableModel(); 
//{
//            // codigo para las que las celdas del jtbale no sean editables
//            public boolean isCellEditable(int rowIndex, int columnIndex) {
//
//                return false; // el resto de las celdas no son editables
//
//            }
//
//    };  
operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
ResultSet res;//creacion de la variable resultset

    public SCRAS_Mostrar_datos_despliegue() {
        initComponents();
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        mostrar_todo(modelo_tabla);
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/scras_icono.png")).getImage()); 
   
//aqui para asignarle un tamñao a cada columna individualmente
TableColumnModel columnModel = jt_despliegue2.getColumnModel();
columnModel.getColumn(0).setPreferredWidth(100);
columnModel.getColumn(1).setPreferredWidth(150);
columnModel.getColumn(2).setPreferredWidth(150);
columnModel.getColumn(3).setPreferredWidth(150);
columnModel.getColumn(4).setPreferredWidth(150);
columnModel.getColumn(5).setPreferredWidth(150);
columnModel.getColumn(6).setPreferredWidth(210);
columnModel.getColumn(7).setPreferredWidth(150);
columnModel.getColumn(8).setPreferredWidth(150);
columnModel.getColumn(9).setPreferredWidth(150);
columnModel.getColumn(10).setPreferredWidth(150);
columnModel.getColumn(11).setPreferredWidth(150);
columnModel.getColumn(12).setPreferredWidth(150);
columnModel.getColumn(13).setPreferredWidth(150);
columnModel.getColumn(14).setPreferredWidth(150);
columnModel.getColumn(15).setPreferredWidth(150);
columnModel.getColumn(16).setPreferredWidth(150);
columnModel.getColumn(17).setPreferredWidth(150);
columnModel.getColumn(18).setPreferredWidth(150);
columnModel.getColumn(19).setPreferredWidth(150);
columnModel.getColumn(20).setPreferredWidth(150);
columnModel.getColumn(21).setPreferredWidth(150);
columnModel.getColumn(22).setPreferredWidth(150);
columnModel.getColumn(23).setPreferredWidth(150);
columnModel.getColumn(24).setPreferredWidth(150);
columnModel.getColumn(25).setPreferredWidth(150);
columnModel.getColumn(26).setPreferredWidth(150);
columnModel.getColumn(27).setPreferredWidth(150);
columnModel.getColumn(28).setPreferredWidth(150);
columnModel.getColumn(29).setPreferredWidth(150);
columnModel.getColumn(30).setPreferredWidth(180);
columnModel.getColumn(31).setPreferredWidth(150);
columnModel.getColumn(32).setPreferredWidth(150);
columnModel.getColumn(33).setPreferredWidth(150);
columnModel.getColumn(34).setPreferredWidth(150);
columnModel.getColumn(35).setPreferredWidth(150);
columnModel.getColumn(36).setPreferredWidth(150);
columnModel.getColumn(37).setPreferredWidth(150);
columnModel.getColumn(38).setPreferredWidth(150);
columnModel.getColumn(39).setPreferredWidth(150);
columnModel.getColumn(40).setPreferredWidth(150);
columnModel.getColumn(41).setPreferredWidth(150);
columnModel.getColumn(42).setPreferredWidth(150);
columnModel.getColumn(43).setPreferredWidth(150);
columnModel.getColumn(44).setPreferredWidth(150);
columnModel.getColumn(45).setPreferredWidth(150);
columnModel.getColumn(46).setPreferredWidth(150);
columnModel.getColumn(47).setPreferredWidth(150);
columnModel.getColumn(48).setPreferredWidth(150);
columnModel.getColumn(49).setPreferredWidth(150);
columnModel.getColumn(50).setPreferredWidth(150);
columnModel.getColumn(51).setPreferredWidth(150);
columnModel.getColumn(52).setPreferredWidth(150);
columnModel.getColumn(53).setPreferredWidth(150);
columnModel.getColumn(54).setPreferredWidth(150);
columnModel.getColumn(55).setPreferredWidth(150);
columnModel.getColumn(56).setPreferredWidth(150);
columnModel.getColumn(57).setPreferredWidth(150);
columnModel.getColumn(58).setPreferredWidth(150);
columnModel.getColumn(59).setPreferredWidth(150);
columnModel.getColumn(60).setPreferredWidth(150);
columnModel.getColumn(61).setPreferredWidth(150);
columnModel.getColumn(62).setPreferredWidth(150);
columnModel.getColumn(63).setPreferredWidth(150);
columnModel.getColumn(64).setPreferredWidth(150);
columnModel.getColumn(65).setPreferredWidth(150);
columnModel.getColumn(66).setPreferredWidth(150);
columnModel.getColumn(67).setPreferredWidth(150);
columnModel.getColumn(68).setPreferredWidth(150);
columnModel.getColumn(69).setPreferredWidth(150);




    }

       
  // PROCESO   MOSTRAR LA LISTA DE la bd
   public void mostrar_todo(DefaultTableModel model) { 
    
    //  Para el desplazamiento horizoltal del scrollpane en el jtable       
  jt_despliegue2.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
  jt_despliegue2.doLayout(); 
   //inabilita el movimiento de las columns   del jtable  
    jt_despliegue2.getTableHeader().setReorderingAllowed(false);
    jt_despliegue2.getTableHeader().setResizingAllowed(false);
        
    jt_despliegue2.setModel(model);
    model.addColumn("n") ;  
    model.addColumn("Pedido");
    model.addColumn("Repeticiones Pedido");
    model.addColumn("Mes de Creacion");
    model.addColumn("Orden");
    model.addColumn("Orden 2");
    model.addColumn("Codigo de Proyecto");
    model.addColumn("Repeticiones cod. Pedido");
    model.addColumn("MTTO Adecuaciones");
    model.addColumn("Circuito");
    model.addColumn("Telepuerto");
    model.addColumn("Estatus");
    model.addColumn("Buzon Tareas");
    model.addColumn("Buzon de Devolucion");
    model.addColumn("Fecha Incidencia");
    model.addColumn("Fecha de Asignacion");
    model.addColumn("Fase");
    model.addColumn("Tipo Plan");
    model.addColumn("Tipo Proyecto");
    model.addColumn("tipo Sector");
    model.addColumn("Fase Proyecto");
    model.addColumn("Sector");
    model.addColumn("Tipo Plan General");
    model.addColumn("Region");
    model.addColumn("Estado");
    model.addColumn("Ciudad");
    model.addColumn("Municipio");
    model.addColumn("Parroquia");
    model.addColumn("Unidad de Negocios");
    model.addColumn("Nombre del Cliente Principal");
    model.addColumn("Nombre");
    model.addColumn("Direccion Completa");
    model.addColumn("Nombre 2");
    model.addColumn("Direccion 2");
    model.addColumn("Contacto");
    model.addColumn("Telefono"); 
    model.addColumn("Contacto 2");
    model.addColumn("Telefono 2");
    model.addColumn("Estatus Red");
    model.addColumn("Año");
    model.addColumn("Mes");
    model.addColumn("Fecha Plan");
    model.addColumn("Empresa Plan");
    model.addColumn("Prioridad");
    model.addColumn("Empresa Instaladora");
    model.addColumn("Fecha Instalacion");
    model.addColumn("Nombre Instalador");
    model.addColumn("Empresa Retiro Plan");
    model.addColumn("Empresa Retiro");
    model.addColumn("Fecha Retiro");
    model.addColumn("Telefono Instalador");
    model.addColumn("QoS Operativo");
    model.addColumn("MAC");
    model.addColumn("Despliegue IP");
    model.addColumn("IPoam");
    model.addColumn("Codigo de Cierre");
    model.addColumn("Telepuerto Operativo");
    model.addColumn("Observacion");
    model.addColumn("Observacion General");
    model.addColumn("Cadena");
    model.addColumn("QoS M6");
    model.addColumn("Modem Marca");
    model.addColumn("Modem Modelo");
    model.addColumn("Modem Serial");
    model.addColumn("LNB Marca");
    model.addColumn("LNB Modelo");
    model.addColumn("LNB serial");
    model.addColumn("BUC Marca");
    model.addColumn("BUC Modelo");
    model.addColumn("BUC Serial");
      
        res = operaciones.mostrar_despliegue();
       
        try {
            while (res.next()) {

                 model.addRow(new Object[]{jt_despliegue2.getRowCount()+1, res.getString("pedido"), res.getString("repeticiones_Pedido"), res.getString("mes_de_creacion"), res.getString("orden"), res.getString("orden_2"),res.getString("codigo_de_proyecto"), res.getString("repeticiones_cod_proy"),res.getString("mtto_adecuaciones"), res.getString("circuito"), res.getString("telepuerto"),res.getString("estatus"), res.getString("buzon_tarea"), res.getString("buzon_de_devolucion"), res.getString("fecha_incidencia"), res.getString("fecha_de_asignacion"), res.getString("fase"),res.getString("tipo_plan"), res.getString("tipo_proyecto"), res.getString("tipo_sector"), res.getString("fase_proyecto"), res.getString("sector"), res.getString("tipo_plan_general"),res.getString("region"), res.getString("edo"), res.getString("ciudad"), res.getString("municipio"), res.getString("parroquia"), res.getString("unidad_de_negocio"),res.getString("nombre_del_cliente_principal"), res.getString("nombre"), res.getString("direccion_completa"), res.getString("nombre_2"), res.getString("direccion_2"), res.getString("contacto"),res.getString("telefono"), res.getString("contacto_2"), res.getString("telefono_2"), res.getString("estatus_red"), res.getString("año"), res.getString("mes"),res.getString("fecha_plan"), res.getString("empresa_plan"), res.getString("prioridad"), res.getString("empresa_instaladora"), res.getString("fecha_instalacion"), res.getString("nombre_instalador"),res.getString("empresa_retiro_plan"), res.getString("empresa_retiro"), res.getString("fecha_retiro"), res.getString("telefono_instalador"), res.getString("qos_operativo"), res.getString("mac"),res.getString("despliegue_ip"), res.getString("ipoam"), res.getString("codigo_de_cierre"), res.getString("telepuerto_operativo"), res.getString("observacion"), res.getString("observacion_general"),res.getString("cadena"), res.getString("qos_m6"), res.getString("modem_marca"), res.getString("modem_modelo"), res.getString("modem_serial"), res.getString("lnb_marca"),res.getString("lnb_modelo"),res.getString("lnb_serial"),res.getString("buc_marca"),res.getString("buc_modelo"),res.getString("buc_serial")});
            }
        } catch (Exception e) {

          

        }

    }
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPane1 = new javax.swing.JScrollPane();
        jt_despliegue2 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SCRAS");
        setMinimumSize(new java.awt.Dimension(920, 470));
        setPreferredSize(new java.awt.Dimension(920, 470));
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jt_despliegue2.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        jt_despliegue2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jt_despliegue2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jt_despliegue2.setOpaque(false);
        jScrollPane1.setViewportView(jt_despliegue2);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 857;
        gridBagConstraints.ipady = 326;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(16, 10, 13, 26);
        getContentPane().add(jScrollPane1, gridBagConstraints);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        jLabel1.setText("Despliegue Satelital Actual");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 174;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 20, 0, 0);
        getContentPane().add(jLabel1, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SCRAS_Mostrar_datos_despliegue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SCRAS_Mostrar_datos_despliegue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SCRAS_Mostrar_datos_despliegue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SCRAS_Mostrar_datos_despliegue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SCRAS_Mostrar_datos_despliegue().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jt_despliegue2;
    // End of variables declaration//GEN-END:variables
}
