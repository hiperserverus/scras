

import Control_bd.operaciones_sql;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Kaizer Enrique <kaizer_enrique@hotmail.com at
 * http://kaizerenrique.esy.es/>
 */
public class Identificacion extends javax.swing.JFrame {

    operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
    ResultSet res;
    ResultSet res2;
    public static String usuario2;
    public static String clave2;
    
    public Identificacion() {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("SCRAS");
         setIconImage(new ImageIcon(getClass().getResource("/imagenes/scras_icono.png")).getImage()); 

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JP_FONDO = new javax.swing.JPanel();
        ETIUSUARIO = new javax.swing.JLabel();
        usuario = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        clave = new javax.swing.JPasswordField();
        bot_intro = new javax.swing.JButton();
        cancelar = new javax.swing.JButton();
        jToggleButton1 = new javax.swing.JToggleButton();
        lbl_registro = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SCRAS");
        setMinimumSize(new java.awt.Dimension(580, 220));
        setPreferredSize(new java.awt.Dimension(580, 220));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        JP_FONDO.setBackground(new java.awt.Color(0, 153, 204));
        JP_FONDO.setMinimumSize(new java.awt.Dimension(600, 200));
        JP_FONDO.setName(""); // NOI18N
        JP_FONDO.setPreferredSize(new java.awt.Dimension(600, 200));
        JP_FONDO.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ETIUSUARIO.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        ETIUSUARIO.setText("USUARIO:");
        JP_FONDO.add(ETIUSUARIO, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, 80, 40));

        usuario.setFont(new java.awt.Font("DejaVu Sans", 0, 18)); // NOI18N
        usuario.setToolTipText("Ingrese  Usuario Valido");
        usuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                usuarioKeyReleased(evt);
            }
        });
        JP_FONDO.add(usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 10, 199, 40));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        jLabel1.setText("CLAVE:");
        JP_FONDO.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 60, 60, 40));

        clave.setFont(new java.awt.Font("DejaVu Sans", 0, 18)); // NOI18N
        clave.setToolTipText("Ingrese Clave Valida");
        clave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                claveKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                claveKeyReleased(evt);
            }
        });
        JP_FONDO.add(clave, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 60, 200, 40));

        bot_intro.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        bot_intro.setText("INGRESAR");
        bot_intro.setToolTipText("Ingreso al Sistema");
        bot_intro.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_intro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_introActionPerformed(evt);
            }
        });
        JP_FONDO.add(bot_intro, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 120, 110, -1));

        cancelar.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        cancelar.setText("CANCELAR");
        cancelar.setToolTipText("Cancelar Ingreso");
        cancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarActionPerformed(evt);
            }
        });
        JP_FONDO.add(cancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 120, 110, -1));

        jToggleButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/SCRAS02.png"))); // NOI18N
        JP_FONDO.add(jToggleButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 189, 90));

        lbl_registro.setFont(new java.awt.Font("Times New Roman", 2, 12)); // NOI18N
        lbl_registro.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_registro.setText("Registro de nuevo usuario");
        lbl_registro.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_registro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_registroMouseClicked(evt);
            }
        });
        JP_FONDO.add(lbl_registro, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 160, 250, 10));

        getContentPane().add(JP_FONDO, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 580, 210));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bot_introActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_introActionPerformed

         if(usuario.getText().equals("") && clave.getText().equals("") ){
            JOptionPane.showMessageDialog(rootPane, "Campos Vacios");
        }else{
            if (String.valueOf(usuario.getText()).compareTo("") == 0 && String.valueOf(clave.getPassword()).compareTo(clave.getText()) == 0) {
                    JOptionPane.showMessageDialog(rootPane, "Campo USUARIO esta Vacio");
                    clave.getText().equals("");
                    clave.setText("");
                }else{
                    if (String.valueOf(usuario.getText()).compareTo(usuario.getText()) == 0 && String.valueOf(clave.getPassword()).compareTo("") == 0) {
                    JOptionPane.showMessageDialog(rootPane, "Campo CLAVE esta Vacio");
                    usuario.getText().equals("");
                    usuario.setText("");
                    }else{
                        
                        try {
                            res= operaciones.identificacion_usuario(usuario.getText());
                            res2= operaciones.identificacion_clave(clave.getText());
                            if(res.next() & res2.next()){
                                usuario2=usuario.getText();
                                clave2=clave.getText();
                                
                                if(usuario.getText().equals(usuario2) & clave.getText().equals(clave2) ){
                                    PRINCIPAL interfaz = new PRINCIPAL();
                                    interfaz.setVisible(true);
                                    dispose();
                                }
                                
                            }else {
                                
                                if(res.next()==false){
                                    JOptionPane.showMessageDialog(null,"No existe registro relacionado");
                                    usuario.setText("");
                                    clave.setText("");
                                    
                                }
                            }
                        } catch (SQLException ex) {
//                            Logger.getLogger(Identificacion.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
        }
    }//GEN-LAST:event_bot_introActionPerformed

    private void cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarActionPerformed
        int dato=JOptionPane.showConfirmDialog(rootPane, "Esta seguro de Salir");
        if(dato==0){
            System.exit(0);
            JOptionPane.showMessageDialog(rootPane, " Sesion terminada");
        }
    }//GEN-LAST:event_cancelarActionPerformed
public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
String cadena= (jTextfieldS.getText()).toUpperCase();
jTextfieldS.setText(cadena);
}
    private void usuarioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_usuarioKeyReleased
       convertiraMayusculasEnJtextfield(usuario);
    }//GEN-LAST:event_usuarioKeyReleased

    private void lbl_registroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_registroMouseClicked
       administrador interfaz = new administrador();
       interfaz.setVisible(true);
       dispose();
    }//GEN-LAST:event_lbl_registroMouseClicked

    private void claveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_claveKeyPressed
    
    if(evt.getKeyCode()==10){
    bot_intro.doClick();}
    }//GEN-LAST:event_claveKeyPressed

    private void claveKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_claveKeyReleased
        convertiraMayusculasEnJtextfield(clave);
    }//GEN-LAST:event_claveKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Identificacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Identificacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Identificacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Identificacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Identificacion().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ETIUSUARIO;
    private javax.swing.JPanel JP_FONDO;
    private javax.swing.JButton bot_intro;
    private javax.swing.JButton cancelar;
    private javax.swing.JPasswordField clave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JLabel lbl_registro;
    private javax.swing.JTextField usuario;
    // End of variables declaration//GEN-END:variables
}
