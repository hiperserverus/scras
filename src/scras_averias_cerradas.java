
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Control_bd.*;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Dayana Martinez <dmaro006@gmail.com>
 */
public class scras_averias_cerradas extends javax.swing.JFrame {

    public DefaultTableModel modelo_tabla = new DefaultTableModel() {
            // codigo para las que las celdas del jtbale no sean editables
            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return false; // el resto de las celdas no son editables

            }

    };
    
    operaciones_sql operaciones = new operaciones_sql(); //creo un objeto  para las consultas sql
    ResultSet res;//creacion de la variable resultset
    
    public scras_averias_cerradas() {
        initComponents();
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/scras_icono.png")).getImage()); 
         
 
        mostrar_averias_cerradas(modelo_tabla);
//aqui para asignarle un tamñao a cada columna individualmente
TableColumnModel columnModel = tbl_ave_cerradas.getColumnModel();
columnModel.getColumn(0).setPreferredWidth(150);
columnModel.getColumn(1).setPreferredWidth(300);
columnModel.getColumn(2).setPreferredWidth(150);
columnModel.getColumn(3).setPreferredWidth(150);
columnModel.getColumn(4).setPreferredWidth(150);
columnModel.getColumn(5).setPreferredWidth(150);
columnModel.getColumn(6).setPreferredWidth(150);
columnModel.getColumn(7).setPreferredWidth(250);
columnModel.getColumn(8).setPreferredWidth(150);
columnModel.getColumn(9).setPreferredWidth(150);
columnModel.getColumn(10).setPreferredWidth(150);
columnModel.getColumn(11).setPreferredWidth(150);
columnModel.getColumn(12).setPreferredWidth(150);
columnModel.getColumn(13).setPreferredWidth(150);
columnModel.getColumn(14).setPreferredWidth(150);
columnModel.getColumn(15).setPreferredWidth(150);
columnModel.getColumn(16).setPreferredWidth(150);
columnModel.getColumn(17).setPreferredWidth(150);
columnModel.getColumn(18).setPreferredWidth(150);
columnModel.getColumn(19).setPreferredWidth(150);
columnModel.getColumn(20).setPreferredWidth(150);
columnModel.getColumn(21).setPreferredWidth(150);
columnModel.getColumn(22).setPreferredWidth(250);
columnModel.getColumn(23).setPreferredWidth(250);
columnModel.getColumn(24).setPreferredWidth(250);
columnModel.getColumn(25).setPreferredWidth(150);
columnModel.getColumn(26).setPreferredWidth(150);
columnModel.getColumn(27).setPreferredWidth(150);
columnModel.getColumn(28).setPreferredWidth(150);
columnModel.getColumn(29).setPreferredWidth(250);
columnModel.getColumn(30).setPreferredWidth(250);

    }
    
    // PROCESO   MOSTRAR LA LISTA DE la bd
   public void mostrar_averias_cerradas(DefaultTableModel model) { 
    
    //  Para el desplazamiento horizoltal del scrollpane en el jtable       
  tbl_ave_cerradas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
  tbl_ave_cerradas.doLayout(); 
  
   //inabilita el movimiento de las columns   del jtable  
    tbl_ave_cerradas.getTableHeader().setReorderingAllowed(false);
    tbl_ave_cerradas.getTableHeader().setResizingAllowed(false);
        
    tbl_ave_cerradas.setModel(model);
       
    model.addColumn("Pedido");
    model.addColumn("Nombre");
    model.addColumn("Region");
    model.addColumn("Estado");
    model.addColumn("Municipio");
    model.addColumn("Parroquia");
    model.addColumn("Direccion");
    model.addColumn("Nombre del Cliente Principal");
    model.addColumn("Tipo de Sector");
    model.addColumn("Sector");
    model.addColumn("MAC");
    model.addColumn("IP");
    model.addColumn("MASCARA");
    model.addColumn("QoS");
    model.addColumn("Telepuerto");
    model.addColumn("Circuito");
    model.addColumn("Estatus de la Red");
    model.addColumn("Empresa Instaladora");
    model.addColumn("Codigo de Cierre");
    model.addColumn("Fecha de Reporte");
    model.addColumn("Contacto de Reporte");
    model.addColumn("Telefono de Contacto");
    model.addColumn("Descripcion de la Falla");
    model.addColumn("Causa de la Falla");
    model.addColumn("Observaciones Operativas");
    model.addColumn("Fecha de Atencion");
    model.addColumn("Empresa Encargada");
    model.addColumn("Nombre del Responsable");
    model.addColumn("Telefono");
    model.addColumn("Observaciones para Telepuerto");
    model.addColumn("Acciones de Correcion de Falla");
    
     res = operaciones.mostrar_averias_cerradas();
       
        try {   
            while (res.next()) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                 model.addRow(new Object[]{ res.getString("n_pedido"), res.getString("nombre"), res.getString("region"), res.getString("edo"), res.getString("municipio"),res.getString("parroquia"), res.getString("direccion_completa"),res.getString("nombre_del_cliente_principal"), res.getString("tipo_sector"), res.getString("sector"),res.getString("mac"), res.getString("despliegue_ip"),  res.getString("ipoam"),  res.getString("qos_operativo"),  res.getString("telepuerto"),res.getString("circuito"), res.getString("estatus_red"), res.getString("empresa_instaladora"),res.getString("codigo_de_cierre"), res.getString("f_apertura_a"), res.getString("nombre_contacto"), res.getString("telefono_contacto"), res.getString("descripcion_falla"),res.getString("causa_falla"),res.getString("o_operativas"),res.getString("f_asignacion"),res.getString("responsable_reparacion"),res.getString("nombre_reparador"),res.getString("telefono_contacto_r"),res.getString("obser_p_tele"),res.getString("acciones_corretivas")});
            }
        } catch (Exception e) {

          

        }
   }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jp_fondo = new javax.swing.JPanel();
        lbl_salir = new javax.swing.JLabel();
        lbl_titulo = new javax.swing.JLabel();
        jp_tabla = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_ave_cerradas = new javax.swing.JTable();
        bot_registrar = new javax.swing.JButton();
        lbl_salir1 = new javax.swing.JLabel();
        bot_salir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SCRAS");
        setMinimumSize(new java.awt.Dimension(920, 500));
        setPreferredSize(new java.awt.Dimension(920, 500));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jp_fondo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_salir.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir.setText("Registrar Nueva Averia");
        lbl_salir.setToolTipText("Menu Principal");
        lbl_salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_salir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_salirMouseClicked(evt);
            }
        });
        jp_fondo.add(lbl_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 390, 170, 30));

        lbl_titulo.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        lbl_titulo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbl_titulo.setText("Averías Cerradas");
        jp_fondo.add(lbl_titulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 170, 30));

        jp_tabla.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Control  de Averías Cerradas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 3, 14))); // NOI18N
        jp_tabla.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tbl_ave_cerradas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tbl_ave_cerradas);

        jp_tabla.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 850, 220));

        jp_fondo.add(jp_tabla, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 880, 270));

        bot_registrar.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bot_registrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/registrar (1).png"))); // NOI18N
        bot_registrar.setToolTipText("Registrar Nueva Averia satelital");
        bot_registrar.setContentAreaFilled(false);
        bot_registrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_registrar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/registrar (2).png"))); // NOI18N
        bot_registrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_registrarActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_registrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 330, 90, 80));

        lbl_salir1.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        lbl_salir1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_salir1.setText("Volver ");
        lbl_salir1.setToolTipText("Menu Principal");
        lbl_salir1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jp_fondo.add(lbl_salir1, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 390, 90, 30));

        bot_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menu (1).png"))); // NOI18N
        bot_salir.setToolTipText("Menu Principal");
        bot_salir.setContentAreaFilled(false);
        bot_salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bot_salir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menu (2).png"))); // NOI18N
        bot_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bot_salirActionPerformed(evt);
            }
        });
        jp_fondo.add(bot_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 330, 90, 80));

        getContentPane().add(jp_fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 900, 460));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lbl_salirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_salirMouseClicked
        this.dispose();
    }//GEN-LAST:event_lbl_salirMouseClicked

    private void bot_registrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_registrarActionPerformed
        scras_registro frame = new scras_registro();
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_bot_registrarActionPerformed

    private void bot_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bot_salirActionPerformed
      this.dispose();
    }//GEN-LAST:event_bot_salirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(scras_averias_cerradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(scras_averias_cerradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(scras_averias_cerradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(scras_averias_cerradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new scras_averias_cerradas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bot_registrar;
    private javax.swing.JButton bot_salir;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel jp_fondo;
    private javax.swing.JPanel jp_tabla;
    private javax.swing.JLabel lbl_salir;
    private javax.swing.JLabel lbl_salir1;
    private javax.swing.JLabel lbl_titulo;
    private javax.swing.JTable tbl_ave_cerradas;
    // End of variables declaration//GEN-END:variables
}
