package Control_bd;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Dayana Martinez <dmaro006@gmail.com>
 */
public class operaciones_sql {


    int n;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }
    int id_user;
    String usuario;
    String clave;
    String nombre;
    String telefono;
    String pedido;
    String repeticiones_pedido;
    String mes_de_creacion;
    String orden;
    String orden_2;
    String codigo_de_proyecto;
    String repeticiones_cod_proy;
    String mtto_adecuaciones;
    String circuito;
    String telepuerto;
    String estatus ;
    String buzon_tarea;
    String buzon_de_devolucion;
    String fecha_incidencia;
    String fecha_de_asignacion;
    String fase;
    String tipo_plan;
    String tipo_proyecto;
    String tipo_sector;
    String fase_proyecto;
    String sector;
    String tipo_plan_general;
    String region;
    String edo;
    String ciudad;
    String municipio;
    String parroquia;
    String unidad_de_negocio;
    String nombre_del_cliente_principal;
    String direccion_completa;
    String nombre_2;
    String direccion_2;
    String contacto;
    String contacto_2;
    String telefono_2;
    String estatus_red;
    String año;
    String mes;
    String fecha_plan;
    String empresa_plan;
    String prioridad;
    String empresa_instaladora;
    String fecha_instalacion;
    String nombre_instalador;
    String empresa_retiro_plan;
    String empresa_retiro;
    String fecha_retiro;
    String telefono_instalador;
    String qos_operativo;
    String mac;
    String despliegue_ip;
    String ipoam;
    String codigo_de_cierre;
    String telepuerto_operativo;
    String observacion;
    String observacion_general;
    String cadena;
    String qos_m6;
    String modem_marca;
    String modem_modelo;
    String modem_serial;
    String lnb_marca;
    String lnb_modelo;
    String lnb_serial;
    String buc_marca;
    String buc_modelo;
    String buc_serial; 
    String f_apertura_a;
    String nombre_contacto;
    String telefono_contacto;
    String descripcion_falla;
    String causa_falla;
    String o_operativas; 
    String f_atencion;
    String responsable_reparacion;
    String obser_p_tele;
    String acciones_corretivas;
    String nombre_reparador;
    String telefono_contacto_r;
    

    public String getF_apertura_a() {
        return f_apertura_a;
    }

    public void setF_apertura_a(String f_apertura_a) {
        this.f_apertura_a = f_apertura_a;
    }

    public String getNombre_contacto() {
        return nombre_contacto;
    }

    public void setNombre_contacto(String nombre_contacto) {
        this.nombre_contacto = nombre_contacto;
    }

    public String getDescripcion_falla() {
        return descripcion_falla;
    }

    public void setDescripcion_falla(String descripcion_falla) {
        this.descripcion_falla = descripcion_falla;
    }

    public String getCausa_falla() {
        return causa_falla;
    }

    public void setCausa_falla(String causa_falla) {
        this.causa_falla = causa_falla;
    }

    public String getO_operativas() {
        return o_operativas;
    }

    public void setO_operativas(String o_operativas) {
        this.o_operativas = o_operativas;
    }

    public conexion_bd getConexion() {
        return conexion;
    }

    public void setConexion(conexion_bd conexion) {
        this.conexion = conexion;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
    
    public String getPedido() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido = pedido;
    }

    public String getRepeticiones_pedido() {
        return repeticiones_pedido;
    }

    public void setRepeticiones_pedido(String repeticiones_pedido) {
        this.repeticiones_pedido = repeticiones_pedido;
    }

    public String getMes_de_creacion() {
        return mes_de_creacion;
    }

    public void setMes_de_creacion(String mes_de_creacion) {
        this.mes_de_creacion = mes_de_creacion;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public String getOrden_2() {
        return orden_2;
    }

    public void setOrden_2(String orden_2) {
        this.orden_2 = orden_2;
    }

    public String getCodigo_de_proyecto() {
        return codigo_de_proyecto;
    }

    public void setCodigo_de_proyecto(String codigo_de_proyecto) {
        this.codigo_de_proyecto = codigo_de_proyecto;
    }

    public String getRepeticiones_cod_proy() {
        return repeticiones_cod_proy;
    }

    public void setRepeticiones_cod_proy(String repeticiones_cod_proy) {
        this.repeticiones_cod_proy = repeticiones_cod_proy;
    }

    public String getMtto_adecuaciones() {
        return mtto_adecuaciones;
    }

    public void setMtto_adecuaciones(String mtto_adecuaciones) {
        this.mtto_adecuaciones = mtto_adecuaciones;
    }

    public String getCircuito() {
        return circuito;
    }

    public void setCircuito(String circuito) {
        this.circuito = circuito;
    }

    public String getTelepuerto() {
        return telepuerto;
    }

    public void setTelepuerto(String telepuerto) {
        this.telepuerto = telepuerto;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getBuzon_tarea() {
        return buzon_tarea;
    }

    public void setBuzon_tarea(String buzon_tarea) {
        this.buzon_tarea = buzon_tarea;
    }

    public String getBuzon_de_devolucion() {
        return buzon_de_devolucion;
    }

    public void setBuzon_de_devolucion(String buzon_de_devolucion) {
        this.buzon_de_devolucion = buzon_de_devolucion;
    }

    public String getFecha_incidencia() {
        return fecha_incidencia;
    }

    public void setFecha_incidencia(String fecha_incidencia) {
        this.fecha_incidencia = fecha_incidencia;
    }

    public String getFecha_de_asignacion() {
        return fecha_de_asignacion;
    }

    public void setFecha_de_asignacion(String fecha_de_asignacion) {
        this.fecha_de_asignacion = fecha_de_asignacion;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public String getTipo_plan() {
        return tipo_plan;
    }

    public void setTipo_plan(String tipo_plan) {
        this.tipo_plan = tipo_plan;
    }

    public String getTipo_proyecto() {
        return tipo_proyecto;
    }

    public void setTipo_proyecto(String tipo_proyecto) {
        this.tipo_proyecto = tipo_proyecto;
    }

    public String getTipo_sector() {
        return tipo_sector;
    }

    public void setTipo_sector(String tipo_sector) {
        this.tipo_sector = tipo_sector;
    }

    public String getFase_proyecto() {
        return fase_proyecto;
    }

    public void setFase_proyecto(String fase_proyecto) {
        this.fase_proyecto = fase_proyecto;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getTipo_plan_general() {
        return tipo_plan_general;
    }

    public void setTipo_plan_general(String tipo_plan_general) {
        this.tipo_plan_general = tipo_plan_general;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getEdo() {
        return edo;
    }

    public void setEdo(String edo) {
        this.edo = edo;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getParroquia() {
        return parroquia;
    }

    public void setParroquia(String parroquia) {
        this.parroquia = parroquia;
    }

    public String getUnidad_de_negocio() {
        return unidad_de_negocio;
    }

    public void setUnidad_de_negocio(String unidad_de_negocio) {
        this.unidad_de_negocio = unidad_de_negocio;
    }

    public String getNombre_del_cliente_principal() {
        return nombre_del_cliente_principal;
    }

    public void setNombre_del_cliente_principal(String nombre_del_cliente_principal) {
        this.nombre_del_cliente_principal = nombre_del_cliente_principal;
    }

    public String getDireccion_completa() {
        return direccion_completa;
    }

    public void setDireccion_completa(String direccion_completa) {
        this.direccion_completa = direccion_completa;
    }

    public String getNombre_2() {
        return nombre_2;
    }

    public void setNombre_2(String nombre_2) {
        this.nombre_2 = nombre_2;
    }

    public String getDireccion_2() {
        return direccion_2;
    }

    public void setDireccion_2(String direccion_2) {
        this.direccion_2 = direccion_2;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getContacto_2() {
        return contacto_2;
    }

    public void setContacto_2(String contacto_2) {
        this.contacto_2 = contacto_2;
    }

    public String getTelefono_2() {
        return telefono_2;
    }

    public void setTelefono_2(String telefono_2) {
        this.telefono_2 = telefono_2;
    }

    public String getEstatus_red() {
        return estatus_red;
    }

    public void setEstatus_red(String estatus_red) {
        this.estatus_red = estatus_red;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        this.año = año;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getFecha_plan() {
        return fecha_plan;
    }

    public void setFecha_plan(String fecha_plan) {
        this.fecha_plan = fecha_plan;
    }

    public String getEmpresa_plan() {
        return empresa_plan;
    }

    public void setEmpresa_plan(String empresa_plan) {
        this.empresa_plan = empresa_plan;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getEmpresa_instaladora() {
        return empresa_instaladora;
    }

    public void setEmpresa_instaladora(String empresa_instaladora) {
        this.empresa_instaladora = empresa_instaladora;
    }

    public String getFecha_instalacion() {
        return fecha_instalacion;
    }

    public void setFecha_instalacion(String fecha_instalacion) {
        this.fecha_instalacion = fecha_instalacion;
    }

    public String getNombre_instalador() {
        return nombre_instalador;
    }

    public void setNombre_instalador(String nombre_instalador) {
        this.nombre_instalador = nombre_instalador;
    }

    public String getEmpresa_retiro_plan() {
        return empresa_retiro_plan;
    }

    public void setEmpresa_retiro_plan(String empresa_retiro_plan) {
        this.empresa_retiro_plan = empresa_retiro_plan;
    }

    public String getEmpresa_retiro() {
        return empresa_retiro;
    }

    public void setEmpresa_retiro(String empresa_retiro) {
        this.empresa_retiro = empresa_retiro;
    }

    public String getFecha_retiro() {
        return fecha_retiro;
    }

    public void setFecha_retiro(String fecha_retiro) {
        this.fecha_retiro = fecha_retiro;
    }

    public String getTelefono_instalador() {
        return telefono_instalador;
    }

    public void setTelefono_instalador(String telefono_instalador) {
        this.telefono_instalador = telefono_instalador;
    }

    public String getQos_operativo() {
        return qos_operativo;
    }

    public void setQos_operativo(String qos_operativo) {
        this.qos_operativo = qos_operativo;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getDespliegue_ip() {
        return despliegue_ip;
    }

    public void setDespliegue_ip(String despliegue_ip) {
        this.despliegue_ip = despliegue_ip;
    }

    public String getIpoam() {
        return ipoam;
    }

    public void setIpoam(String ipoam) {
        this.ipoam = ipoam;
    }

    public String getCodigo_de_cierre() {
        return codigo_de_cierre;
    }

    public void setCodigo_de_cierre(String codigo_de_cierre) {
        this.codigo_de_cierre = codigo_de_cierre;
    }

    public String getTelepuerto_operativo() {
        return telepuerto_operativo;
    }

    public void setTelepuerto_operativo(String telepuerto_operativo) {
        this.telepuerto_operativo = telepuerto_operativo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObservacion_general() {
        return observacion_general;
    }

    public void setObservacion_general(String observacion_general) {
        this.observacion_general = observacion_general;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public String getQos_m6() {
        return qos_m6;
    }

    public void setQos_m6(String qos_m6) {
        this.qos_m6 = qos_m6;
    }

    public String getModem_marca() {
        return modem_marca;
    }

    public void setModem_marca(String modem_marca) {
        this.modem_marca = modem_marca;
    }

    public String getModem_modelo() {
        return modem_modelo;
    }

    public void setModem_modelo(String modem_modelo) {
        this.modem_modelo = modem_modelo;
    }

    public String getModem_serial() {
        return modem_serial;
    }

    public void setModem_serial(String modem_serial) {
        this.modem_serial = modem_serial;
    }

    public String getLnb_marca() {
        return lnb_marca;
    }

    public void setLnb_marca(String lnb_marca) {
        this.lnb_marca = lnb_marca;
    }

    public String getLnb_modelo() {
        return lnb_modelo;
    }

    public void setLnb_modelo(String lnb_modelo) {
        this.lnb_modelo = lnb_modelo;
    }

    public String getLnb_serial() {
        return lnb_serial;
    }

    public void setLnb_serial(String lnb_serial) {
        this.lnb_serial = lnb_serial;
    }

    public String getBuc_marca() {
        return buc_marca;
    }

    public void setBuc_marca(String buc_marca) {
        this.buc_marca = buc_marca;
    }

    public String getBuc_modelo() {
        return buc_modelo;
    }

    public void setBuc_modelo(String buc_modelo) {
        this.buc_modelo = buc_modelo;
    }

    public String getBuc_serial() {
        return buc_serial;
    }

    public void setBuc_serial(String buc_serial) {
        this.buc_serial = buc_serial;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
        public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getclave() {
        return clave;
    }

    public void setContraseña(String contraseña) {
        this.clave = clave;
    }
    
    PreparedStatement ps;
    ResultSet res;
    conexion_bd conexion = new conexion_bd();
    
    //Ingresar o Insetar datos
    
    public boolean insertar_despliegue(  String pedido,String repeticiones_pedido,String mes_de_creacion,String orden,String orden_2,String 
            codigo_de_proyecto,
            String repeticiones_cod_proy,
            String mtto_adecuaciones,
            String circuito,
            String telepuerto,
            String estatus ,
            String buzon_tarea,
            String buzon_de_devolucion,
            String fecha_incidencia,
            String fecha_de_asignacion,
            String fase,
            String tipo_plan,
            String tipo_proyecto,
            String tipo_sector,
            String fase_proyecto,
            String sector,
            String tipo_plan_general,
            String region,
            String edo,
            String ciudad,
            String municipio,
            String parroquia,
            String unidad_de_negocio,
            String nombre_del_cliente_principal,
            String nombre,
            String direccion_completa,
            String nombre_2,
            String direccion_2,
            String contacto,
            String telefono,
            String contacto_2,
            String telefono_2,
            String estatus_red,
            String año,
            String mes,
            String fecha_plan,
            String empresa_plan,
            String prioridad,
            String empresa_instaladora,
            String fecha_instalacion,
            String nombre_instalador,
            String Empresa_retiro_plan,
            String Empresa_retiro,
            String fecha_retiro,
            String telefono_instalador,
            String qos_operativo,
            String mac,
            String despliegue_ip,
            String ipoam,
            String codigo_de_cierre,
            String telepuerto_operativo,
            String observacion,
            String observacion_general,
            String cadena,
            String qos_m6,
            String modem_marca,
            String modem_modelo,
            String modem_serial,
            String lnb_marca,
            String lnb_modelo,
            String lnb_serial,
            String buc_marca,
            String buc_modelo,
            String buc_serial){
        boolean estado = false;

        try{
//        String sql= "insert into tbl_guardar_despliegue ( pedido, repeticiones_pedido , mes_de_creacion ,orden , orden_2 ,codigo_de_proyecto , repeticiones_cod_proy , mtto_adecuaciones ,circuito, telepuerto ,estatus , buzon_tarea , buzon_de_devolucion ,fecha_incidencia , fecha_de_asignacion , fase , tipo_plan , tipo_proyecto,tipo_sector , fase_proyecto ,sector , tipo_plan_general , region ,edo , ciudad ,municipio, parroquia, unidad_de_negocio ,nombre_del_cliente_principal , nombre , direccion_completa,nombre_2,  direccion_2 ,contacto, telefono ,contacto_2 , telefono_2 ,estatus_red , año ,mes ,fecha_plan , empresa_plan , prioridad , empresa_instaladora , fecha_instalacion,nombre_instalador , Empresa_retiro_plan ,Empresa_retiro, fecha_retiro, telefono_instalador,qos_operativo, mac , despliegue_ip , ipoam , codigo_de_cierre ,telepuerto_operativo , observacion , observacion_general,cadena, qos_m6 ,modem_marca , modem_modelo,modem_serial,lnb_marca, lnb_modelo,lnb_serial  , buc_marca , buc_modelo, buc_serial ) values ("+ "\""+ pedido+ "\", "\""+repeticiones_pedido+ "\" ,"\""+ mes_de_creacion + "\","\""+orden + "\", "\""+orden_2 + "\","\""+codigo_de_proyecto + "\", "\""+repeticiones_cod_proy + "\","\""+ mtto_adecuaciones +"\","\""+circuito+ "\", "\""+telepuerto+ "\" ,"\""+estatus +"\","\""+ buzon_tarea + "\","\""+ buzon_de_devolucion + "\","\""+fecha_incidencia + "\","\""+ fecha_de_asignacion + "\","\""+ fase + "\", "\""+tipo_plan + "\", "\""+tipo_proyecto+ "\","\""+tipo_sector + "\","\""+ fase_proyecto + "\","\""+sector + "\","\""+ tipo_plan_general + "\","\""+ region +-"\","\""+edo+"\" , "\""+ciudad + "\","\""+municipio+ "\", "\""+parroquia+ "\", "\""+unidad_de_negocio + "\","\""+nombre_del_cliente_principal+ "\" ,"\""+ nombre+ "\" , "\""+direccion_completa+ "\","\""+nombre_2+ "\",  "\""+direccion_2 + "\","\""+contacto+ "\","\""+ telefono + "\","\""+contacto_2 + "\","\""+ telefono_2+ "\" ,"\""+estatus_red + "\","\""+ año + "\","\""+mes+ "\" ,"\""+fecha_plan + "\","\""+ empresa_plan + "\","\""+ prioridad +"\","\""+ empresa_instaladora + "\","\""+ fecha_instalacion+ "\","\""+nombre_instalador + "\","\""+ Empresa_retiro_plan + "\","\""+Empresa_retiro+ "\","\""+ fecha_retiro+ "\","\""+ telefono_instalador+ "\","\""+qos_operativo+ "\", "\""+mac + "\", "\""+despliegue_ip + "\","\""+ ipoam + "\", "\""+codigo_de_cierre + "\","\""+telepuerto_operativo + "\","\""+ observacion + "\","\""+ observacion_general+ "\","\""+cadena+ "\","\""+ qos_m6 + "\","\""+modem_marca + "\","\""+ modem_modelo+ "\","\""+modem_serial+ "\","\""+lnb_marca+"\", "\""+lnb_modelo+ "\","\""+lnb_serial + "\","\""+ buc_marca + "\", "\""+buc_modelo+ "\", "\""+buc_serial +"\")";

        String sql= "insert into tbl_guardar_despliegue ( pedido, repeticiones_pedido , mes_de_creacion ,orden , orden_2 ,codigo_de_proyecto , repeticiones_cod_proy , mtto_adecuaciones ,circuito, telepuerto ,estatus , buzon_tarea , buzon_de_devolucion ,fecha_incidencia , fecha_de_asignacion , fase , tipo_plan , tipo_proyecto,tipo_sector , fase_proyecto ,sector , tipo_plan_general , region ,edo , ciudad ,municipio, parroquia, unidad_de_negocio ,nombre_del_cliente_principal , nombre , direccion_completa,nombre_2,  direccion_2 ,contacto, telefono ,contacto_2 , telefono_2 ,estatus_red , año ,mes ,fecha_plan , empresa_plan , prioridad , empresa_instaladora , fecha_instalacion,nombre_instalador , Empresa_retiro_plan ,Empresa_retiro, fecha_retiro, telefono_instalador,qos_operativo, mac , despliegue_ip , ipoam , codigo_de_cierre ,telepuerto_operativo , observacion , observacion_general,cadena, qos_m6 ,modem_marca , modem_modelo,modem_serial,lnb_marca, lnb_modelo,lnb_serial  , buc_marca , buc_modelo, buc_serial ) values ( '"+ pedido+ "', '"+repeticiones_pedido+ "' ,'"+ mes_de_creacion + "','"+orden + "', '"+orden_2 + "','"+codigo_de_proyecto + "', '"+repeticiones_cod_proy + "','"+ mtto_adecuaciones + "','"+circuito+ "', '"+telepuerto+ "' ,'"+estatus + "','"+ buzon_tarea + "','"+ buzon_de_devolucion + "','"+fecha_incidencia + "','"+ fecha_de_asignacion + "','"+ fase + "', '"+tipo_plan + "', '"+tipo_proyecto+ "','"+tipo_sector + "','"+ fase_proyecto + "','"+sector + "','"+ tipo_plan_general + "','"+ region + "','"+edo+ "' , '"+ciudad + "','"+municipio+ "', '"+parroquia+ "', '"+unidad_de_negocio + "','"+nombre_del_cliente_principal+ "' , '"+ nombre+ "' , '"+ direccion_completa+ "' , '"+nombre_2+ "',  '"+direccion_2 + "','"+contacto+ "','"+ telefono + "','"+contacto_2 + "','"+ telefono_2+ "' ,'"+estatus_red + "','"+ año + "','"+mes+ "' ,'"+fecha_plan + "','"+ empresa_plan + "','"+ prioridad + "','"+ empresa_instaladora + "','"+ fecha_instalacion+ "','"+nombre_instalador + "','"+ Empresa_retiro_plan + "','"+Empresa_retiro+ "','"+ fecha_retiro+ "','"+ telefono_instalador+ "','"+qos_operativo+ "', '"+mac + "', '"+despliegue_ip + "','"+ ipoam + "', '"+codigo_de_cierre + "','"+telepuerto_operativo + "','"+ observacion + "','"+ observacion_general+ "','"+cadena+ "','"+ qos_m6 + "','"+modem_marca + "','"+ modem_modelo+ "','"+modem_serial+ "','"+lnb_marca+ "', '"+lnb_modelo+ "','"+lnb_serial  + "','"+ buc_marca + "', '"+buc_modelo+ "', '"+buc_serial +"')";
          ps = conexion.conectado().prepareStatement(sql);
          ps.execute();
                ps.close();
                estado = true;
        }catch(SQLException e){
            System.out.println("error" +e);

        }

        return estado;
        }
    
    
    public boolean insertar_datos_tecnicos(String pedido , String nombre_del_cliente_principal, String region ,String edo  ,String municipio ,String parroquia  ,String direccion_completa ,String tipo_sector   ,String sector ,String mac, String nombre , String despliegue_ip  ,String ipoam ,String  qos_operativo , String telepuerto  , String circuito  ,String codigo_de_cierre,String  estatus_red , String  empresa_instaladora){
        boolean estado = false;
    
        try{
        

        String sql= "insert into tbl_datos_tecnicos (n_pedido , nombre_del_cliente_principal,region , edo   ,municipio ,parroquia  ,direccion_completa ,tipo_sector   ,sector,mac, nombre , despliegue_ip  , ipoam , qos_operativo , telepuerto  , circuito  ,codigo_de_cierre, estatus_red ,  empresa_instaladora ) values ('"+pedido+"' , '"+nombre_del_cliente_principal+"','"+region+"' ,'"+ edo+"' , '"+municipio+"' ,'"+parroquia+"' ,'"+direccion_completa+"' , '"+tipo_sector+"','"+sector+"','"+mac+"' ,'"+nombre+"' ,'"+ despliegue_ip+"', '"+ipoam+"' , '"+ qos_operativo+"' , '"+telepuerto+"'  , '"+circuito+"'  , '"+codigo_de_cierre+"', '"+estatus_red+"' , '"+empresa_instaladora+"' )";
          
          ps = conexion.conectado().prepareStatement(sql);
          ps.execute();
                ps.close();
                estado = true;
        }catch(SQLException e){
            System.out.println("error" +e);

        }

        return estado;
        }    
    

    
       public boolean insertar_averia(String id_ticket, String f_apertura_a ,String nombre_contacto ,String telefono_contacto , String descripcion_falla ,String causa_falla  ,String o_operativas,Integer id_dato_tecnico ){
        boolean estado = false;
    
        try{
       

        String sql= "insert into tbl_averias_reportadas (id_ticket, f_apertura_a ,nombre_contacto ,telefono_contacto , descripcion_falla ,causa_falla  , o_operativas, tbl_datos_tecnicos_n_dato ) values ( '"+id_ticket+ "','"+f_apertura_a+ "', '"+nombre_contacto+ "' ,'"+telefono_contacto+ "' , '"+descripcion_falla + "','"+causa_falla + "','"+o_operativas+ "' , "+id_dato_tecnico+")";
          ps = conexion.conectado().prepareStatement(sql);
          ps.execute();
                ps.close();
                estado = true;
        }catch(SQLException e){
            System.out.println("error" +e);

        }

        return estado;
        }    
       
       
       
         public boolean insertar_averia_2(String id_ticket, String f_apertura_a ,String nombre_contacto ,String telefono_contacto , String descripcion_falla ,String causa_falla  ,String o_operativas ){
        boolean estado = false;
    
        try{
       

        String sql= "insert into tbl_averias_reportadas_2 (id_ticket, f_apertura_a ,nombre_contacto ,telefono_contacto , descripcion_falla ,causa_falla  , o_operativas) values ( '"+id_ticket+ "','"+f_apertura_a+ "', '"+nombre_contacto+ "' ,'"+telefono_contacto+ "' , '"+descripcion_falla + "','"+causa_falla + "','"+o_operativas+ "' )";
          ps = conexion.conectado().prepareStatement(sql);
          ps.execute();
                ps.close();
                estado = true;
        }catch(SQLException e){
            System.out.println("error" +e);

        }

        return estado;
        }    
       
              public boolean insertar_cierre_averia( String f_atencion ,String responsable_reparacion ,String obser_p_tele  ,String acciones_corretivas, String nombre_reparador, String telefono_contacto_r,Integer id_ticket,  Integer id_pedido ,String numero_referencia){
        boolean estado = false;
    
        try{
 

        String sql= "insert into tbl_cierre_averias ( f_asignacion ,responsable_reparacion ,obser_p_tele ,  acciones_corretivas, nombre_reparador, telefono_contacto_r,tbl_averias_reportadas_2_id_tick,tbl_datos_tecnicos_n_dato, numero_referencia ) values ( '"+f_atencion+ "', '"+responsable_reparacion+ "' ,'"+obser_p_tele+ "' , '"+acciones_corretivas + "','"+nombre_reparador + "','"+telefono_contacto_r+ "', "+id_ticket+ ", "+id_pedido+",'"+numero_referencia+"' )";
          ps = conexion.conectado().prepareStatement(sql);
          ps.execute();
                ps.close();
                estado = true;
        }catch(SQLException e){
            System.out.println("error" +e);

        }

        return estado;
        } 
              
        public boolean insertar_usuario(String usuario, String clave ){
        boolean estado = false;
    
        try{
       

        String sql= "insert into tbl_inicio_sesion (usuario, clave   ) values ( '"+usuario+ "','"+clave+ "')";
          ps = conexion.conectado().prepareStatement(sql);
          ps.execute();
                ps.close();
                estado = true;
        }catch(SQLException e){
            System.out.println("error" +e);

        }

        return estado;
        } 
              
//////////////////////////////////////////////////////Consultas sql
    
    public ResultSet mostrar_usuario(String usuario){
   
        try{
            ps = conexion.conectado().prepareStatement("select * from tbl_inicio_sesion where usuario like '%"+usuario+ "%' ");
            res = ps.executeQuery();

        }catch(SQLException e){
            System.out.println("error" +e);
           //JOptionPane.showMessageDialog(null,"error"+ e);
        }
            return res;
     }
    
    public ResultSet mostrar_contraseña(String contraseña){
   
        try{
            ps = conexion.conectado().prepareStatement("select * from tbl_inicio_sesion where contraseña like '%"+contraseña+ "%' ");
            res = ps.executeQuery();

        }catch(SQLException e){
            System.out.println("error" +e);
           //JOptionPane.showMessageDialog(null,"error"+ e);
        }
            return res;
     }
    

public ResultSet mostrar_despliegue (){
        
    
        try{

    
          ps = conexion.conectado().prepareStatement("select  pedido, repeticiones_pedido , mes_de_creacion ,orden , orden_2 ,codigo_de_proyecto , repeticiones_cod_proy , mtto_adecuaciones ,circuito, telepuerto ,estatus , buzon_tarea , buzon_de_devolucion ,fecha_incidencia , fecha_de_asignacion , fase , tipo_plan , tipo_proyecto,tipo_sector , fase_proyecto ,sector , tipo_plan_general , region ,edo , ciudad ,municipio, parroquia, unidad_de_negocio ,nombre_del_cliente_principal , nombre , direccion_completa,nombre_2,  direccion_2 ,contacto, telefono ,contacto_2 , telefono_2 ,estatus_red , año ,mes ,fecha_plan , empresa_plan , prioridad , empresa_instaladora , fecha_instalacion,nombre_instalador , empresa_retiro_plan ,empresa_retiro, fecha_retiro, telefono_instalador,qos_operativo, mac , despliegue_ip , ipoam , codigo_de_cierre ,telepuerto_operativo , observacion , observacion_general,cadena, qos_m6 ,modem_marca , modem_modelo,modem_serial,lnb_marca, lnb_modelo,lnb_serial  , buc_marca , buc_modelo, buc_serial  from tbl_guardar_despliegue");
          res = ps.executeQuery();
                

        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        }

public Boolean actualizar_despliegue( String pedido,String repeticiones_pedido,String mes_de_creacion,String orden,String orden_2,String codigo_de_proyecto,String repeticiones_cod_proy,String mtto_adecuaciones,String circuito,String telepuerto,String estatus ,String buzon_tarea,String buzon_de_devolucion,String fecha_incidencia,String fecha_de_asignacion,String fase,String tipo_plan,String tipo_proyecto,String tipo_sector,String fase_proyecto,String sector,String tipo_plan_general,String region,String edo,String ciudad,String municipio,String parroquia,String unidad_de_negocio,String nombre_del_cliente_principal,String nombre,String direccion_completa,String nombre_2,String direccion_2,String contacto,String telefono,String contacto_2,String telefono_2,String estatus_red,String año,String mes,String fecha_plan,String empresa_plan,String prioridad,String empresa_instaladora,String fecha_instalacion,String nombre_instalador,String empresa_retiro_plan,String empresa_retiro,String fecha_retiro,String telefono_instalador,String qos_operativo,String mac,String despliegue_ip,String ipoam,String codigo_de_cierre,String telepuerto_operativo,String observacion,String observacion_general,String cadena,String qos_m6,String modem_marca,String modem_modelo,String modem_serial,String lnb_marca,String lnb_modelo,String lnb_serial,String buc_marca,String buc_modelo,String buc_serial){
     
      boolean estado = false;
              try {
      
  String   sql = "update tbl_guardar_despliegue  set  pedido= '"+pedido+"', repeticiones_pedido= '"+repeticiones_pedido+"',mes_de_creacion= '"+mes_de_creacion+"',orden= '"+orden+"',orden_2 = '"+orden_2+"', codigo_de_proyecto='"+codigo_de_proyecto+"',repeticiones_cod_proy= '"+repeticiones_cod_proy+"', mtto_adecuaciones= '"+mtto_adecuaciones+"',circuito= '"+circuito+"',telepuerto= '"+telepuerto+"',estatus = '"+estatus+"', buzon_tarea='"+buzon_tarea+"',buzon_de_devolucion= '"+buzon_de_devolucion+"', fecha_incidencia= '"+fecha_incidencia+"',fecha_de_asignacion= '"+fecha_de_asignacion+"',fase= '"+fase+"',tipo_plan = '"+tipo_plan+"', tipo_proyecto='"+tipo_proyecto+"',tipo_sector= '"+tipo_sector+"', fase_proyecto= '"+fase_proyecto+"',sector= '"+sector+"',tipo_plan_general= '"+tipo_plan_general+"',region= '"+region+"', edo='"+edo+"', ciudad= '"+ciudad+"', municipio= '"+municipio+"',parroquia= '"+parroquia+"',unidad_de_negocio= '"+unidad_de_negocio+"',nombre_del_cliente_principal = '"+nombre_del_cliente_principal+"', nombre='"+nombre+"',direccion_completa= '"+direccion_completa+"', nombre_2= '"+nombre_2+"',direccion_2= '"+direccion_2+"',contacto= '"+contacto+"',telefono = '"+telefono+"', contacto_2='"+contacto_2+"', telefono_2= '"+telefono_2+"', estatus_red= '"+estatus_red+"',año= '"+año+"',mes= '"+mes+"',fecha_plan = '"+fecha_plan+"', empresa_plan='"+empresa_plan+"',prioridad= '"+prioridad+"', empresa_instaladora= '"+empresa_instaladora+"',fecha_instalacion= '"+fecha_instalacion+"',nombre_instalador= '"+nombre_instalador+"',empresa_retiro_plan = '"+empresa_retiro_plan+"', empresa_retiro='"+empresa_retiro+"',fecha_retiro= '"+fecha_retiro+"', telefono_instalador= '"+telefono_instalador+"',qos_operativo= '"+qos_operativo+"',mac= '"+mac+"',despliegue_ip = '"+despliegue_ip+"',ipoam = '"+ipoam+"', codigo_de_cierre='"+codigo_de_cierre+"',telepuerto_operativo= '"+telepuerto_operativo+"', observacion= '"+observacion+"',observacion_general= '"+observacion_general+"',cadena= '"+cadena+"',qos_m6 = '"+qos_m6+"', modem_marca='"+modem_marca+"',modem_modelo = '"+modem_modelo+"',modem_serial= '"+modem_serial+"', lnb_marca= '"+lnb_marca+"',lnb_modelo= '"+lnb_modelo+"',lnb_serial= '"+lnb_serial+"',buc_marca = '"+buc_marca+"', buc_modelo='"+buc_modelo+"',buc_serial= '"+buc_serial+"' where pedido = '"+pedido+"' "; 
         
                   ps = conexion.conectado().prepareStatement(sql);
   
                   ps.execute();
                   ps.close();
                        estado = true;
              }catch(SQLException e){
         System.out.println(e);
      }
       return estado;  
    
    }

public ResultSet mostrar_averias (){
        
    
        try{
   

    
          ps = conexion.conectado().prepareStatement("select  n_pedido, nombre ,region ,edo , municipio, parroquia,direccion_completa,nombre_del_cliente_principal,tipo_sector,sector,mac,despliegue_ip, ipoam ,qos_operativo, telepuerto, circuito ,estatus_red , empresa_instaladora,codigo_de_cierre,f_apertura_a,nombre_contacto,telefono_contacto,descripcion_falla,causa_falla,o_operativas "
                  + " from tbl_averias_reportadas "
                  + " inner join tbl_datos_tecnicos  "
                  + "  on tbl_averias_reportadas.tbl_datos_tecnicos_n_dato= tbl_datos_tecnicos.n_dato " );
          res = ps.executeQuery();
                

        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        }

public ResultSet mostrar_averias_cerradas (){
        
    
        try{
   

    
          ps = conexion.conectado().prepareStatement("select  n_pedido, nombre ,region ,edo , municipio, parroquia,direccion_completa,nombre_del_cliente_principal,tipo_sector,sector,mac,despliegue_ip, ipoam ,qos_operativo, telepuerto, circuito ,estatus_red , empresa_instaladora,codigo_de_cierre,f_apertura_a,nombre_contacto,telefono_contacto,descripcion_falla,causa_falla,o_operativas,f_asignacion,responsable_reparacion,nombre_reparador,telefono_contacto_r,obser_p_tele,acciones_corretivas "
                  + " from tbl_cierre_averias "
                  
                  + "   inner join tbl_averias_reportadas_2  "
                  + " on tbl_cierre_averias.tbl_averias_reportadas_2_id_tick = tbl_averias_reportadas_2.id_tick "
                  
                  + "  inner join tbl_datos_tecnicos  "
                  + "  on tbl_cierre_averias.tbl_datos_tecnicos_n_dato= tbl_datos_tecnicos.n_dato " );
          
          res = ps.executeQuery();
                

        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        }

public ResultSet mostrar_usuario (){
        
    
        try{

    
          ps = conexion.conectado().prepareStatement("select  id_user, usuario , clave from tbl_inicio_sesion");
          res = ps.executeQuery();
                

        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        }

public ResultSet existe_archivo (String pedido){
      
        try{

          ps = conexion.conectado().prepareStatement("select  pedido, repeticiones_pedido , mes_de_creacion ,orden , orden_2 ,codigo_de_proyecto , repeticiones_cod_proy , mtto_adecuaciones ,circuito, telepuerto ,estatus , buzon_tarea , buzon_de_devolucion ,fecha_incidencia , fecha_de_asignacion , fase , tipo_plan , tipo_proyecto,tipo_sector , fase_proyecto ,sector , tipo_plan_general , region ,edo , ciudad ,municipio, parroquia, unidad_de_negocio ,nombre_del_cliente_principal , nombre , direccion_completa,nombre_2,  direccion_2 ,contacto, telefono ,contacto_2 , telefono_2 ,estatus_red , año ,mes ,fecha_plan , empresa_plan , prioridad , empresa_instaladora , fecha_instalacion,nombre_instalador , empresa_retiro_plan ,empresa_retiro, fecha_retiro, telefono_instalador,qos_operativo, mac , despliegue_ip , ipoam , codigo_de_cierre ,telepuerto_operativo , observacion , observacion_general,cadena, qos_m6 ,modem_marca , modem_modelo,modem_serial,lnb_marca, lnb_modelo,lnb_serial  , buc_marca , buc_modelo, buc_serial   from tbl_guardar_despliegue where pedido='"+pedido+"'");
          res = ps.executeQuery();
        
        }catch(SQLException e){
            System.out.println("error" +e);

        }

        return res;

        }

public ResultSet consultar_despliegue (String consultar, String opcion){
        
    
        try{

    
          ps = conexion.conectado().prepareStatement("select  pedido, circuito,telepuerto ,tipo_sector ,sector , region ,edo , municipio, parroquia,nombre_del_cliente_principal , nombre , direccion_completa,estatus_red , empresa_instaladora , qos_operativo, mac , despliegue_ip , ipoam , codigo_de_cierre   from tbl_guardar_despliegue where "+opcion+" like '%"+consultar+"%' ");
          res = ps.executeQuery();
                

        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        }

 

 
 public ResultSet id_tick (String id_tick){ // se esta uando esta
        
    
        try{

    
          ps = conexion.conectado().prepareStatement("select  id_tick  from tbl_averias_reportadas where id_ticket like '%"+id_tick+"%'  ");
          res = ps.executeQuery();
                

        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        }

 
 public ResultSet contador_averias_abiertas(){ // se esta uando esta
        
    
        try{
           ps = conexion.conectado().prepareStatement("SELECT COUNT(*) FROM tbl_averias_reportadas ");
          res = ps.executeQuery();
         
        }catch(SQLException e){
            System.out.println("error" +e);

       }

        return res;


        }

  public ResultSet contador_averias_cerradas(){ 
      try {
    
      ps = conexion.conectado().prepareStatement("SELECT COUNT(*) FROM tbl_cierre_averias ");
      res = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(operaciones_sql.class.getName()).log(Level.SEVERE, null, ex);
        }

 return res;
} 

      

 
 
 
  public ResultSet id_pedido (String n_pedido){ //Para capturar el numero de pedido
        
    
        try{

    
          ps = conexion.conectado().prepareStatement("select  n_dato  from tbl_datos_tecnicos where n_pedido like '"+n_pedido+"' ");
          res = ps.executeQuery();
                

        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        } 
  
 
  public ResultSet id_mac (String n_mac){ //Para capturar el numero de pedido
        
    
        try{

    
          ps = conexion.conectado().prepareStatement("select  n_dato  from tbl_datos_tecnicos where mac like '"+n_mac+"' ");
          res = ps.executeQuery();
                

        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        } 
  

         
         //CODIGO PARA GENERAR LA RUTA
         
        
      
           public ResultSet generar_ruta_estado(String estado,String municipio){
        
    
        try{
   

    
            ps = conexion.conectado().prepareStatement("select  n_pedido, nombre ,region ,edo , municipio, parroquia,direccion_completa,nombre_del_cliente_principal,tipo_sector,sector,mac,despliegue_ip, ipoam ,qos_operativo, telepuerto, circuito ,estatus_red , empresa_instaladora,codigo_de_cierre,f_apertura_a,nombre_contacto,telefono_contacto,descripcion_falla,causa_falla,o_operativas "
                  + " from tbl_averias_reportadas "
                  + " inner join tbl_datos_tecnicos  "
                  + "  on tbl_averias_reportadas.tbl_datos_tecnicos_n_dato= tbl_datos_tecnicos.n_dato where edo like '%"+estado+"%' && municipio like '%"+municipio+"%' ");
          res = ps.executeQuery();
                                                                            


        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        }
           
             public ResultSet identificacion_usuario(String usuario){
                try{
                    ps = conexion.conectado().prepareStatement("select usuario from tbl_inicio_sesion where usuario like '%"+usuario+"' ");
                    res = ps.executeQuery();
                }catch(SQLException e){
                    JOptionPane.showMessageDialog(null,"error"+ e);
                }
                    return res;
            }
           
           public ResultSet identificacion_clave(String clave){
                try{
                    ps = conexion.conectado().prepareStatement("select clave from tbl_inicio_sesion where clave like '%"+clave+"' ");
                    res = ps.executeQuery();
                }catch(SQLException e){
                    JOptionPane.showMessageDialog(null,"error"+ e);
                }
                return res;
            }
           
   public ResultSet consultar_login(){
                try{
                    ps = conexion.conectado().prepareStatement("select * from tbl_inicio_sesion ");
                    res = ps.executeQuery();
                }catch(SQLException e){
                    System.out.println("aqui hay un error"+ e);
                }
                return res;
            }         
           
          public ResultSet id_cierre(String numero_referencia){
                try{
                    ps = conexion.conectado().prepareStatement("select numero_referencia from tbl_cierre_averias  where numero_referencia like '%"+numero_referencia+"%' ");
                    res = ps.executeQuery();
                }catch(SQLException e){
                    JOptionPane.showMessageDialog(null,"error"+ e);
                }
                return res;
            }
    
          public boolean borrar_despliegue(){
     
     boolean estado = true;
     try{
         
        String sql = ("DELETE FROM tbl_guardar_despliegue ");
          
        ps= conexion.conectado().prepareStatement(sql);
        ps.execute();
        ps.close();
     }catch (SQLException e){
         
         System.out.println(e);
     }
     return estado;     
 }   
          
                   public boolean borrar_averia_abierta(String tikec){
     
     boolean estado = true;
     try{
         
        String sql = ("DELETE FROM tbl_averias_reportadas where id_ticket like '%"+tikec+"%'");
          
        ps= conexion.conectado().prepareStatement(sql);
        ps.execute();
        ps.close();
     }catch (SQLException e){
         
         System.out.println(e);
     }
     return estado;     
 }  
        
          
         public ResultSet despliegue_lleno(){
        
    
        try{
         ps = conexion.conectado().prepareStatement("select * from tbl_guardar_despliegue" );
         res = ps.executeQuery();
        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        } 
         
          
         public ResultSet comparar_fecha_registro(String registro){
        
    
        try{
         ps = conexion.conectado().prepareStatement("select id_ticket from tbl_averias_reportadas where f_apertura_a like '"+registro+"'" );
         res = ps.executeQuery();
        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        }  
         
         
       public ResultSet tbl_cierre_averias(String numero_referencia){
        
    
        try{
         ps = conexion.conectado().prepareStatement("select id_cierre from tbl_cierre_averias where numero_referencia like '%"+numero_referencia+"%' " );
         res = ps.executeQuery();
        }catch(SQLException e){
            System.out.println("error" +e);

            //JOptionPane.showMessageDialog(null,"error" +e);
        }

        return res;


        }       
         
   
}
